package com.john.test.demorpc.common;

import com.john.study.grpc.common.infos.MethodInfo;
import com.john.study.grpc.common.infos.ServiceInfo;
import com.john.test.demorpc.common.ifaces.IUserService;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * @author jianguangtao03382 2018/5/3
 */
public class InfoParseTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(InfoParseTest.class);

    @Test
    public void testParseInterfaceInfo() {
        ServiceInfo serviceInfo = ServiceInfo.from(IUserService.class);
        LOGGER.info("接口信息：{}", serviceInfo.getInterfaceName());

        for (Map.Entry<String, MethodInfo> entry : serviceInfo.getMethodInfos().entrySet()) {
            LOGGER.info("=={}", entry.getKey());
        }
    }


}
