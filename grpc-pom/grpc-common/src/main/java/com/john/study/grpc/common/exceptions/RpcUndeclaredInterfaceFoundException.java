package com.john.study.grpc.common.exceptions;

/**
 * @author jianguangtao03382 2018/5/4
 */
public class RpcUndeclaredInterfaceFoundException extends RpcException {
    private static final long serialVersionUID = 5231377953114229666L;

    public RpcUndeclaredInterfaceFoundException(String interfaceName) {
        super("Undeclared interface " + interfaceName);
    }
}
