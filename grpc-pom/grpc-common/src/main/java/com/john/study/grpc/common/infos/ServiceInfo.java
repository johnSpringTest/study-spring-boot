package com.john.study.grpc.common.infos;

import com.john.study.grpc.common.exceptions.RpcException;
import com.john.study.grpc.common.util.RpcUtils;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * 服务中包含的接口信息
 *
 * @author jianguangtao03382 2018/5/3
 */
public class ServiceInfo implements Serializable {
    private static final long serialVersionUID = 9004280132899114660L;
    /**
     * 服务的接口实现类
     */
    private Class<?> clazz;
    /**
     * 服务名称
     */
    private String serviceName;
    /**
     * 服务的方法列表
     */
    private Map<String, MethodInfo> methodInfos = new HashMap<>();

    private ServiceInfo(Class<?> interfaceType) {
        this.clazz = interfaceType;
        parse();
    }

    /**
     * 初始化一个服务信息
     *
     * @param interfaceType 服务的接口类型
     * @return 服务信息
     */
    public static ServiceInfo from(Class<?> interfaceType) {
        if (null == interfaceType) {
            throw new IllegalArgumentException("请指定接口类");
        }
        if (!interfaceType.isInterface()) {
            throw new IllegalArgumentException("必须是接口类，不能是其它类型！");
        }
        return new ServiceInfo(interfaceType);
    }

    /**
     * 内部的解析方法
     */
    private void parse() {
        this.serviceName = clazz.getName();
        Method[] methods = clazz.getMethods();
        if (null == methods || methods.length == 0) {
//            throw new IllegalArgumentException("接口类"+interfaceType.getName()+" 未声明任何公用方法！");
            AppInfo.LOGGER.warn("接口类 {} 未声明任何公用方法！", clazz.getName());
        } else {
            for (Method method : methods) {
                MethodInfo mi = MethodInfo.from(method);
                if (methodInfos.containsKey(mi.getMethodKey())) {
                    throw new RpcException("接口" + serviceName + "中有重复的方法:" + method.toString());
                }
                methodInfos.put(mi.getMethodKey(), mi);
            }
        }
    }

    public String getInterfaceName() {
        return serviceName;
    }

    public MethodInfo getMethodInfo(Method method) {
        String mkey = RpcUtils.getMethodKey(method);
        return methodInfos.get(mkey);
    }

    public MethodInfo getMethodInfo(String methodKey) {
        return methodInfos.get(methodKey);
    }

    public Map<String, MethodInfo> getMethodInfos() {
        return methodInfos;
    }


}
