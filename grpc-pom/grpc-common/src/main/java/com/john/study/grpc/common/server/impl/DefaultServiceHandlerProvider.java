package com.john.study.grpc.common.server.impl;

import com.google.common.collect.Maps;
import com.john.study.grpc.common.server.ServiceHandlerProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author jianguangtao03382 2018/5/4
 */
public class DefaultServiceHandlerProvider implements ServiceHandlerProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultServiceHandlerProvider.class);
    private ReentrantLock lock = new ReentrantLock();
    /**
     * 接口名称到实现类的映射
     */
    private Map<String, Object> serviceNameImplMap = Maps.newHashMap();

    /**
     * 添加接口实现类
     *
     * @param serviceType 服务接口类型
     * @param serviceImpl 服务实现类
     */
    public void addServiceImpl(Class<?> serviceType, Object serviceImpl) {
        lock.lock();
        try {
            String serviceTypeName = serviceType.getName();
            if (serviceNameImplMap.containsKey(serviceTypeName)) {
                LOGGER.warn("重复的接口与实现类映射{}-->{}", serviceTypeName, serviceImpl);
                return;
            }
            serviceNameImplMap.put(serviceTypeName, serviceImpl);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public Object getServiceHandler(String serviceName) {
        return serviceNameImplMap.get(serviceName);
    }

    @Override
    public Object getServiceHandler(Class<?> serviceType) {
        return serviceNameImplMap.get(serviceType.getName());
    }
}
