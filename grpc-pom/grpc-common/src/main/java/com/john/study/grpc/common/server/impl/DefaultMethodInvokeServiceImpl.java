package com.john.study.grpc.common.server.impl;

import com.john.study.grpc.common.RpcConfigContext;
import com.john.study.grpc.common.exceptions.RpcException;
import com.john.study.grpc.common.exceptions.RpcInvokeServiceException;
import com.john.study.grpc.common.infos.MethodInfo;
import com.john.study.grpc.common.infos.ServiceInfo;
import com.john.study.grpc.common.server.MethodInvokeService;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Map;

/**
 * @author jianguangtao03382 2018/5/3
 */
public class DefaultMethodInvokeServiceImpl implements MethodInvokeService {
    private Object targetBean;

    public DefaultMethodInvokeServiceImpl(Object targetBean) {
        this.targetBean = targetBean;
    }

    @Override
    public Object invoke(Method method, Map<String, Object> args, Map<String, String> metas) {
        try {
            return method.invoke(targetBean, getInvokeArgs(method, args));
        } catch (IllegalAccessException e) {
            throw new RpcInvokeServiceException("invoke method:" + method.toString() + " illegal access", e);
        } catch (InvocationTargetException e) {
            throw new RpcInvokeServiceException("invoke method:" + method.toString() + " invocke target", e);
        }
    }

    private Object[] getInvokeArgs(Method method, Map<String, Object> args) {
        Object[] result = null;
        String serviceName = method.getDeclaringClass().getName();
        ServiceInfo serviceInfo = RpcConfigContext.getServiceInfo(serviceName);
        MethodInfo methodInfo = serviceInfo.getMethodInfo(method);
        if (methodInfo.isHasParameters()) {
            result = new Object[method.getParameterCount()];
            Parameter[] parameters = method.getParameters();
            for (int i = 0; i < parameters.length; i++) {

                Parameter parameter = parameters[i];
                String pkey = MethodInfo.getArgName(i);
                Object val = null;
                if (args.containsKey(pkey)) {
                    val = args.get(pkey);
                } else if (args.containsKey(parameter.getName())) {
                    val = args.get(parameter.getName());
                } else {
                    throw new RpcException("method " + method.toString() + " doesn't contains parameter "
                            + parameter.getName());
                }
                result[i] = val;
            }
        }

        return result;
    }


}
