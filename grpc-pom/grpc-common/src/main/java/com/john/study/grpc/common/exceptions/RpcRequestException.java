package com.john.study.grpc.common.exceptions;

/**
 * RPC请求异常
 *
 * @author jianguangtao03382 2018/5/3
 */
public class RpcRequestException extends RpcException {
    private static final long serialVersionUID = -6090328024018955295L;

    public RpcRequestException() {
    }

    public RpcRequestException(String message) {
        super(message);
    }

    public RpcRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public RpcRequestException(Throwable cause) {
        super(cause);
    }

    public RpcRequestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
