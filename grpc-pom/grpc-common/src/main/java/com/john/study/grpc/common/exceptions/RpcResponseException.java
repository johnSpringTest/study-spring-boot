package com.john.study.grpc.common.exceptions;

/**
 * RPC返回异常
 *
 * @author jianguangtao03382 2018/5/3
 */
public class RpcResponseException extends RpcException {
    private static final long serialVersionUID = -3637064065521175600L;

    public RpcResponseException() {
    }

    public RpcResponseException(String message) {
        super(message);
    }

    public RpcResponseException(String message, Throwable cause) {
        super(message, cause);
    }

    public RpcResponseException(Throwable cause) {
        super(cause);
    }

    public RpcResponseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
