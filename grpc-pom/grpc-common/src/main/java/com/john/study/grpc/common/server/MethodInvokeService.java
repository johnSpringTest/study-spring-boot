package com.john.study.grpc.common.server;

import java.lang.reflect.Method;
import java.util.Map;

/**
 * 方法调用服务类
 * @author jianguangtao03382 2018/5/3
 */
public interface MethodInvokeService {
    /**
     * 调用方法
     *
     * @param method 要调用的方法
     * @param args   调用参数map
     * @param metas  其它附加的消息
     * @return 调用结果
     * @throws com.john.study.grpc.common.exceptions.RpcException 出现任何调用异常时
     */
    Object invoke(Method method, Map<String, Object> args, Map<String, String> metas);
}
