package com.john.study.grpc.common;

import com.google.common.collect.Maps;
import com.john.study.grpc.common.client.nameresolver.GrpcNameResolverProvider;
import com.john.study.grpc.common.config.ClientConfigInfo;
import com.john.study.grpc.common.config.CommonConfigInfo;
import com.john.study.grpc.common.config.GrpcAppConfigInfo;
import com.john.study.grpc.common.config.ServerConfigInfo;
import com.john.study.grpc.common.consts.Consts;
import com.john.study.grpc.common.infos.ServiceInfo;
import com.john.study.grpc.common.util.RpcUtils;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 全局的配置
 *
 * @author jianguangtao03382 2018/5/4
 */
public class RpcConfigContext {
    private static final Logger LOGGER = LoggerFactory.getLogger(RpcConfigContext.class);
    private static final ReentrantLock app_lock = new ReentrantLock();

    private static final Map<String, ClientConfigInfo> appClientConfigs = Maps.newHashMap();
    private static final Map<String, ServiceInfo> serviceInfoMap = Maps.newHashMap();
    private static final Map<String, GrpcNameResolverProvider> appNameResolverMap = Maps.newHashMap();
    private static ServerConfigInfo serverConfigInfo;
    private static GrpcAppConfigInfo grpcAppConfigInfo = null;

    private RpcConfigContext() {

    }

    /**
     * 初始化应用环境参数
     *
     * @param appConfigInfo 配置参数
     */
    public static void initAppContext(GrpcAppConfigInfo appConfigInfo) {
        if (null != grpcAppConfigInfo) {
            return;
        }
        String serverName = null == appConfigInfo.getServerConfigInfo() ? null : appConfigInfo.getServerConfigInfo().getName();
        String appName = RpcUtils.getFirstNotEmptyVal(serverName, appConfigInfo.getName());

        if (StringUtils.isBlank(appName)) {
            throw new IllegalArgumentException("请指定应用名称");
        }
        if (null == appConfigInfo.getServerConfigInfo() && CollectionUtils.isEmpty(appConfigInfo.getClientConfigInfos())) {
            throw new IllegalArgumentException("服务或者客户端配置至少指定一个！");
        }
        app_lock.lock();
        try {
            if (null != grpcAppConfigInfo) {
                return;
            }
            grpcAppConfigInfo = appConfigInfo;

            CommonConfigInfo commonConfigInfo = appConfigInfo.getCommonConfigInfo();
            ServerConfigInfo serverConfigInfo = appConfigInfo.getServerConfigInfo();
            List<ClientConfigInfo> clientConfigInfos = appConfigInfo.getClientConfigInfos();

            appConfigInfo.setName(appName);

            if (appConfigInfo.isConfigClient()) {
                clientConfigInfos.forEach((clientConfigInfo -> initClientContext(commonConfigInfo, clientConfigInfo)));
            }
            if (appConfigInfo.isConfigServer()) {
                serverConfigInfo.setName(appName);
                initServerContext(commonConfigInfo, serverConfigInfo);
            }
        } finally {
            app_lock.unlock();
        }
    }

    private static void initServerContext(CommonConfigInfo commonConfigInfo, ServerConfigInfo serverConfigInfo) {
        if (StringUtils.isBlank(serverConfigInfo.getName())) {
            throw new IllegalArgumentException("必须指定应用名称");
        }
        ServerConfigInfo configInfo = new ServerConfigInfo();
        try {
            if (null != commonConfigInfo) {
                BeanUtils.copyProperties(configInfo, commonConfigInfo);
            }
            BeanUtils.copyProperties(configInfo, serverConfigInfo);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new IllegalArgumentException("配置参数Bean异常", e);
        }

        if (CollectionUtils.isEmpty(configInfo.getServices())) {
            throw new IllegalArgumentException("至少指定一个要暴露的服务接口！");
        }

        if (configInfo.getVersion() < 0) {
            LOGGER.warn("app version must greater or equal to 0");
            configInfo.setVersion(0);
        }
        if (configInfo.getConnectionTimeout() < -1) {
            configInfo.setConnectionTimeout(-1);
        }
        RpcConfigContext.serverConfigInfo = configInfo;
        initServiceInfo(configInfo.getServices());
    }

    /**
     * 初始化客户端上下文环境
     *
     * @param commonConfigInfo 共用配置信息
     * @param clientConfigInfo 客户端使用的配置信息
     */
    private static void initClientContext(CommonConfigInfo commonConfigInfo, ClientConfigInfo clientConfigInfo) {
        if (StringUtils.isBlank(clientConfigInfo.getName())) {
            throw new IllegalArgumentException("必须指定调用应用名称");
        }

        ClientConfigInfo configInfo = new ClientConfigInfo();
        try {
            if (null != commonConfigInfo) {
                BeanUtils.copyProperties(configInfo, commonConfigInfo);
            }
            BeanUtils.copyProperties(configInfo, clientConfigInfo);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new IllegalArgumentException("配置参数Bean异常", e);
        }

        if (CollectionUtils.isEmpty(configInfo.getServices())) {
            throw new IllegalArgumentException("至少指定一个待调用应用的服务接口！");
        }
        if (CollectionUtils.isEmpty(configInfo.getServerAddresses())) {
            throw new IllegalArgumentException("至少需要指定一个服务地址！");
        }

        if (configInfo.getVersion() < 0) {
            LOGGER.warn("app version must greater or equal to 0");
            configInfo.setVersion(0);
        }
        if (configInfo.getConnectionTimeout() < -1) {
            configInfo.setConnectionTimeout(-1);
        }

        GrpcNameResolverProvider nameResolverProvider = new GrpcNameResolverProvider(configInfo.getLoadBalancer(), configInfo.getServerAddresses());
        appNameResolverMap.put(configInfo.getName(), nameResolverProvider);

        appClientConfigs.put(clientConfigInfo.getName(), clientConfigInfo);
        initServiceInfo(configInfo.getServices());

//        ThreadPools.registerLimitedQueuePool(clientConfigInfo.getThreadPoolCoreSize(),
//                clientConfigInfo.getThreadPoolMaxSize(),
//                clientConfigInfo.getThreadAliveSeconds(), clientConfigInfo.getName(), false,
//                clientConfigInfo.getThreadQueueSize(), true);

    }

    private static void initServiceInfo(List<Class<?>> services) {
        services.forEach((service) -> {
            String serviceName = service.getName();
            if (!serviceInfoMap.containsKey(serviceName)) {
                serviceInfoMap.put(serviceName, ServiceInfo.from(service));
            } else {
                LOGGER.warn("重复的服务接口初始化！{}", serviceName);
            }
        });
    }

    /**
     * 获取连接超时时间
     *
     * @param appName          连接到的应用
     * @param serviceClassName 调用的服务接口
     * @param methodName       调用的方法
     * @return 超时时间 毫秒
     */
    public static int getConnectionTimeout(String appName, String serviceClassName, String methodName) {
        ClientConfigInfo configInfo = appClientConfigs.get(appName);
        if (null == configInfo) {
            LOGGER.warn("app {} 可能未初始化配置！未能获取到配置信息", appName);
            return Consts.DEFAULT_CONNECTION_TIMEOUT;
        }
        Map<String, Integer> methodTimeout = configInfo.getInterfaceMethodTimeout();
        if (null == methodTimeout || methodTimeout.size() == 0 || StringUtils.isBlank(serviceClassName)) {
            return configInfo.getConnectionTimeout();
        }
        // 服务名+方法名的超时配置
        String smkey = serviceClassName + "#" + methodName;
        if (methodTimeout.containsKey(smkey)) {
            return methodTimeout.get(smkey);
        } else if (methodTimeout.containsKey(serviceClassName)) {
            return methodTimeout.get(serviceClassName);
        }

        return configInfo.getConnectionTimeout();
    }

    /**
     * 获取指定名称的服务信息
     *
     * @param serviceName 服务名称
     * @return 服务信息
     */
    public static ServiceInfo getServiceInfo(String serviceName) {
        return serviceInfoMap.get(serviceName);
    }

    /**
     * 获取当前应用服务名称
     *
     * @return
     */
    public static String getAppName() {
        return serverConfigInfo.getName();
    }

    /**
     * 获取有效的服务端配置信息
     *
     * @return 服务端配置信息
     */
    public static ServerConfigInfo getServerConfigInfo() {
        return serverConfigInfo;
    }

    /**
     * 根据应用名称来获取客户端对应调用服务器的配置信息
     *
     * @param appName 应用服务名称
     * @return 客户端应用配置
     */
    public static ClientConfigInfo getClientConfigInfo(String appName) {
        return appClientConfigs.get(appName);
    }

    /**
     * 获取应用的地址解析器提供者
     *
     * @param appName 应用名称
     * @return 地址解析器提供者
     */
    public static GrpcNameResolverProvider getClientNameResolverProvider(String appName) {
        return appNameResolverMap.get(appName);
    }

    /**
     * 获取有效的客户端配置信息列表
     *
     * @return 客户端配置信息列表
     */
    public static List<ClientConfigInfo> getClientConfigInfoList() {
        Collection<ClientConfigInfo> values = appClientConfigs.values();
        if (CollectionUtils.isEmpty(values)) {
            return Collections.emptyList();
        }
        return new ArrayList<>(values);
    }
}
