package com.john.study.grpc.common.client;

import com.google.common.collect.Maps;
import com.john.common.util.threads.ThreadPools;
import com.john.study.grpc.common.RpcConfigContext;
import com.john.study.grpc.common.client.nameresolver.GrpcNameResolverProvider;
import com.john.study.grpc.common.config.ClientConfigInfo;
import com.john.study.grpc.common.consts.Consts;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author jianguangtao03382 2018/5/4
 */
public class ChannelPools {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChannelPools.class);
    private static final Map<String, ManagedChannel> serviceChannels = Maps.newConcurrentMap();
//    private static final String CHANNEL_THREAD_POOL_NAME = "grpc-channel";
    private static final ReentrantLock channel_lock = new ReentrantLock();

//    static {
//        // 所有的客户端使用一个线程池了
//        //核心线程2个，最大并发64，允许空闲时间1秒，线程队列大小1024.
//        ThreadPools.registerLimitedQueuePool(2, 64, 1,
//                CHANNEL_THREAD_POOL_NAME, false, 1024, true);
//    }

    /**
     * 为应用初始化客户端连接池
     *
     * @param appName 应用名称
     */
    public static void initChannel(String appName) {
        if (serviceChannels.containsKey(appName)) {
            LOGGER.warn("应用{}已经初始化过了", appName);
            return;
        }
        channel_lock.lock();
        try {
            if (serviceChannels.containsKey(appName)) {
                LOGGER.warn("应用{}已经初始化过了", appName);
                return;
            }

            //核心线程2个，最大并发64，允许空闲时间1秒，线程队列大小1024.
            ThreadPools.registerLimitedQueuePool(1, 32, 1,
                    appName, false, 1024, true);

            LOGGER.info("准备连接服务：{}", appName);
//            ClientConfigInfo clientConfigInfo = RpcConfigContext.getClientConfigInfo(appName);
            ManagedChannel channel = ManagedChannelBuilder.forTarget(appName)
//                    .executor(ThreadPools.getPoolService(CHANNEL_THREAD_POOL_NAME))
                    .executor(ThreadPools.getPoolService(appName))
                    .nameResolverFactory(RpcConfigContext.getClientNameResolverProvider(appName))
                    .usePlaintext()
                    .maxInboundMessageSize(Consts.DEFAULT_MAX_MESSAGE_SIZE)
                    .build();
            channel.getState(true);
            serviceChannels.put(appName, channel);
        } finally {
            channel_lock.unlock();
        }
    }

    public static ManagedChannel getChannel(String appName) {
        return serviceChannels.get(appName);
    }
}
