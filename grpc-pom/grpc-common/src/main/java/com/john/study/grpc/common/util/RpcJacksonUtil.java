package com.john.study.grpc.common.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * @author jianguangtao03382 2018/5/3
 */
public class RpcJacksonUtil {
    private static ObjectMapper objectMapper;
    private static ObjectMapper prettyObjectMapper;

    static {
//        SimpleModule module = new SimpleModule();
        objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .setSerializationInclusion(JsonInclude.Include.NON_NULL);

        prettyObjectMapper = objectMapper.copy().configure(SerializationFeature.INDENT_OUTPUT, true);
    }

    public static ObjectMapper getMapper() {
        return objectMapper;
    }

    public static ObjectMapper getPrettyMapper() {
        return prettyObjectMapper;
    }

}
