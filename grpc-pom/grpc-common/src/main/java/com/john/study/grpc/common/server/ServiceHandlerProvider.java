package com.john.study.grpc.common.server;

/**
 * 接口类实现类提供类
 * @author jianguangtao03382 2018/5/4
 */
public interface ServiceHandlerProvider {
    /**
     * 根据接口类名来查找处理实现类
     *
     * @param serviceName 接口服务名称
     * @return 处理实现类
     */
    Object getServiceHandler(String serviceName);

    /**
     * 根据接口类型来查找处理实现类
     *
     * @param serviceType 接口类型
     * @return 处理实现类
     */
    Object getServiceHandler(Class<?> serviceType);

}
