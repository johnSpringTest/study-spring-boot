package com.john.study.grpc.common.config;

import com.john.common.dao.data.data.BaseData;

/**
 * 能用的配置信息
 *
 * @author jianguangtao03382 2018/5/4
 */
public class CommonConfigInfo extends BaseData {
    private static final long serialVersionUID = -552792725145320477L;
    /**
     * 应用服务版本号 0表示默认的
     */
    private int version = 0;
    /**
     * 连接超时时间 默认为3秒
     */
    private int connectionTimeout;

    /**
     * 服务监听的核心线程数 默认为1
     */
    private int threadPoolCoreSize;
    /**
     * 最大执行线程数（并发数） 默认为64
     */
    private int threadPoolMaxSize;
    /**
     * 执行线程池大小
     */
    private int threadQueueSize;

    /**
     * 空闲线程等待时间
     */
    private int threadAliveSeconds;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public int getThreadPoolCoreSize() {
        return threadPoolCoreSize;
    }

    public void setThreadPoolCoreSize(int threadPoolCoreSize) {
        this.threadPoolCoreSize = threadPoolCoreSize;
    }

    public int getThreadPoolMaxSize() {
        return threadPoolMaxSize;
    }

    public void setThreadPoolMaxSize(int threadPoolMaxSize) {
        this.threadPoolMaxSize = threadPoolMaxSize;
    }

    public int getThreadQueueSize() {
        return threadQueueSize;
    }

    public void setThreadQueueSize(int threadQueueSize) {
        this.threadQueueSize = threadQueueSize;
    }

    public int getThreadAliveSeconds() {
        return threadAliveSeconds;
    }

    public void setThreadAliveSeconds(int threadAliveSeconds) {
        this.threadAliveSeconds = threadAliveSeconds;
    }
}
