package com.john.study.grpc.common.exceptions;

/**
 * RPC请求超时异常
 *
 * @author jianguangtao03382 2018/5/3
 */
public class RpcRequestTimeoutException extends RpcException {
    private static final long serialVersionUID = -6090328024018955295L;

    public RpcRequestTimeoutException() {
    }

    public RpcRequestTimeoutException(String message) {
        super(message);
    }

    public RpcRequestTimeoutException(String message, Throwable cause) {
        super(message, cause);
    }

    public RpcRequestTimeoutException(Throwable cause) {
        super(cause);
    }

    public RpcRequestTimeoutException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
