package com.john.study.grpc.common.infos;

import com.fasterxml.jackson.databind.JavaType;
import com.john.study.grpc.common.util.RpcJacksonUtil;
import com.john.study.grpc.common.util.RpcUtils;

import java.io.Serializable;
import java.lang.reflect.Parameter;

/**
 * 方法上的参数信息
 *
 * @author jianguangtao03382 2018/5/3
 */
public class ParameterInfo implements Serializable {
    private static final long serialVersionUID = -333638923235985016L;
    private int index;
    private String name;
    private Parameter parameter;
    private Object defaultValue;
    private JavaType paramJavaType;

    private ParameterInfo(Parameter parameter, int index) {
        this.parameter = parameter;
        this.index = index;
        this.defaultValue = RpcUtils.getTypeDefaultValue(parameter.getType());
        this.name = parameter.getName();
        this.paramJavaType = RpcJacksonUtil.getMapper().constructType(parameter.getType());
        //TODO 此处的名称是可以优化的
    }

    public static ParameterInfo from(Parameter parameter, int index) {
        return new ParameterInfo(parameter, index);
    }

    public int getIndex() {
        return index;
    }

    public Parameter getParameter() {
        return parameter;
    }

    public Object getDefaultValue() {
        return defaultValue;
    }

    public String getName() {
        return name;
    }

    public JavaType getParamJavaType() {
        return paramJavaType;
    }
}
