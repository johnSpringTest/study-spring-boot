package com.john.study.grpc.common.client;

import com.john.common.util.threads.ThreadPools;
import com.john.study.grpc.common.RpcRequestContext;
import com.john.study.grpc.common.RpcConfigContext;
import com.john.study.grpc.common.exceptions.RpcException;
import com.john.study.grpc.common.exceptions.RpcRequestException;
import com.john.study.grpc.common.exceptions.RpcRequestTimeoutException;
import com.john.study.grpc.common.infos.MethodInfo;
import com.john.study.grpc.common.infos.ServiceInfo;
import com.john.study.grpc.common.internal.GrpcInvokeRequest;
import com.john.study.grpc.common.internal.GrpcInvokeResponse;
import com.john.study.grpc.common.internal.GrpcInvokerServiceGrpc;
import com.john.study.grpc.common.util.RpcUtils;
import io.grpc.ManagedChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * @author jianguangtao03382 2018/5/4
 */
public class ClientInvocationImpl implements InvocationHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientInvocationImpl.class);
    private String appName;

    public ClientInvocationImpl(String appName) {
        this.appName = appName;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Future<GrpcInvokeResponse> responseFuture = null;

        ManagedChannel channel = ChannelPools.getChannel(appName);
        if (channel == null) {
            throw new RpcException("app " + appName + "的执行连接池未初始化！");
        }

        try {
            String serviceClassName = method.getDeclaringClass().getName();
            String methodName = method.getName();
            ServiceInfo serviceInfo = RpcConfigContext.getServiceInfo(serviceClassName);
            MethodInfo methodInfo = serviceInfo.getMethodInfo(method);

            int connectionTimeout = RpcConfigContext.getConnectionTimeout(appName, serviceClassName, methodName);
            RpcRequestContext.prepareCall(appName, serviceClassName, methodName);
            GrpcInvokeRequest request = buildRequest(methodInfo, args, serviceClassName);

            GrpcInvokerServiceGrpc.GrpcInvokerServiceBlockingStub blockingStub = GrpcInvokerServiceGrpc.newBlockingStub(channel);
            blockingStub.withDeadlineAfter(connectionTimeout, TimeUnit.MILLISECONDS);

//        blockingStub.withOption()

            RpcRequestContext.RpcCallStack callStack = RpcRequestContext.getCallStack();
            logRequest(request);
            RpcRequestContext.startCall();
            responseFuture = ThreadPools.getPoolService(appName).submit(new Callable<GrpcInvokeResponse>() {
                @Override
                public GrpcInvokeResponse call() throws Exception {
                    RpcRequestContext.fromCallStack(callStack);
                    return blockingStub.grpcInvoke(request);
                }
            });
            GrpcInvokeResponse response = responseFuture.get(connectionTimeout, TimeUnit.MILLISECONDS);
            RpcRequestContext.endCall();
            logResponse(response);
            // FIXME 处理异常返回

            return RpcUtils.parseResponseJson(methodInfo, response.getData());
        } catch (InterruptedException | TimeoutException e) {
            if (null != responseFuture) {
                responseFuture.cancel(true);
            }
            throw new RpcRequestTimeoutException("call " + appName + " call timeout!", e);
        } catch (Throwable e) {
            throw new RpcRequestException(e.getMessage(), e);
        } finally {
            RpcRequestContext.clearCall();
        }
    }

    /**
     * 记录开始请求了
     * @param request 请求参数
     */
    private void logRequest(final GrpcInvokeRequest request) {
        final RpcRequestContext.CallInfo callInfo = RpcRequestContext.getCallInfo();
        ThreadPools.getLoggerPool().submit(() -> {
            // request hellWord.hello args:[]
            LOGGER.debug("[{},{}] request {}#{} args:{}", callInfo.getRpcId(), callInfo.getReqId(),
                    callInfo.getService(), callInfo.getMethod(), request.getData());
        });
    }

    /**
     * 记录请求调用返回了
     * @param response 返回结果
     */
    private void logResponse(final GrpcInvokeResponse response) {
        final RpcRequestContext.CallInfo callInfo = RpcRequestContext.getCallInfo();
        final long durations = callInfo.getEndTime()-callInfo.getStartTime();
        ThreadPools.getLoggerPool().submit(() -> {
            // request hellWord.hello response:[]
            LOGGER.debug("[{},{}] ({}) response {}#{} datas:{}", callInfo.getRpcId(), callInfo.getReqId(), durations,
                    callInfo.getService(), callInfo.getMethod(), response.getData());
        });
    }

    private GrpcInvokeRequest buildRequest(MethodInfo methodInfo, Object[] args, String serviceClassName) {
        String json = RpcUtils.getArgsJson(methodInfo, args);

        return GrpcInvokeRequest.newBuilder()
                .setRpcId(RpcRequestContext.getRpcId())
                .setReqId(RpcRequestContext.getReqId())
                .setIface(serviceClassName)
                .setMethod(methodInfo.getMethodKey())
                .setData(json)
                .build();
    }
}
