package com.john.study.grpc.common.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.type.MapLikeType;
import com.google.common.collect.Maps;
import com.john.study.grpc.common.exceptions.RpcCodecParameterException;
import com.john.study.grpc.common.exceptions.RpcInvokeServiceException;
import com.john.study.grpc.common.exceptions.RpcResponseException;
import com.john.study.grpc.common.infos.MethodInfo;
import com.john.study.grpc.common.infos.ParameterInfo;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author jianguangtao03382 2018/5/3
 */
public class RpcUtils {
    private RpcUtils() {

    }

    /**
     * 获取第一个非空字符串
     * @param strs 候选字符串列表
     * @return
     */
    public static String getFirstNotEmptyVal(String... strs) {
        if (null == strs || strs.length == 0) {
            return null;
        }
        for (String str : strs) {
            if (StringUtils.isNotBlank(str)) {
                return str;
            }
        }
        return null;
    }

    /**
     * 获取方法名称的key
     * <p>
     * methodName#ParameterType1,ParameterType2>returnType
     * methodName#void>returnType
     *
     * @param method 方法对象
     * @return 方法名称的key
     */
    public static String getMethodKey(Method method) {
        StringBuilder sb = new StringBuilder();
        sb.append(method.getName()).append("(");
        if (method.getParameterCount() > 0) {
            for (Class<?> ptype : method.getParameterTypes()) {
                sb.append(ptype.getName()).append(",");
            }
            sb.deleteCharAt(sb.length() - 1);
        } else {
            sb.append("void");
        }
        sb.append(")");
        Class<?> returnType = method.getReturnType();
        sb.append(returnType.getName());
        return sb.toString();
    }

    /**
     * 获取指定类型的默认值
     *
     * @param clazz 类型
     * @return 默认值
     */
    public static Object getTypeDefaultValue(Class<?> clazz) {
        if (clazz == boolean.class) {
            return false;
        } else if (clazz == char.class) {
            return (char) 0;
        } else if (clazz == byte.class) {
            return (byte) 0;
        } else if (clazz == short.class) {
            return (short) 0;
        } else if (clazz == int.class) {
            return (int) 0;
        } else if (clazz == long.class) {
            return (long) 0;
        } else if (clazz == float.class) {
            return (float) 0;
        } else if (clazz == double.class) {
            return (double) 0;
        } else {
            return null;
        }
    }

    /**
     * 根据方法的请求参数生成请求调用的JSON数据
     *
     * @param methodInfo 调用的方法对象
     * @param args       调用参数列表
     * @return 请求调用的JSON数据
     */
    public static String getArgsJson(MethodInfo methodInfo, Object[] args) {
        if (null == args || !methodInfo.isHasParameters()) {
            return "";
        }
        if (methodInfo.getParameterCount() != args.length) {
            throw new RpcInvokeServiceException("调用服务方法" + methodInfo.getMethodKey() + " 的参数个数不匹配！");
        }
        try {
            Map<String, String> params = Maps.newHashMap();
            for (int i = 0; i < args.length; i++) {
                Object arg = args[i];
                String val = null;
                if (null != arg) {
                    val = RpcJacksonUtil.getMapper().writeValueAsString(arg);
                }
                params.put(methodInfo.getParamName(i), val);
            }
            return RpcJacksonUtil.getMapper().writeValueAsString(params);
        } catch (JsonProcessingException e) {
            throw new RpcCodecParameterException("序列化请求参数异常", e);
        }
    }

    /**
     * 将请求的JSON形式的参数转换为本地类型参数
     *
     * @param methodInfo 请求的方法
     * @param jsonArgs   请求的参数json数据
     * @return 方法参数的本地类型参数
     */
    public static Map<String, Object> parseArgJson(MethodInfo methodInfo, String jsonArgs) {
        Map<String, Object> params = new HashMap<>();
        try {
            MapLikeType mapLikeType = RpcJacksonUtil.getMapper().getTypeFactory().constructMapLikeType(HashMap.class, String.class, String.class);
            Map<String, String> jsonMap = RpcJacksonUtil.getMapper().readValue(jsonArgs, mapLikeType);

            for (Map.Entry<String, ParameterInfo> entry : methodInfo.getParameterInfos().entrySet()) {
                String argv = jsonMap.get(entry.getKey());
                if (null == argv) {
                    params.put(entry.getKey(), entry.getValue().getDefaultValue());
                } else {
                    Object val = RpcJacksonUtil.getMapper().readValue(argv, entry.getValue().getParamJavaType());
                    params.put(entry.getKey(), val);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        return params;
    }

    /**
     * 将返回的结果对象转换为json字符串
     *
     * @param methodInfo 方法
     * @param val        方法的本地执行结果
     * @return 结果json字符串
     */
    public static String getResponseJson(MethodInfo methodInfo, Object val) {
        if (methodInfo.isVoidReturn()) {
            return StringUtils.EMPTY;
        }
        if (null == val) {
            return null;
        }
        try {
            return RpcJacksonUtil.getMapper().writeValueAsString(val);
        } catch (JsonProcessingException e) {
            throw new RpcResponseException("转换返回结果到JSON时异常", e);
        }
    }

    /**
     * 解析rpc请求返回的json数据
     *
     * @param methodInfo 方法信息
     * @param jsonData   返回结果数据
     * @return 对应于此方法的返回值
     */
    public static Object parseResponseJson(MethodInfo methodInfo, String jsonData) {
        if (null == jsonData) {
//            throw new RpcResponseException("返回结果json为null");
            return null;
        }
        if (methodInfo.isVoidReturn()) {
            return null;
        }
        try {
            return RpcJacksonUtil.getMapper().readValue(jsonData, methodInfo.getReturnJavaType());
        } catch (IOException e) {
            throw new RpcResponseException("解析返回结果：" + jsonData + " 异常", e);
        }
    }


    /**
     * 获取一个UUID对象
     *
     * @return 一个uuid值
     */
    public static String getRandomUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }


}
