package com.john.study.grpc.common.client.nameresolver;

import com.john.study.grpc.common.consts.Consts;
import com.john.study.grpc.common.exceptions.RpcException;
import io.grpc.Attributes;
import io.grpc.EquivalentAddressGroup;
import io.grpc.NameResolver;
import io.grpc.NameResolverProvider;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jianguangtao03382 2018/5/4
 */
public class GrpcNameResolverProvider extends NameResolverProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(GrpcNameResolverProvider.class);

    private GrpcNameResolver nameResolver = new GrpcNameResolver();

    private int loadBalancer = 0;
    private List<String> addresses;

    private List<EquivalentAddressGroup> equivalentAddresses;


    public GrpcNameResolverProvider(int loadBalancer, List<String> addresses) {
        this.loadBalancer = loadBalancer;
        this.addresses = addresses;
        this.equivalentAddresses = transformAddress(addresses);
    }

    public void refreshServers(int loadBalancer, List<String> addresses) {
        this.loadBalancer = loadBalancer;
        this.addresses = addresses;
        this.equivalentAddresses = transformAddress(addresses);
        nameResolver.refreshServers(this.loadBalancer, this.equivalentAddresses);
    }

    /**
     * 将配置的字符串地址，转换为有效的地址格式
     * @param addresses 配置的地址
     * @return
     */
    private List<EquivalentAddressGroup> transformAddress(List<String> addresses) {
        List<EquivalentAddressGroup> list = new ArrayList<>();
        for (String address : addresses) {
            // ip:port:wight
            String[] addArr = StringUtils.splitByWholeSeparatorPreserveAllTokens(address, ":");
            if (addArr.length < 2) {
                LOGGER.error("错误的服务地址配置信息 {},,正确格式： 127.0.0.1:29234", address);
                continue;
            }
            int weight = 100;
            String host = addArr[0];
            if (StringUtils.isBlank(host)) {
                host = "localhost";
            }
            int port = Integer.parseInt(addArr[1]);
            try {
                if (addArr.length > 2) {
                    weight = Integer.parseInt(addArr[2]);
                    if (weight > 100) {
                        weight = 100;
                    }
                    if (weight < 0) {
                        weight = 0;
                    }
                }
            } catch (NumberFormatException e) {
                LOGGER.warn("未能解析的负载均衡配置值：{}", addArr[2]);
            }

            SocketAddress socketAddress = new InetSocketAddress(host, port);

            Attributes attrs = Attributes.newBuilder().set(Consts.GrpcAttrs.ADDRESS_WEIGHT, weight).build();
            EquivalentAddressGroup addressGroup = new EquivalentAddressGroup(socketAddress, attrs);
            list.add(addressGroup);
        }
        if (CollectionUtils.isEmpty(list)) {
            throw new RpcException("未能解析出有效的服务地址！");
        }
        return list;
    }


    @Override
    protected boolean isAvailable() {
        return true;
    }

    @Override
    protected int priority() {
        return 0;
    }

    @Nullable
    @Override
    public NameResolver newNameResolver(URI targetUri, Attributes params) {
        nameResolver.init(loadBalancer, equivalentAddresses);
        return nameResolver;
    }

    @Override
    public String getDefaultScheme() {
        return null;
    }

    public List<EquivalentAddressGroup> getEquivalentAddresses() {
        return equivalentAddresses;
    }
}
