package com.john.study.grpc.common.server;

import com.john.common.util.threads.ThreadPools;
import com.john.study.grpc.common.config.ServerConfigInfo;
import com.john.study.grpc.common.consts.Consts;
import com.john.study.grpc.common.server.impl.DefaultServiceHandlerProvider;
import io.grpc.Server;
import io.grpc.ServerInterceptors;
import io.grpc.netty.NettyServerBuilder;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * @author jianguangtao03382 2018/5/3
 */
public class GrpcServerHelpler {
    private static final Logger LOGGER = LoggerFactory.getLogger(GrpcServerHelpler.class);
    public static final int SYS_CORES = Runtime.getRuntime().availableProcessors();
    private static final int TRIBLE_SYS_CORES = SYS_CORES * 3;
    /**
     * HTTP2 flow control window size
     */
    private static final int FLOW_CONTROLL_WINDOW = 5 * 1024 * 1024; //
    /**
     * 最大消息体大小
     */
    private static final int MAX_MESSAGE_SIZE = Consts.DEFAULT_MAX_MESSAGE_SIZE;
    /**
     * 允许最大线程队列大小
     */
    private static final int MAX_THREAD_QUEUE_SIZE = Consts.MAX_THREAD_QUEUE_SIZE;

    /**
     * 启动一个服务
     *
     * @param configInfo 服务器端配置参数
     * @return 启动的服务器对象
     * @throws IOException 启动失败
     */
    public static Server startServer(ServerConfigInfo configInfo,
                                     ServiceHandlerProvider serviceHandlerProvider) throws IOException {

        GrpcServerHandler serverHandler = new GrpcServerHandler(serviceHandlerProvider);
        ThreadPools.registerLimitedQueuePool(getThreadPoolCoreSize(configInfo), getThreadPoolSize(configInfo), getThreadAliveSeconds(configInfo),
                configInfo.getName(), false, getThreadQueueSize(configInfo), true);

        LOGGER.info("listen on port:{}", configInfo.getPort());
        return NettyServerBuilder.forPort(configInfo.getPort())
                .addService(ServerInterceptors.intercept(serverHandler, new RpcServerInterceptor()))
                .executor(ThreadPools.getPoolService(configInfo.getName()))
                .flowControlWindow(FLOW_CONTROLL_WINDOW)
                .maxMessageSize(MAX_MESSAGE_SIZE)
                .build()
                .start()
                ;
    }

    private static int getThreadPoolCoreSize(ServerConfigInfo configInfo) {
        int size = configInfo.getThreadPoolCoreSize();
        if (size < 0) {
            size = 0;
        } else {
            int allowMax = Math.max(1, SYS_CORES);
            if (size > allowMax) {
                size = allowMax;
            }
        }
        return size;
    }

    private static int getThreadQueueSize(ServerConfigInfo configInfo) {
        int size = configInfo.getThreadQueueSize();
        if (size < 128) {
            size = 128;
        } else if (size > MAX_THREAD_QUEUE_SIZE) {
            size = MAX_THREAD_QUEUE_SIZE;
        }
        return size;
    }

    private static int getThreadAliveSeconds(ServerConfigInfo configInfo) {
        int threadAliveSeconds = configInfo.getThreadAliveSeconds();
        if (threadAliveSeconds < 0) {
            threadAliveSeconds = 0;
        }
        return threadAliveSeconds;
    }

    private static int getThreadPoolSize(ServerConfigInfo configInfo) {
        int size = configInfo.getThreadPoolMaxSize();
        if (size < 1) {
            size = 1;
        } else if (size > TRIBLE_SYS_CORES) {
            size = TRIBLE_SYS_CORES;
        }
        return size;
    }


}
