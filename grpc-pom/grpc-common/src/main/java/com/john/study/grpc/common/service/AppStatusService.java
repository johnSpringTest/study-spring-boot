package com.john.study.grpc.common.service;

/**
 * 获取应用服务的状态服务接口，每一个服务必须有一个实现
 *
 * @author jianguangtao03382 2018/5/3
 */
public interface AppStatusService {
    /**
     * 获取服务状态
     *
     * @return 状态码
     */
    String getStatus();
}
