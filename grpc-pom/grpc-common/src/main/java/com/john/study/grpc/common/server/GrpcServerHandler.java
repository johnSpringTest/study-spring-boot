package com.john.study.grpc.common.server;

import com.john.common.util.threads.ThreadPools;
import com.john.study.grpc.common.RpcConfigContext;
import com.john.study.grpc.common.RpcRequestContext;
import com.john.study.grpc.common.consts.GrpcCodeEnum;
import com.john.study.grpc.common.infos.MethodInfo;
import com.john.study.grpc.common.infos.ServiceInfo;
import com.john.study.grpc.common.internal.GrpcInvokeRequest;
import com.john.study.grpc.common.internal.GrpcInvokeResponse;
import com.john.study.grpc.common.internal.GrpcInvokerServiceGrpc;
import com.john.study.grpc.common.util.RpcUtils;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * @author jianguangtao03382 2018/5/3
 */
public class GrpcServerHandler extends GrpcInvokerServiceGrpc.GrpcInvokerServiceImplBase {
    private static final Logger LOGGER = LoggerFactory.getLogger(GrpcServerHandler.class);
    private ServiceHandlerProvider serviceHandlerProvider;

    public GrpcServerHandler(ServiceHandlerProvider serviceHandlerProvider) {
        this.serviceHandlerProvider = serviceHandlerProvider;
    }

    @Override
    public void grpcInvoke(GrpcInvokeRequest request, StreamObserver<GrpcInvokeResponse> responseObserver) {
        RpcRequestContext.fromRequest(request);
        logStartProcess(request);
        Object result = null;
        try {
            ServiceInfo serviceInfo = RpcConfigContext.getServiceInfo(request.getIface());
            if (null == serviceInfo) {
                errorCodeReturn(responseObserver, GrpcCodeEnum.SERVICE_NOT_FOUND.getCode(), GrpcCodeEnum.SERVICE_NOT_FOUND.getMsg());
                return;
            }
            MethodInfo methodInfo = serviceInfo.getMethodInfo(request.getMethod());
            if (null == methodInfo) {
                errorCodeReturn(responseObserver, GrpcCodeEnum.SERVICE_METHOD_NOT_FOUND.getCode(), GrpcCodeEnum.SERVICE_METHOD_NOT_FOUND.getMsg());
                return;
            }

            Object serviceHandler = serviceHandlerProvider.getServiceHandler(serviceInfo.getInterfaceName());
            Object[] args = buildMethodArgs(methodInfo, request.getData());
            result = methodInfo.getMethod().invoke(serviceHandler, args);

            String respJson = RpcUtils.getResponseJson(methodInfo, result);

            GrpcInvokeResponse response = GrpcInvokeResponse.newBuilder()
                    .setReqId(RpcRequestContext.getReqId())
                    .setCode(GrpcCodeEnum.OK.getCode())
                    .setMsg(GrpcCodeEnum.OK.getMsg())
                    .setData(respJson)
                    .build();
            responseObserver.onNext(response);
            responseObserver.onCompleted();
            RpcRequestContext.endCall();
        } catch (Exception e) {
            errorCodeReturn(responseObserver, GrpcCodeEnum.SERVER_ERROR.getCode(), e.getMessage());
            LOGGER.error("处理请求失败", e);
        } finally {
            RpcRequestContext.endCall();
            logProcessTime(result);
            RpcRequestContext.clearCall();
        }
    }

    /**
     * 记录接收到请求了
     *
     * @param request 请求对象
     */
    private void logStartProcess(final GrpcInvokeRequest request) {
        final RpcRequestContext.CallInfo callInfo = RpcRequestContext.getCallInfo();
        ThreadPools.getLoggerPool().submit(() -> {
            LOGGER.debug("[{},{}] receive {}#{} reqArgs:{}", callInfo.getRpcId(), callInfo.getReqId(), callInfo.getService(),
                    callInfo.getMethod(), request.getData());
        });
    }

    /**
     * 记录返回值
     *
     * @param result 本地执行的返回结果
     */
    private void logProcessTime(Object result) {
        final RpcRequestContext.CallInfo callInfo = RpcRequestContext.getCallInfo();
        final long durations = callInfo.getEndTime() - callInfo.getStartTime();
        ThreadPools.getLoggerPool().submit(() -> {
            LOGGER.debug("[{},{}] ({}) {}#{} response:{}", callInfo.getRpcId(), callInfo.getReqId(), durations,
                    callInfo.getService(), callInfo.getMethod(), result);
        });
    }

    /**
     * 构建本地方法调用参数
     *
     * @param methodInfo      要请求的方法
     * @param requestJsonData 请求的json方法参数
     * @return 参数数组
     */
    private Object[] buildMethodArgs(MethodInfo methodInfo, String requestJsonData) {
        if (methodInfo.isHasParameters()) {
            Map<String, Object> params = RpcUtils.parseArgJson(methodInfo, requestJsonData);
            Object[] args = new Object[methodInfo.getParameterCount()];
            for (int i = 0; i < methodInfo.getParameterCount(); i++) {
                args[i] = params.get(methodInfo.getParamName(i));
            }
            return args;
        }

        return null;
    }

    /**
     * 返回代码信息，直接结束流
     *
     * @param responseObserver 数据写回对象
     * @param code             异常代码
     * @param msg              异常描述
     */
    private void errorCodeReturn(StreamObserver<GrpcInvokeResponse> responseObserver, String code, String msg) {
        GrpcInvokeResponse response = GrpcInvokeResponse.newBuilder()
                .setReqId(RpcRequestContext.getReqId())
                .setCode(code)
                .setMsg(msg)
                .build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
