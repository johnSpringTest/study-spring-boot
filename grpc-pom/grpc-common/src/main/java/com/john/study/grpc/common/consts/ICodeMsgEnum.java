package com.john.study.grpc.common.consts;

/**
 * @author jianguangtao03382 2018/5/3
 */
public interface ICodeMsgEnum<E extends Enum<?>> {
    /**
     * 获取代码
     *
     * @return 代码
     */
    String getCode();

    /**
     * 获取消息
     *
     * @return 消息
     */
    String getMsg();

    /**
     * 是否相等
     *
     * @param code 另一个代码值
     * @return true相同
     */
    boolean equals(String code);
}
