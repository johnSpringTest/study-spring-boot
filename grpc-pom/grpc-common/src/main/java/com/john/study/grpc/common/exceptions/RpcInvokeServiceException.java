package com.john.study.grpc.common.exceptions;

/**
 * @author jianguangtao03382 2018/5/3
 */
public class RpcInvokeServiceException extends RpcException {
    private static final long serialVersionUID = -6938254601032204988L;

    public RpcInvokeServiceException() {
    }

    public RpcInvokeServiceException(String message) {
        super(message);
    }

    public RpcInvokeServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public RpcInvokeServiceException(Throwable cause) {
        super(cause);
    }

    public RpcInvokeServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
