package com.john.study.grpc.common.config;

import java.util.List;

/**
 * 服务端配置信息
 *
 * @author jianguangtao03382 2018/5/3
 */
public class ServerConfigInfo extends CommonConfigInfo {
    private static final long serialVersionUID = 1452970396714801221L;
    /**
     * 服务名称
     */
    private String name;
    /**
     * 监听端口
     */
    private int port;
    /**
     * 监听地址，一般不写
     */
    private String host;
    /**
     * 要暴露的服务接口列表
     */
    private List<Class<?>> services;
    /**
     * 接收的消息体允许的最大大小 字节
     */
    private Integer maxMessageBytes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public List<Class<?>> getServices() {
        return services;
    }

    public void setServices(List<Class<?>> services) {
        this.services = services;
    }

    public Integer getMaxMessageBytes() {
        return maxMessageBytes;
    }

    public void setMaxMessageBytes(Integer maxMessageBytes) {
        this.maxMessageBytes = maxMessageBytes;
    }
}
