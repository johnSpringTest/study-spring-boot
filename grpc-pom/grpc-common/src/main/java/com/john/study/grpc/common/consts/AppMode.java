package com.john.study.grpc.common.consts;

/**
 * 应用的启动模式
 *
 * @author jianguangtao03382 2018/5/4
 */
public enum AppMode {
    /**
     * 单独的服务器模式， 只对外提供服务，不依赖于其它服务的
     */
    SERVER,
    /**
     * 单独的客户端模式，只使用服务，而不对外提供服务
     */
    CLIENT,
    /**
     * 既使用服务，也提供服务
     */
    BOTH
}
