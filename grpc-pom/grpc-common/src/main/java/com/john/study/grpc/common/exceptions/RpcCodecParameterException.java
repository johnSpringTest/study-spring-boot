package com.john.study.grpc.common.exceptions;

/**
 * @author jianguangtao03382 2018/5/4
 */
public class RpcCodecParameterException extends RpcException {
    private static final long serialVersionUID = 1738023802155058786L;

    public RpcCodecParameterException() {
    }

    public RpcCodecParameterException(String message) {
        super(message);
    }

    public RpcCodecParameterException(String message, Throwable cause) {
        super(message, cause);
    }

    public RpcCodecParameterException(Throwable cause) {
        super(cause);
    }

    public RpcCodecParameterException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
