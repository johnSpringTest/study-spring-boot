package com.john.study.grpc.common.util;

import com.john.study.grpc.common.exceptions.RpcException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * @author jianguangtao03382 2018/5/4
 */
public class ClassFinder {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClassFinder.class);
    /**
     * 接受所有的实现类
     */
    public static ClassFilter TRUE_FILTER = clazz -> true;

    /**
     * 在指定包列表下查询类
     *
     * @param packages    包路径列表
     * @param classFilter 类过滤器
     * @return 查询出的类列表
     */
    public static List<Class<?>> find(List<String> packages, ClassFilter classFilter) {
        Set<Class<?>> result = new LinkedHashSet<>();
        ClassLoader cl = ClassFinder.class.getClassLoader();

        for (String pkg : packages) {
            Set<Class<?>> pkgSet = findPkg(cl, pkg, classFilter, result);
            if (CollectionUtils.isNotEmpty(pkgSet)) {
                result.addAll(pkgSet);
            }
        }

        return new ArrayList<>(result);
    }

    private static Set<Class<?>> findPkg(ClassLoader cl, final String packageName, ClassFilter classFilter,
                                         Set<Class<?>> result) {
        if (StringUtils.isBlank(packageName)) {
            return null;
        }

        String findPath = packageName.replace('.', '/');

        try {
            Enumeration<URL> resources = cl.getResources(findPath);
            while (resources.hasMoreElements()) {
                URL url = resources.nextElement();
                if ("file".equalsIgnoreCase(url.getProtocol())) {
                    // 获取包的物理路径
                    String filePath = URLDecoder.decode(url.getFile(), "UTF-8");
                    findFileClass(cl, packageName, filePath, result, classFilter);
                } else if ("jar".equalsIgnoreCase(url.getProtocol())) {
                    findJarClass(cl, packageName, findPath, url, result, classFilter);
                }
            }

        } catch (IOException | ClassNotFoundException e) {
            throw new RpcException("初始化扫描接口实现类时异常", e);
        }

        return result;
    }

    private static void findJarClass(ClassLoader cl, String packageName, String packageDirName, URL url,
                                     Set<Class<?>> result, ClassFilter classFilter) throws IOException, ClassNotFoundException {

        JarFile jarFile = ((JarURLConnection) url.openConnection()).getJarFile();
        Enumeration<JarEntry> entries = jarFile.entries();
        while (entries.hasMoreElements()) {
            JarEntry jarEntry = entries.nextElement();
            String name = jarEntry.getName();
            if (name.startsWith("/")) {
                name = name.substring(1);
            }

            // 如果前半部分和定义的包名相同
            if (name.startsWith(packageDirName)) {
                int idx = name.lastIndexOf('/');
                // 如果以"/"结尾 是一个包
                if (idx != -1) {
                    // 获取包名 把"/"替换成"."
                    packageName = name.substring(0, idx).replace('/', '.');
                }
                // 如果可以迭代下去
                if ((idx != -1)) {
                    // 如果是一个.class文件 而且不是目录
                    if (name.endsWith(".class") && !jarEntry.isDirectory()) {
                        // 去掉后面的".class" 获取真正的类名
                        String className = name.substring(packageName.length() + 1, name.length() - 6);
                        Class<?> loadedClass = cl.loadClass(className);
                        if (classFilter.filter(loadedClass)) {
                            result.add(loadedClass);
                            LOGGER.debug("扫描出一个类：{}", loadedClass);
                        }
                    }
                }
            }
        }
    }

    /**
     * 在本地文件资源中查询类
     *
     * @param cl          类加载器
     * @param packageName 当前包路径
     * @param findPath    查询文件路径
     * @param result      扫描的类文件保存处
     * @param classFilter 类过滤规则
     * @throws ClassNotFoundException 加载类时未找到
     */
    private static void findFileClass(ClassLoader cl, final String packageName, final String findPath,
                                      Set<Class<?>> result,
                                      ClassFilter classFilter) throws ClassNotFoundException {
        File filePathFile = new File(findPath);
        if (!filePathFile.exists()) {
            return;
        }

        if (!filePathFile.isDirectory()) {
            if (!filePathFile.getName().endsWith(".class")) {
                return;
            }
            String className = FilenameUtils.getBaseName(filePathFile.getName());
            className = packageName + "." + className;
            Class<?> loadedClass = cl.loadClass(className);

            if (classFilter.filter(loadedClass)) {
                result.add(loadedClass);
                LOGGER.debug("扫描出一个类：{}", loadedClass);
            }
        } else {
            // 目录
            File[] files = filePathFile.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return dir.isDirectory() || name.endsWith(".class");
                }
            });
            if (null != files && files.length > 0) {
                for (File file : files) {
                    if (file.isDirectory()) {
                        String nextPkgName = packageName + "." + file.getName();
                        findFileClass(cl, nextPkgName, file.getPath(), result, classFilter);
                    } else {
                        findFileClass(cl, packageName, file.getPath(), result, classFilter);
                    }
                }
            }
        }
    }

    /**
     * 扫描出的类文件过滤器
     */
    public static interface ClassFilter {
        /**
         * true表示接受，false不接受
         *
         * @param clazz 类对象
         * @return true表示接受，false不接受
         */
        boolean filter(Class<?> clazz);
    }
}
