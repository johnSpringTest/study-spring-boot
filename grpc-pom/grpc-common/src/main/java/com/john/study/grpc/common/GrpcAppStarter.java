package com.john.study.grpc.common;

import com.john.study.grpc.common.client.GrpcClientHelper;
import com.john.study.grpc.common.config.ClientConfigInfo;
import com.john.study.grpc.common.config.GrpcAppConfigInfo;
import com.john.study.grpc.common.config.ServerConfigInfo;
import com.john.study.grpc.common.server.GrpcServerHelpler;
import com.john.study.grpc.common.server.ServiceHandlerProvider;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author jianguangtao03382 2018/5/2
 */
public abstract class GrpcAppStarter {
    protected static final Logger LOGGER = LoggerFactory.getLogger(GrpcAppStarter.class);

    /**
     * 根据配置信息启动服务
     *
     * @param appConfigInfo          配置信息
     * @throws Exception 启动时出现异常了
     */
    public void startApp(GrpcAppConfigInfo appConfigInfo) throws Exception {
        RpcConfigContext.initAppContext(appConfigInfo);

        ServerConfigInfo serverConfigInfo = RpcConfigContext.getServerConfigInfo();
        if (null != serverConfigInfo) {

            LOGGER.info("准备启动服务：{}", serverConfigInfo.getName());
            GrpcServerHelpler.startServer(serverConfigInfo, getServiceHandlerProvider());
        }

        List<ClientConfigInfo> clientConfigInfoList = RpcConfigContext.getClientConfigInfoList();
        if (CollectionUtils.isNotEmpty(clientConfigInfoList)) {
            clientConfigInfoList.forEach(clientConfigInfo -> {
                LOGGER.info("初始化客户端连接到服务{}", clientConfigInfo.getName());
                GrpcClientHelper.initClient(clientConfigInfo);
            });
        }
    }

    /**
     * 获取服务接口的实现类提供者
     *
     * @return 服务接口的实现类提供者
     */
    protected abstract ServiceHandlerProvider getServiceHandlerProvider();
}
