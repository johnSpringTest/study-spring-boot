package com.john.study.grpc.common.infos;

import com.fasterxml.jackson.databind.JavaType;
import com.john.study.grpc.common.util.RpcJacksonUtil;
import com.john.study.grpc.common.util.RpcUtils;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.HashMap;
import java.util.Map;

/**
 * 接口中包含的方法信息
 *
 * @author jianguangtao03382 2018/5/3
 */
public class MethodInfo implements Serializable {
    private static final long serialVersionUID = -4381713620345108822L;
    /**
     * 原始方法信息
     */
    private Method method;
    /**
     * 此方法的签名key值
     */
    private String methodKey;
    /**
     * 方法的名称
     */
    private String methodName;

    /**
     * 方法的参数信息
     */
    private Map<String, ParameterInfo> parameterInfos;
    /**
     * 此方法上的参数个数统计
     */
    private int parameterCount = 0;
    /**
     * 指定位置的参数名称
     */
    private Map<Integer, String> indexParamName;
    /**
     * 方法的返回JavaType类型(应用于JaskconJSON)
     */
    private JavaType returnJavaType;

    public static MethodInfo from(Method method) {
        MethodInfo info = new MethodInfo();
        info.method = method;
        info.parseMethodInfo();
        return info;
    }

    /**
     * 获取指定序号的参数名称
     *
     * @param index 参数顺序序号
     * @return 指定顺序的参数名称
     */
    public static String getArgName(int index) {
        return "arg" + index;
    }

    private void parseMethodInfo() {
        methodName = method.getName();
        parameterCount = method.getParameterCount();
        Parameter[] parameters = method.getParameters();

        if (parameterCount > 0) {
            parameterInfos = new HashMap<>();
            indexParamName = new HashMap<>();
            for (int i = 0; i < parameterCount; i++) {
                Parameter parameter = parameters[i];
                ParameterInfo pi = ParameterInfo.from(parameter, i);

                parameterInfos.put(pi.getName(), pi);
                indexParamName.put(i, pi.getName());
            }
        }
        this.methodKey = RpcUtils.getMethodKey(method);

        if (!isVoidReturn()) {
            returnJavaType = RpcJacksonUtil.getMapper().constructType(method.getReturnType());
        }
    }

    public Method getMethod() {
        return method;
    }

    public String getMethodName() {
        return methodName;
    }

    public String getMethodKey() {
        return methodKey;
    }

    public int getParameterCount() {
        return parameterCount;
    }

    public boolean isHasParameters() {
        return parameterCount > 0;
    }

    public Map<String, ParameterInfo> getParameterInfos() {
        return parameterInfos;
    }

    /**
     * 获取指定索引位置的参数名称
     *
     * @param index 参数索引位置
     * @return 参数名称
     */
    public String getParamName(int index) {
        return indexParamName.get(index);
    }

    /**
     * 是否是空的返回对象
     *
     * @return true是
     */
    public boolean isVoidReturn() {
        Class<?> returnType = method.getReturnType();
        return void.class == returnType || Void.class == returnType;
    }

    /**
     * 获取返回值的JacksonJson使用的Java类型
     * <p>
     * 只有在方法 {@link #isVoidReturn()} 返回 {@code false} 时才会有此结果
     *
     * @return JacksonJson使用的Java类型
     */
    public JavaType getReturnJavaType() {
        return returnJavaType;
    }
}
