package com.john.study.grpc.common.server;

import com.john.study.grpc.common.consts.Consts;
import io.grpc.Context;
import io.grpc.Contexts;
import io.grpc.Grpc;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;

import java.net.SocketAddress;

/**
 * @author jianguangtao03382 2018/5/3
 */
public class RpcServerInterceptor implements ServerInterceptor {
    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> call,
                                                                 Metadata headers, ServerCallHandler<ReqT, RespT> next) {
        SocketAddress remoteAddress = call.getAttributes().get(Grpc.TRANSPORT_ATTR_REMOTE_ADDR);
        Context ctx = Context.current()
                .withValue(Consts.GrpcCtxKeys.CLIENT_IP, remoteAddress);
        return Contexts.interceptCall(ctx, call, headers, next);
    }
}
