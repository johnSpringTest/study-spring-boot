package com.john.study.grpc.common.client;

import com.google.common.collect.Maps;
import com.john.study.grpc.common.config.ClientConfigInfo;
import com.john.study.grpc.common.exceptions.RpcUndeclaredInterfaceFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Proxy;
import java.util.List;
import java.util.Map;

/**
 * @author jianguangtao03382 2018/5/4
 */
public class GrpcClientHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(GrpcClientHelper.class);
    /**
     * 服务接口与应用名称的对应关系
     */
    private static final Map<Class<?>, String> clients = Maps.newConcurrentMap();

    /**
     * 初始客户端信息
     *
     * @param configInfo 客户端配置信息
     */
    public static void initClient(ClientConfigInfo configInfo) {
        String appName = configInfo.getName();
        ChannelPools.initChannel(appName);

        List<Class<?>> services = configInfo.getServices();
        for (Class<?> service : services) {
            if (null != clients.putIfAbsent(service, appName)) {
                LOGGER.warn("duplicated interface {} found!", service);
            }
        }
    }

    public static <T> T getClient(Class<T> interfaceType) {
        return getClient(interfaceType, null);
    }

    public static <T> T getClient(Class<T> interfaceType, Map<String, String> metas) {
        String appName = clients.get(interfaceType);
        if (null == appName) {
            throw new RpcUndeclaredInterfaceFoundException(interfaceType.getName());
        }
        ClassLoader classLoader = GrpcClientHelper.class.getClassLoader();
        ClientInvocationImpl clientInvocation = new ClientInvocationImpl(appName);
        return (T) Proxy.newProxyInstance(classLoader, new Class[]{interfaceType}, clientInvocation);
    }
}
