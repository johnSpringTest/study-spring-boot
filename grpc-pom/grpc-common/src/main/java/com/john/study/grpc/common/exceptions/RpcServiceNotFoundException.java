package com.john.study.grpc.common.exceptions;

/**
 * @author jianguangtao03382 2018/5/3
 */
public class RpcServiceNotFoundException extends RpcException {
    private static final long serialVersionUID = -4440544129084798850L;

    /**
     * 接口在指定的服务不存在异常
     *
     * @param serviceName   服务名称
     * @param interfaceName 接口名称
     */
    public RpcServiceNotFoundException(String serviceName, String interfaceName) {
        super("interface " + interfaceName + " not exists in service " + serviceName);
    }
}
