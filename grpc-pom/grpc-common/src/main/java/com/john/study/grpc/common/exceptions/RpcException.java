package com.john.study.grpc.common.exceptions;

/**
 * @author jianguangtao03382 2018/5/3
 */
public class RpcException extends RuntimeException {
    private static final long serialVersionUID = 2451110571233132113L;

    public RpcException() {
    }

    public RpcException(String message) {
        super(message);
    }

    public RpcException(String message, Throwable cause) {
        super(message, cause);
    }

    public RpcException(Throwable cause) {
        super(cause);
    }

    public RpcException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
