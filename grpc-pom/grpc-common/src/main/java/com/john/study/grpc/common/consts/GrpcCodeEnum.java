package com.john.study.grpc.common.consts;

/**
 * @author jianguangtao03382 2018/5/3
 */
public enum GrpcCodeEnum implements ICodeMsgEnum<GrpcCodeEnum> {
    OK("200", "成功"),
    SERVER_ERROR("500", "服务器出错"),
    TIMEOUT_ERROR("501", "请示超时"),
    SERVICE_NOT_FOUND("510", "请求的服务不存在"),
    SERVICE_METHOD_NOT_FOUND("511", "请求的服务不存在")
    ;


    private String code;
    private String msg;

    GrpcCodeEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }

    @Override
    public boolean equals(String code) {
        return this.code.equals(code);
    }
}
