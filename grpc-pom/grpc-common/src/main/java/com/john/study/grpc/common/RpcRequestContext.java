package com.john.study.grpc.common;

import com.john.common.dao.data.data.BaseData;
import com.john.study.grpc.common.internal.GrpcInvokeRequest;
import com.john.study.grpc.common.util.RpcUtils;

import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * RPC请求上下文环境
 *
 * @author jianguangtao03382 2018/5/4
 */
public class RpcRequestContext {
    private static final ThreadLocal<RpcCallStack> threadCallStackInfo = new ThreadLocal<>();

    /**
     * 准备开始一个新的调用  初始化调用信息
     * <p>
     * 需要在 {@link #startCall()} 方法之前调用
     *
     * @param appName          应用名称
     * @param serviceClassName 服务名称
     * @param method           方法名称
     */
    public static void prepareCall(String appName, String serviceClassName, String method) {
        CallInfo callInfo = null;
        if (hasCallInfo()) {
            callInfo = getCallInfo().getSubCallInfo();
        } else {
            callInfo = CallInfo.initOne();
        }

        callInfo.setAppName(appName);
        callInfo.setService(serviceClassName);
        callInfo.setMethod(method);

        getCallStack().offer(callInfo);
    }

    /**
     * 开始方法调用 设置开始调用时间、Rpc调用次数
     */
    public static void startCall() {
        getCallInfo().setStartTime(System.currentTimeMillis());
        incrementCall();
    }

    /**
     * 结束方法调用 设置方法结束时间
     */
    public static void endCall() {
        getCallInfo().setEndTime(System.currentTimeMillis());
    }

    /**
     * 结束方法调用后的清理 清空当前方法调用信息
     * <p>
     * 一般在final中执行
     */
    public static void clearCall() {
        getCallStack().pollLast();
    }

    /**
     * 获取当前Rpc调用栈信息
     *
     * @return 当前Rpc调用栈信息
     */
    public static RpcCallStack getCallStack() {
        RpcCallStack stack = threadCallStackInfo.get();
        if (null == stack) {
            threadCallStackInfo.set(new RpcCallStack());
            stack = threadCallStackInfo.get();
        }
        return stack;
    }

    /**
     * 获取Rpc调用id
     *
     * @return Rpc调用id
     */
    public static String getRpcId() {
        return getCallInfo().getRpcId();
    }

    /**
     * 获取请求id
     *
     * @return 请求id
     */
    public static String getReqId() {
        return getCallInfo().getReqId();
    }

    /**
     * 多线程中的，把上级线程的context移入到子线程中
     * <p>
     * 如果在子线程中还有单独的rpc调用，也是需要再次使用此方法的
     *
     * @param callStack 上线调用信息
     */
    public static void fromCallStack(RpcCallStack callStack) {
        threadCallStackInfo.set(callStack);
    }

    /**
     * 是否有调用信息
     *
     * @return true 有
     */
    public static boolean hasCallInfo() {
        return null != getCallStack().peekLast();
    }

    /**
     * 获取当前调用信息
     *
     * @return 当前调用信息
     */
    public static CallInfo getCallInfo() {
        return getCallStack().peekLast();
    }

    private static void incrementCall() {
        getCallStack().incrementReqTimes();
    }

    /**
     * 根据接受到的请求生成一个调用栈
     *
     * @param request rpc请求对象
     */
    public static void fromRequest(GrpcInvokeRequest request) {
        CallInfo callInfo = new CallInfo();
        callInfo.setRpcId(request.getRpcId());
        callInfo.setReqId(request.getReqId());
        callInfo.setService(request.getIface());
        String method = request.getMethod();
        if (method.indexOf("(") > 0) {
            method = method.substring(0, method.indexOf("("));
        }
        callInfo.setMethod(method);
        callInfo.setStartTime(System.currentTimeMillis());

        getCallStack().offer(callInfo);
    }


    public static class RpcCallStack extends LinkedList<CallInfo> {
        private static final long serialVersionUID = 1696662766340476282L;
        private AtomicInteger reqTimes = new AtomicInteger(0);

        RpcCallStack() {
        }

        public int getReqTimes() {
            return reqTimes.get();
        }

        int incrementReqTimes() {
            return reqTimes.incrementAndGet();
        }
    }

    /**
     * 调用信息
     */
    public static class CallInfo extends BaseData {
        private static final long serialVersionUID = -7572856241490884233L;
        /**
         * 总的请求ID
         */
        private String rpcId;
        /**
         * 当前请求ID 在一个rpcId内可能会发生多次rpc请求
         */
        private String reqId;
        /**
         * 准备调用的应用名称 准备阶段
         */
        private String appName;
        /**
         * 服务名称 开始的时候
         */
        private String service;
        /**
         * 方法名称 开始的时候
         */
        private String method;
        /**
         * 调用开始时的时间戳
         */
        private Long startTime;
        /**
         * 调用结束时间戳
         */
        private Long endTime;

        /**
         * 初始化一个新的调用信息
         *
         * @return 一个新的调用信息
         */
        static CallInfo initOne() {
            CallInfo info = new CallInfo();
            info.setRpcId(RpcUtils.getRandomUUID());
            info.setReqId(RpcUtils.getRandomUUID());
            return info;
        }

        /**
         * 获取子调用信息
         *
         * @return 子调用信息
         */
        CallInfo getSubCallInfo() {
            CallInfo info = new CallInfo();
            info.setRpcId(this.rpcId);
            return info;
        }

        public String getRpcId() {
            return rpcId;
        }

        void setRpcId(String rpcId) {
            this.rpcId = rpcId;
        }

        public String getReqId() {
            return reqId;
        }

        void setReqId(String reqId) {
            this.reqId = reqId;
        }

        public String getAppName() {
            return appName;
        }

        public void setAppName(String appName) {
            this.appName = appName;
        }

        public String getService() {
            return service;
        }

        public void setService(String service) {
            this.service = service;
        }

        public String getMethod() {
            return method;
        }

        public void setMethod(String method) {
            this.method = method;
        }

        public Long getStartTime() {
            return startTime;
        }

        void setStartTime(Long startTime) {
            this.startTime = startTime;
        }

        public Long getEndTime() {
            return endTime;
        }

        void setEndTime(Long endTime) {
            this.endTime = endTime;
        }
    }
}
