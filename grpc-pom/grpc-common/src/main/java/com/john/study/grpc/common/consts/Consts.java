package com.john.study.grpc.common.consts;


import io.grpc.Attributes;
import io.grpc.Context;

import java.net.SocketAddress;

/**
 * @author jianguangtao03382 2018/5/3
 */
public interface Consts {
    /**
     * 最大消息体大小 20M
     */
    int DEFAULT_MAX_MESSAGE_SIZE = 20 * 1024 * 1024;

    /**
     * 允许最大线程队列大小
     */
    int MAX_THREAD_QUEUE_SIZE = 1000000;
    /**
     * 连接的默认超时时间 5秒
     */
    int DEFAULT_CONNECTION_TIMEOUT = 5000;

    interface GrpcAttrs {

        Attributes.Key<Integer> SUBGROUP_CHANNEL_SIZE = Attributes.Key.of("subGroupChannel");
        Attributes.Key<Integer> SUBGROUP_LOAD_BALANCER = Attributes.Key.of("subGroupLoadBalancer");

        Attributes.Key<Integer> ADDRESS_WEIGHT = Attributes.Key.of("addressWeight");



    }

    interface GrpcCtxKeys {
        /**
         * 客户端IP
         */
        Context.Key<SocketAddress> CLIENT_IP = Context.key("clientIp");
    }
}
