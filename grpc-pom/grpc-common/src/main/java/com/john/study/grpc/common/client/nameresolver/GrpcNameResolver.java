package com.john.study.grpc.common.client.nameresolver;

import com.john.study.grpc.common.consts.Consts;
import io.grpc.Attributes;
import io.grpc.EquivalentAddressGroup;
import io.grpc.NameResolver;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jianguangtao03382 2018/5/4
 */
public class GrpcNameResolver extends NameResolver {
    private static final Logger LOGGER = LoggerFactory.getLogger(GrpcNameResolver.class);
    private List<EquivalentAddressGroup> servers = new ArrayList<>();
    private int loadBalance;
    private Listener listener;

    void init(int loadBalance, List<EquivalentAddressGroup> servers) {
        this.loadBalance = loadBalance;
        this.servers = servers;
    }

    @Override
    public String getServiceAuthority() {
        return StringUtils.EMPTY;
    }

    @Override
    public void start(Listener listener) {
        this.listener = listener;
        resolve(loadBalance);
    }

    @Override
    public void shutdown() {

    }

    @Override
    public void refresh() {
        LOGGER.info("refresh name resolver");
        super.refresh();
        resolve(loadBalance);
    }

    public void refreshServers(int loadBalance, List<EquivalentAddressGroup> servers) {
        this.loadBalance = loadBalance;
        this.servers = servers;
        resolve(loadBalance);
    }


    private void resolve(int loadBalance) {
        Attributes.Builder builder = Attributes.newBuilder();
        builder.set(Consts.GrpcAttrs.SUBGROUP_CHANNEL_SIZE, 5);
        builder.set(Consts.GrpcAttrs.SUBGROUP_LOAD_BALANCER, loadBalance);
        listener.onAddresses(servers, builder.build());
    }
}
