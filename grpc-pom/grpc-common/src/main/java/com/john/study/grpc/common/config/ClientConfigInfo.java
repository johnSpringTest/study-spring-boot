package com.john.study.grpc.common.config;

import java.util.List;
import java.util.Map;

/**
 * 单个客户端调用服务时的配置信息，
 * <p>
 * 一般一个服务引用的服务数可能是多个的，因此当前对象主要用于列表形式的。
 *
 * @author jianguangtao03382 2018/5/3
 */
public class ClientConfigInfo extends CommonConfigInfo {
    private static final long serialVersionUID = -8867557201420960103L;
    /**
     * 服务名称
     */
    private String name;
    /**
     * 接口列表
     */
    private List<Class<?>> services;
    /**
     * 多个服务器地址的负载均衡策略
     */
    private int loadBalancer;
    /**
     * 服务器地址
     */
    private List<String> serverAddresses;
    /**
     * 接口独立配置的超时时间(毫秒)，最多精确到方法级别
     */
    private Map<String, Integer> interfaceMethodTimeout;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Class<?>> getServices() {
        return services;
    }

    public void setServices(List<Class<?>> services) {
        this.services = services;
    }

    public int getLoadBalancer() {
        return loadBalancer;
    }

    public void setLoadBalancer(int loadBalancer) {
        this.loadBalancer = loadBalancer;
    }

    public List<String> getServerAddresses() {
        return serverAddresses;
    }

    public void setServerAddresses(List<String> serverAddresses) {
        this.serverAddresses = serverAddresses;
    }

    public Map<String, Integer> getInterfaceMethodTimeout() {
        return interfaceMethodTimeout;
    }

    public void setInterfaceMethodTimeout(Map<String, Integer> interfaceMethodTimeout) {
        this.interfaceMethodTimeout = interfaceMethodTimeout;
    }
}
