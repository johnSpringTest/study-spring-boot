package com.john.study.grpc.common.infos;

import com.john.study.grpc.common.exceptions.RpcServiceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 服务信息
 * <p>
 * 一个服务是对外提供的一组服务接口列表的组合
 *
 * @author jianguangtao03382 2018/5/3
 */
public class AppInfo implements Serializable {
    public static final Logger LOGGER = LoggerFactory.getLogger(AppInfo.class);
    private static final long serialVersionUID = 1247573836715467040L;
    /**
     * 提供的服务版本
     */
    private int version;
    /**
     * 当前服务的名称
     */
    private String appName;
    /**
     * 服务接口名称-->服务接口定义的map映射
     */
    private Map<String, ServiceInfo> serviceInfos = new HashMap<>();

    /**
     * @param appName    应用名称
     * @param interfaces 服务接口列表
     */
    public AppInfo(String appName, List<Class<?>> interfaces) {
        this.appName = appName;
        interfaces.forEach((clazz) -> {
            serviceInfos.put(clazz.getName(), ServiceInfo.from(clazz));
        });
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getAppName() {
        return appName;
    }

    /**
     * 根据接口名称获取服务中的接口信息定义
     *
     * @param clazzName 接口类名称
     * @return 接口服务定义
     */
    public ServiceInfo getInterfaceInfo(String clazzName) {
        ServiceInfo info = serviceInfos.get(clazzName);
        if (null == info) {
            throw new RpcServiceNotFoundException(appName, clazzName);
        }
        return info;
    }
}
