package com.john.study.grpc.common.exceptions;

/**
 * @author jianguangtao03382 2018/5/3
 */
public class RpcInterfaceDuplicateException extends RpcException {
    private static final long serialVersionUID = -4440544129084798850L;

    /**
     * 出现重复的接口类定义
     *
     * @param serviceName 服务接口名称
     */
    public RpcInterfaceDuplicateException(String serviceName) {
        super("duplicate service " + serviceName);
    }
}
