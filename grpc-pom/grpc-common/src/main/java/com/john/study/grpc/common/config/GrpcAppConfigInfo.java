package com.john.study.grpc.common.config;

import com.john.common.dao.data.data.BaseData;
import com.john.study.grpc.common.consts.AppMode;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;

/**
 * 应用的配置信息
 *
 * @author jianguangtao03382 2018/5/4
 */
public class GrpcAppConfigInfo extends BaseData {
    private static final long serialVersionUID = -3839918096657710170L;
    /**
     * 应用的名称，一般也是默认的服务器的名称（如果ServerConfigInfo未指定）
     */
    private String name;
    private Boolean register;
    private AppMode appMode = AppMode.BOTH;
    private CommonConfigInfo commonConfigInfo;
    private ServerConfigInfo serverConfigInfo;
    private List<ClientConfigInfo> clientConfigInfos;

    public GrpcAppConfigInfo() {
    }

    /**
     * 应用的名称
     *
     * @param name     应用名称
     * @param register 是否向注册中心注册
     */
    public GrpcAppConfigInfo(String name, boolean register, AppMode appMode) {
        this.name = name;
        this.register = register;
        this.appMode = appMode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getRegister() {
        return register;
    }

    public void setRegister(Boolean register) {
        this.register = register;
    }

    public AppMode getAppMode() {
        return appMode;
    }

    public void setAppMode(AppMode appMode) {
        this.appMode = appMode;
    }

    public CommonConfigInfo getCommonConfigInfo() {
        return commonConfigInfo;
    }

    public void setCommonConfigInfo(CommonConfigInfo commonConfigInfo) {
        this.commonConfigInfo = commonConfigInfo;
    }

    public ServerConfigInfo getServerConfigInfo() {
        return serverConfigInfo;
    }

    public void setServerConfigInfo(ServerConfigInfo serverConfigInfo) {
        this.serverConfigInfo = serverConfigInfo;
    }

    public List<ClientConfigInfo> getClientConfigInfos() {
        return clientConfigInfos;
    }

    public void setClientConfigInfos(List<ClientConfigInfo> clientConfigInfos) {
        this.clientConfigInfos = clientConfigInfos;
    }

    /**
     * 是否进行配置服务端
     *
     * @return true是
     */
    public boolean isConfigServer() {
        return ((AppMode.SERVER.equals(appMode) || AppMode.BOTH.equals(appMode)) && null != serverConfigInfo);
    }

    /**
     * 是否进行客户端配置
     *
     * @return true是
     */
    public boolean isConfigClient() {
        return ((AppMode.CLIENT.equals(appMode) || AppMode.BOTH.equals(appMode)) && CollectionUtils.isNotEmpty(clientConfigInfos));
    }

}
