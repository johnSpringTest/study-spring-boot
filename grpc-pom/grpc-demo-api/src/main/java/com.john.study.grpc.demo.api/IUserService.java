package com.john.study.grpc.demo.api;

import com.john.study.grpc.demo.api.data.UserInfoVo;

import java.util.Date;
import java.util.List;

/**
 * @author jianguangtao03382 2018/5/3
 */
public interface IUserService {

    boolean checkUserExists(String username);

    UserInfoVo findByUsername(String username);

    UserInfoVo findById(Long id);

    UserInfoVo findById(Long id, String status);

    List<UserInfoVo> queryUserList(UserInfoVo queryVo);

    Long createUser(String username, int age, Date birthDay);

    void updateUser(UserInfoVo vo);
}
