package com.john.study.grpc.demo.api.data;

import com.john.common.dao.data.data.BaseData;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.Date;

/**
 * @author jianguangtao03382 2018/5/3
 */
public class UserInfoVo extends BaseData {
    private static final long serialVersionUID = 7706468100410238219L;
    private Long id;
    private String username;
    private int age;
    private Date birthDay;
    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
