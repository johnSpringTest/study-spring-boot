package com.john.study.grpc.demo.api.data;

import com.john.common.dao.data.data.IKeyTextEnum;

/**
 * @author jianguangtao03382 2018/5/7
 */
public enum UserStatusEnum implements IKeyTextEnum<UserStatusEnum>{
    NORMAL("1", "正常"),
    DELETED("2", "删除");

    private String key;
    private String text;

    UserStatusEnum(String key, String text) {
        this.key = key;
        this.text = text;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public boolean equals(String key) {
        return this.key.equals(key);
    }

}
