package com.john.demo.test.grpc.client;

import com.john.common.util.DateUtil;
import com.john.study.grpc.boot.config.EnableGrpcApp;
import com.john.study.grpc.common.client.GrpcClientHelper;
import com.john.study.grpc.demo.api.IUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author jianguangtao03382 2018/5/7
 */
@EnableGrpcApp
@SpringBootTest
@SpringBootConfiguration
@RunWith(SpringRunner.class)
public class FirstClientTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(FirstClientTest.class);

    @Test
    public void testStartUp() throws InterruptedException {
        LOGGER.info("服务启动完成！");
        Thread.sleep(5000L);
    }


    @Test
    public void testAddUser() {
        IUserService userService = GrpcClientHelper.getClient(IUserService.class);
        LOGGER.info("准备创建用户！");
        Long id = userService.createUser("john", 18, DateUtil.currentTime());
        LOGGER.info("新用户id：{}", id);

    }

}
