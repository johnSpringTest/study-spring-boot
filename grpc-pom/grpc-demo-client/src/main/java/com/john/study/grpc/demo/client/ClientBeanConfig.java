package com.john.study.grpc.demo.client;

import com.john.study.grpc.common.consts.GrpcCodeEnum;
import com.john.study.grpc.common.server.ServiceHandlerProvider;
import com.john.study.grpc.common.server.impl.DefaultServiceHandlerProvider;
import com.john.study.grpc.common.service.AppStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author jianguangtao03382 2018/5/7
 */
@Configuration
public class ClientBeanConfig {
    @Autowired
    private AppStatusService appStatusService;

    @Bean
    @ConditionalOnMissingBean
    public AppStatusService appStatusService() {
        return new AppStatusService() {
            @Override
            public String getStatus() {
                return GrpcCodeEnum.OK.getCode();
            }
        };
    }

    @Bean
    public ServiceHandlerProvider serviceHandlerProvider() {
        DefaultServiceHandlerProvider provider = new DefaultServiceHandlerProvider();
        provider.addServiceImpl(AppStatusService.class, appStatusService);

        return provider;
    }

}
