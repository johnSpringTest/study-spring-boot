package com.john.study.grpc.demo.client;

import com.john.study.grpc.common.config.ClientConfigInfo;
import com.john.study.grpc.common.config.GrpcAppConfigInfo;
import com.john.study.grpc.common.config.ServerConfigInfo;
import com.john.study.grpc.common.service.AppStatusService;
import com.john.study.grpc.demo.api.IUserService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author jianguangtao03382 2018/5/7
 */
@SpringBootApplication
public class GrpcDemoClientApplication implements CommandLineRunner{

    @Autowired
    private GrpcClientStarter clientStarter;

    @Value("${spring.application.name:GrpcDemoClient}")
    private String appName;


    public static void main(String[] args) {
        SpringApplication.run(GrpcDemoClientApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        GrpcAppConfigInfo appConfig = new GrpcAppConfigInfo();
        ServerConfigInfo serverConfig = new ServerConfigInfo();
        appConfig.setServerConfigInfo(serverConfig);

        serverConfig.setName(appName);
        serverConfig.setPort(13244);
        List<Class<?>> services = Collections.singletonList(AppStatusService.class);
        serverConfig.setServices(services);

        ClientConfigInfo clientConfig = new ClientConfigInfo();
        clientConfig.setName("simpleGrpcServer");
        clientConfig.setServerAddresses(Collections.singletonList("127.0.0.1:13233"));
        List<Class<?>> clientServices = new ArrayList<>();
        clientServices.add(IUserService.class);
        clientConfig.setServices(clientServices);


        List<ClientConfigInfo> clientConfigInfos = new ArrayList<>();
        clientConfigInfos.add(clientConfig);
        appConfig.setClientConfigInfos(clientConfigInfos);

        clientStarter.startApp(appConfig);
        Thread.currentThread().join();
    }
}
