package com.john.study.grpc.demo.client;

import com.john.study.grpc.common.GrpcAppStarter;
import com.john.study.grpc.common.server.ServiceHandlerProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author jianguangtao03382 2018/5/7
 */
@Component
public class GrpcClientStarter extends GrpcAppStarter {
    @Autowired
    private ServiceHandlerProvider serviceHandlerProvider;


    @Override
    protected ServiceHandlerProvider getServiceHandlerProvider() {
        return serviceHandlerProvider;
    }
}
