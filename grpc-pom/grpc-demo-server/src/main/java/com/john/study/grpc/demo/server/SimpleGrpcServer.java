package com.john.study.grpc.demo.server;

import com.john.study.grpc.common.GrpcAppStarter;
import com.john.study.grpc.common.config.GrpcAppConfigInfo;
import com.john.study.grpc.common.config.ServerConfigInfo;
import com.john.study.grpc.common.server.ServiceHandlerProvider;
import com.john.study.grpc.common.server.impl.DefaultServiceHandlerProvider;
import com.john.study.grpc.common.service.AppStatusService;
import com.john.study.grpc.demo.api.IUserService;
import com.john.study.grpc.demo.server.service.impl.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jianguangtao03382 2018/5/4
 */
public class SimpleGrpcServer extends GrpcAppStarter {
    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleGrpcServer.class);

    public static void main(String[] args) throws Exception {
        GrpcAppConfigInfo appConfig = new GrpcAppConfigInfo();
        ServerConfigInfo serverConfig = new ServerConfigInfo();
        appConfig.setServerConfigInfo(serverConfig);
        serverConfig.setPort(13233);
        serverConfig.setName("simpleGrpcServer");
        List<Class<?>> services = new ArrayList<>();
        services.add(AppStatusService.class);
        services.add(IUserService.class);

        serverConfig.setServices(services);

        SimpleGrpcServer server = new SimpleGrpcServer();
        server.startApp(appConfig);
        LOGGER.info("服务已经启动");
        Thread.currentThread().join();
    }

    @Override
    protected ServiceHandlerProvider getServiceHandlerProvider() {
        DefaultServiceHandlerProvider provider  = new DefaultServiceHandlerProvider();

        provider.addServiceImpl(IUserService.class, new UserServiceImpl());

        return provider;
    }
}
