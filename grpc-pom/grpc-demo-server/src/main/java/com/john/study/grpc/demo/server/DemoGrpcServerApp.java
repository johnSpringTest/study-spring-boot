package com.john.study.grpc.demo.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author jianguangtao03382 2018/5/3
 */
@SpringBootApplication
public class DemoGrpcServerApp implements CommandLineRunner {
    private static final Logger LOGGER = LoggerFactory.getLogger(DemoGrpcServerApp.class);

    public static void main(String[] args) {
        LOGGER.info("准备启动main方法！");
        SpringApplication.run(DemoGrpcServerApp.class, args);
        LOGGER.info("启动main方法结束！");
    }

    @Override
    public void run(String... args) throws Exception {
        LOGGER.info("进入run方法");
    }
}
