package com.john.study.grpc.demo.server.service.impl;

import com.google.common.collect.Maps;
import com.john.common.util.RelaxedBeanCopy;
import com.john.study.grpc.demo.api.IUserService;
import com.john.study.grpc.demo.api.data.UserInfoVo;
import com.john.study.grpc.demo.api.data.UserStatusEnum;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author jianguangtao03382 2018/5/7
 */
public class UserServiceImpl implements IUserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
    private static AtomicLong userIdLong = new AtomicLong(1);

    private Map<String, UserInfoVo> userNameUserMap = Maps.newHashMap();
    private Map<Long, UserInfoVo> userIdUserMap = Maps.newHashMap();


    @Override
    public boolean checkUserExists(String username) {
        return userNameUserMap.containsKey(username);
    }

    @Override
    public UserInfoVo findByUsername(String username) {
        return userNameUserMap.get(username);
    }

    @Override
    public UserInfoVo findById(Long id) {
        return userIdUserMap.get(id);
    }

    @Override
    public UserInfoVo findById(Long id, String status) {
        UserInfoVo vo = findById(id);
        if (null != vo && StringUtils.isNotBlank(status) && status.equals(vo.getStatus())) {
            return vo;
        }
        return null;
    }

    @Override
    public List<UserInfoVo> queryUserList(UserInfoVo queryVo) {
        return new ArrayList<>(userIdUserMap.values());
    }

    @Override
    public Long createUser(String username, int age, Date birthDay) {
        if (null != findByUsername(username)) {
            throw new IllegalArgumentException("指定名称的用户已经存在！");
        }

        UserInfoVo vo = new UserInfoVo();
        vo.setId(userIdLong.incrementAndGet());
        vo.setUsername(username);
        vo.setAge(age);
        vo.setBirthDay(birthDay);
        vo.setStatus(UserStatusEnum.NORMAL.getKey());

        userNameUserMap.put(vo.getUsername(), vo);
        userIdUserMap.put(vo.getId(), vo);

        return vo.getId();
    }

    @Override
    public void updateUser(UserInfoVo vo) {
        if (null == vo.getId()) {
            throw new IllegalArgumentException("更新时必须指定用户id！");
        }
        UserInfoVo dbvo = findById(vo.getId());
        if (null == dbvo) {
            LOGGER.error("用户不存在 {}", vo.getId());
            return;
        }
        LOGGER.debug("原数据：{}", dbvo);
        LOGGER.debug("更新参数：{}", vo);
        RelaxedBeanCopy.create(dbvo)
                .ignores("id")
                .copyFrom(vo);
    }
}
