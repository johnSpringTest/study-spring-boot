package com.john.study.grpc.boot.config.vo;

/**
 * @author jianguangtao03382 2018/5/3
 */
public class ServerConfigVo extends CommonConfigVo {
    private static final long serialVersionUID = 8526118032942610025L;
    /**
     * 服务的名称 ，如果不写使用上级的配置或者使用Spring.application.name配置
     */
    private String name;
    /**
     * 监听地址 默认为localhost
     */
    private String address;
    /**
     * 监听端口
     */
    private Integer port;

    /**
     * 要对外提供的服务接口类列表
     */
    private String[] services;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String[] getServices() {
        return services;
    }

    public void setServices(String[] services) {
        this.services = services;
    }
}
