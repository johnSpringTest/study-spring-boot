package com.john.study.grpc.boot.config;

import org.springframework.context.annotation.Import;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 启用Grpc服务
 *
 * @author jianguangtao03382 2018/5/7
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import({GrpcAutoConfiguration.class})
public @interface EnableGrpcApp {
}
