package com.john.study.grpc.boot.config;

import com.google.common.collect.Maps;
import com.john.common.util.RelaxedBeanCopy;
import com.john.study.grpc.boot.config.vo.ClientConfigVo;
import com.john.study.grpc.boot.config.vo.GrpcAppConfigVo;
import com.john.study.grpc.common.config.ClientConfigInfo;
import com.john.study.grpc.common.config.CommonConfigInfo;
import com.john.study.grpc.common.config.GrpcAppConfigInfo;
import com.john.study.grpc.common.config.ServerConfigInfo;
import com.john.study.grpc.common.exceptions.RpcException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author jianguangtao03382 2018/5/7
 */
@Configuration
@EnableConfigurationProperties(GrpcAppConfigVo.class)
public class GrpcAutoConfiguration implements ApplicationListener<ContextRefreshedEvent> {
    private static final Logger LOGGER = LoggerFactory.getLogger(GrpcAutoConfiguration.class);
    private GrpcAppConfigVo appConfigVo;

    @Value("${spring.application.name}")
    private String applicationName;

    public GrpcAutoConfiguration(GrpcAppConfigVo appConfigVo) {
        this.appConfigVo = appConfigVo;
    }

    @Bean
    public ApplicationContextServiceProvider applicationContextServiceProvider() {
        return new ApplicationContextServiceProvider();
    }

    @Bean
    public GrpcBootStarter grpcBootStarter() {
        return new GrpcBootStarter();
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        GrpcBootStarter bootStarter = grpcBootStarter();

        if(StringUtils.isBlank(appConfigVo.getName())) {
            appConfigVo.setName(applicationName);
        }

        GrpcAppConfigInfo appConfigInfo = getGrpcAppConfigInfo(appConfigVo);

        try {
            LOGGER.info("开始启动App");
            bootStarter.startApp(appConfigInfo);
            LOGGER.info("启动App完成！");
        } catch (Exception e) {
            throw new RpcException("启动失败", e);
        }

    }

    private GrpcAppConfigInfo getGrpcAppConfigInfo(GrpcAppConfigVo appConfigVo) {
        GrpcAppConfigInfo appConfigInfo = new GrpcAppConfigInfo();
        appConfigInfo.setName(appConfigVo.getName());
        appConfigInfo.setRegister(appConfigVo.isRegister());
        appConfigInfo.setAppMode(appConfigVo.getAppMode());

        if(null != appConfigVo.getCommon()) {
            CommonConfigInfo commonConfigInfo = new CommonConfigInfo();
            RelaxedBeanCopy.create(commonConfigInfo).copyFrom(appConfigVo.getCommon());
            appConfigInfo.setCommonConfigInfo(commonConfigInfo);
        }

        if(null != appConfigVo.getServer()) {
            ServerConfigInfo serverConfigInfo = new ServerConfigInfo();
            RelaxedBeanCopy.create(serverConfigInfo).copyFrom(appConfigInfo.getServerConfigInfo());
            appConfigInfo.setServerConfigInfo(serverConfigInfo);
        }

        if(null != appConfigVo.getClients() && appConfigVo.getClients().length>0) {
            List<ClientConfigInfo> clientConfigs = new ArrayList<>();

            ClassLoader cl = getClass().getClassLoader();

            try {
                for (ClientConfigVo clientConfigVo : appConfigVo.getClients()) {
                    ClientConfigInfo clientConfigInfo = new ClientConfigInfo();
                    RelaxedBeanCopy.create(clientConfigInfo)
                            .ignores("services","interfaceMethodTimeout")
                            .copyFrom(clientConfigVo);

                    Set<Class<?>> services = new LinkedHashSet<>();
                    /*
                     * 接口独立配置的超时时间，最多精确到方法级别
                     */
                    Map<String, Integer> interfaceMethodTimeout = Maps.newHashMap();

                    for (String svn : clientConfigVo.getServices()) {
                        // com.john.demo.hello.WordService#longword 25000
                        String[] parts = StringUtils.split(svn);
                        Class<?> clazz = null;
                        if (parts.length == 1) {
                            clazz = cl.loadClass(svn);
                        } else if(parts.length>1) {
                            String typeParts[] = StringUtils.split(parts[0],"#");
                            String timePart = parts[1];

                            int timeout = -2;
                            try {
                                timeout = Integer.parseInt(timePart);
                                if(timeout<0) {
                                    timeout = -1;
                                } else if(timeout<500) {
                                    timeout = 500;
                                }
                            } catch (NumberFormatException e) {
                                LOGGER.warn("错误的超时时间配置 {}", timePart);
                                timeout = -2;
                            }

                            clazz = cl.loadClass(typeParts[0]);
                            if(timeout!=-2) {
                                interfaceMethodTimeout.put(parts[0], timeout);
                            }
                        }
                        services.add(clazz);
                    }
                    clientConfigInfo.setServices(new ArrayList<>(services));
                    clientConfigInfo.setInterfaceMethodTimeout(interfaceMethodTimeout);
                    clientConfigs.add(clientConfigInfo);
                }
            } catch (ClassNotFoundException e) {
                throw new RpcException("未找到相应的类定义", e);
            }

            appConfigInfo.setClientConfigInfos(clientConfigs);
        }

        return appConfigInfo;
    }
}
