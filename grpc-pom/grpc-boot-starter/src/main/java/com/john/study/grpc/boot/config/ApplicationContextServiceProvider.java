package com.john.study.grpc.boot.config;

import com.john.study.grpc.common.exceptions.RpcException;
import com.john.study.grpc.common.server.ServiceHandlerProvider;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * @author jianguangtao03382 2018/5/7
 */
public class ApplicationContextServiceProvider implements ServiceHandlerProvider, ApplicationContextAware {
    private ApplicationContext applicationContext;
    @Override
    public Object getServiceHandler(String serviceName) {
        try {
            Class<?> aClass = applicationContext.getClassLoader().loadClass(serviceName);
            return getServiceHandler(aClass);
        } catch (ClassNotFoundException e) {
            throw new RpcException("接口类" + serviceName + " 未找到相应的定义", e);
        }
    }

    @Override
    public Object getServiceHandler(Class<?> serviceType) {
        return applicationContext.getBean(serviceType);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
