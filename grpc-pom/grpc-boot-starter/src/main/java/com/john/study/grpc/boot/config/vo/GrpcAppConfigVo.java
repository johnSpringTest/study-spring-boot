package com.john.study.grpc.boot.config.vo;

import com.john.study.grpc.common.consts.AppMode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

import java.io.Serializable;

/**
 * @author jianguangtao03382 2018/5/7
 */
@ConfigurationProperties(prefix = "grpc.app", ignoreNestedProperties = false)
public class GrpcAppConfigVo implements Serializable {
    private static final long serialVersionUID = -8269406564083942813L;
    /**
     * 应用服务的名称
     */
    private String name;

    /**
     * 是否向注册中心声明当前服务
     */
    private boolean register;
    /**
     * 当前应用模式 服务模式、客户端模式 默认为全模式
     */
    private AppMode appMode = AppMode.BOTH;
    /**
     * 公用配置信息 服务端或者客户端都可以公用的配置信息
     */
    @NestedConfigurationProperty
    private CommonConfigVo common;
    /**
     * 服务端的配置信息
     */
    @NestedConfigurationProperty
    private ServerConfigVo server;
    /**
     * 客户端配置信息
     */
    @NestedConfigurationProperty
    private ClientConfigVo[] clients;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isRegister() {
        return register;
    }

    public void setRegister(boolean register) {
        this.register = register;
    }

    public AppMode getAppMode() {
        return appMode;
    }

    public void setAppMode(AppMode appMode) {
        this.appMode = appMode;
    }

    public CommonConfigVo getCommon() {
        return common;
    }

    public void setCommon(CommonConfigVo common) {
        this.common = common;
    }

    public ServerConfigVo getServer() {
        return server;
    }

    public void setServer(ServerConfigVo server) {
        this.server = server;
    }

    public ClientConfigVo[] getClients() {
        return clients;
    }

    public void setClients(ClientConfigVo[] clients) {
        this.clients = clients;
    }
}
