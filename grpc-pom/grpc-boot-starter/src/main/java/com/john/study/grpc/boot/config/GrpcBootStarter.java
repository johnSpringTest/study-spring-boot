package com.john.study.grpc.boot.config;

import com.john.study.grpc.common.GrpcAppStarter;
import com.john.study.grpc.common.server.ServiceHandlerProvider;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author jianguangtao03382 2018/5/7
 */
public class GrpcBootStarter extends GrpcAppStarter {

    private ServiceHandlerProvider serviceHandlerProvider;

    @Autowired
    public void setServiceHandlerProvider(ServiceHandlerProvider serviceHandlerProvider) {
        this.serviceHandlerProvider = serviceHandlerProvider;
    }

    @Override
    protected ServiceHandlerProvider getServiceHandlerProvider() {
        return serviceHandlerProvider;
    }
}
