package com.john.study.grpc.boot.config.vo;

import java.util.List;

/**
 * @author jianguangtao03382 2018/5/3
 */
public class ClientConfigVo extends CommonConfigVo {
    private static final long serialVersionUID = 3405702689668453250L;
    /**
     * 引用服务的名称
     */
    private String name;
    /**
     * 多个服务器地址的负载均衡策略
     */
    private int loadBalancer = 0;
    /**
     * 需要引用的接口列表
     */
    private String[] services;

    /**
     * 服务器地址
     */
    private List<String> serverAddresses;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLoadBalancer() {
        return loadBalancer;
    }

    public void setLoadBalancer(int loadBalancer) {
        this.loadBalancer = loadBalancer;
    }

    public String[] getServices() {
        return services;
    }

    public void setServices(String[] services) {
        this.services = services;
    }

    public List<String> getServerAddresses() {
        return serverAddresses;
    }

    public void setServerAddresses(List<String> serverAddresses) {
        this.serverAddresses = serverAddresses;
    }
}
