package com.john.study.grpc.boot.config.vo;

import java.io.Serializable;

/**
 * 能用配置部分
 *
 * @author jianguangtao03382 2018/5/3
 */
public class CommonConfigVo implements Serializable {
    private static final long serialVersionUID = -6505584789440060557L;
    /**
     * 应用服务版本号 0表示默认的
     */
    private int version = 0;
    /**
     * 应用所属分组
     */
    private String group;
    /**
     * 注册中心地址 不使用Spring集成的注册中心时的注册中心参数
     */
    private String registerUrl;

    /**
     * 连接超时时间 默认为3秒
     */
    private int connectionTimeout = 3000;

    /**
     * 服务监听的核心线程数 默认为1
     */
    private int threadPoolCoreSize = 1;
    /**
     * 最大执行线程数（并发数） 默认为64
     */
    private int threadPoolMaxSize = 64;
    /**
     * 执行线程池大小
     */
    private int threadQueueSize = 1024;

    /**
     * 空闲线程等待时间
     */
    private int threadAliveSeconds = 3;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getRegisterUrl() {
        return registerUrl;
    }

    public void setRegisterUrl(String registerUrl) {
        this.registerUrl = registerUrl;
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public int getThreadPoolCoreSize() {
        return threadPoolCoreSize;
    }

    public void setThreadPoolCoreSize(int threadPoolCoreSize) {
        this.threadPoolCoreSize = threadPoolCoreSize;
    }

    public int getThreadPoolMaxSize() {
        return threadPoolMaxSize;
    }

    public void setThreadPoolMaxSize(int threadPoolMaxSize) {
        this.threadPoolMaxSize = threadPoolMaxSize;
    }

    public int getThreadQueueSize() {
        return threadQueueSize;
    }

    public void setThreadQueueSize(int threadQueueSize) {
        this.threadQueueSize = threadQueueSize;
    }

    public int getThreadAliveSeconds() {
        return threadAliveSeconds;
    }

    public void setThreadAliveSeconds(int threadAliveSeconds) {
        this.threadAliveSeconds = threadAliveSeconds;
    }
}
