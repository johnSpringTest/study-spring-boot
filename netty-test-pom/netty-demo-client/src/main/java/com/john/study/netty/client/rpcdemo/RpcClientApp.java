package com.john.study.netty.client.rpcdemo;

import com.john.common.util.threads.ThreadPools;
import com.john.study.netty.common.codec.packet.PacketVos;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author jianguangtao03382 2018/4/28
 */
public class RpcClientApp {
    private static final Logger LOGGER = LoggerFactory.getLogger(RpcClientApp.class);
    private static AtomicLong packetId = new AtomicLong(1);

    public static void main(String[] args) throws InterruptedException {
        Random random = new Random();

        String host = "localhost";
        int port = 9921;

        int threads = 4;

        CountDownLatch latch = new CountDownLatch(threads);
        for (int i = 0; i < threads; i++) {
            DemoRpcClient rpcClient1 = new DemoRpcClient(host, port);
            int times = random.nextInt(100) + 10;
            ThreadPools.getNormalPool().submit(new ClientRunner(rpcClient1, times, latch));
        }
        LOGGER.info("成功启动{}个调用客户端", threads);
        latch.await();
        LOGGER.info("所有客户端调用完成！");
    }

    public static class ClientRunner implements Runnable {
        private static final Logger LOGGER = LoggerFactory.getLogger(ClientRunner.class);
        private Random random = new Random();
        private DemoRpcClient rpcClient;
        private int testTimes;
        private CountDownLatch latch;

        public ClientRunner(DemoRpcClient rpcClient, int testTimes, CountDownLatch latch) {
            this.rpcClient = rpcClient;
            this.testTimes = testTimes;
            this.latch = latch;
            if (this.testTimes < 1) {
                this.testTimes = 1;
            }
        }

        @Override
        public void run() {
            LOGGER.info("当前客户端准备调用次数：{}", testTimes);
            rpcClient.start();
            try {
                for (int i = 0; i < testTimes; i++) {
                    rpcClient.getSocketChannel().writeAndFlush(PacketVos.getPingPacket(packetId.incrementAndGet()));
                    int sleepTimes = random.nextInt(1500) + 500;
                    try {
                        Thread.sleep(sleepTimes);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                rpcClient.stop();
            } finally {
                latch.countDown();
            }
        }
    }
}
