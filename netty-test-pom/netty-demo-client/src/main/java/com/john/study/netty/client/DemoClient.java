package com.john.study.netty.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author jianguangtao03382 2018/4/27
 */
public class DemoClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(DemoClient.class);

    public static void main(String[] args) throws IOException {

        EchoClient echoClient = new EchoClient("localhost", 9911);
        echoClient.start();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            String line = reader.readLine();

            if("exit".equalsIgnoreCase(line)||"quit".equalsIgnoreCase(line)) {
                break;
            }

            echoClient.getSocketChannel().writeAndFlush(line);
            LOGGER.info("消息已经发送 请重新输入新消息");
        }

        echoClient.stop();
        LOGGER.info("关闭客户端");

    }
}
