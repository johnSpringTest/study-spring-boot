package com.john.study.netty.client;

import com.john.study.netty.client.channelhandler.TimeClientHandler;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author jianguangtao03382 2018/4/27
 */
public class EchoClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(EchoClient.class);

    private String host;

    private int port;

    private EventLoopGroup workGroup = new NioEventLoopGroup(4);

    private  Bootstrap bootstrap;
    private SocketChannel socketChannel;

    public EchoClient(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public void start() {

        bootstrap = new Bootstrap();
        try {
            bootstrap.group(workGroup)
                    .channel(NioSocketChannel.class)
                    .option(ChannelOption.SO_KEEPALIVE, true)
                    .option(ChannelOption.TCP_NODELAY, true)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
//                            ch.pipeline().addLast(StringEncoder.class)
                            ch.pipeline().addLast(new StringDecoder(), new StringEncoder(), new TimeClientHandler());
                        }
                    });

            // Start the client.
            ChannelFuture f = bootstrap.connect(host, port).sync(); // (5)
            if (f.isSuccess()) {
                LOGGER.info("客户端启动完成！{}:{}", host, port);
                socketChannel = (SocketChannel) f.channel();
            } else {
                LOGGER.warn("客户端启动失败！");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        workGroup.shutdownGracefully();
    }

    public SocketChannel getSocketChannel() {
        return socketChannel;
    }
}
