package com.john.study.netty.client.rpcdemo;

import com.john.study.netty.common.codec.packet.PacketVo;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.socket.SocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author jianguangtao03382 2018/4/28
 */
public class ClientPacketInHandler extends ChannelInboundHandlerAdapter {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientPacketInHandler.class);

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
//        super.channelRead(ctx, msg);
        PacketVo vo = (PacketVo) msg;
        LOGGER.info("接收到的服务端的返回消息是：{}", vo);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
//        super.exceptionCaught(ctx, cause);
        LOGGER.error("返回消息出现异常", cause);
        try {
            SocketChannel channel = (SocketChannel) ctx.channel();
            channel.close();
        } catch (Exception e) {
            LOGGER.warn("关闭channel异常", e);
        }
    }
}
