package com.john.study.netty.client.channelhandler;

import com.john.study.netty.common.codec.RequestInfoVO;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NettyClientHandler extends SimpleChannelInboundHandler<RequestInfoVO> {
    private static final Logger LOGGER = LoggerFactory.getLogger(NettyClientHandler.class);

    protected void messageReceived(ChannelHandlerContext ctx, RequestInfoVO msg) throws Exception {
        channelRead0(ctx, msg);
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, RequestInfoVO msg) throws Exception {
        LOGGER.info(msg.getBody());
        RequestInfoVO req = new RequestInfoVO();
        req.setSequence(msg.getSequence());
        req.setType(msg.getType());
        if (2 == msg.getType()) {
            req.setBody("client");
            ctx.channel().writeAndFlush(req);
        } else if (3 == msg.getType()) {
            req.setBody("zpksb");
            ctx.channel().writeAndFlush(req);
        }

    }
}