package com.john.study.netty.client.rpcdemo;

import com.john.study.netty.common.codec.packet.PacketDecoder;
import com.john.study.netty.common.codec.packet.PacketEncoder;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author jianguangtao03382 2018/4/28
 */
public class DemoRpcClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(DemoRpcClient.class);
    private String host;
    private int port;

    private SocketChannel socketChannel;
    private Bootstrap bootstrap;
    private EventLoopGroup workGroup = new NioEventLoopGroup(4);
    private transient boolean started = false;

    public DemoRpcClient(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public void start() {
        if (started) {
            return;
        }
        doStart();
    }

    public void restart() {
        if (!started) {
            doStart();
        } else {
            stop();
            doStart();
        }
    }

    private void doStart() {
        bootstrap = new Bootstrap();
        try {
            bootstrap.group(workGroup)
                    .channel(NioSocketChannel.class)
                    .option(ChannelOption.SO_KEEPALIVE, true)
                    .option(ChannelOption.TCP_NODELAY, true)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline().addLast(new PacketDecoder(), new PacketEncoder(),
                                    new ClientPacketInHandler());
                        }
                    });

            // Start the client.
            ChannelFuture f = bootstrap.connect(host, port).sync(); // (5)
            if (f.isSuccess()) {
                LOGGER.info("客户端启动完成！{}:{}", host, port);
                socketChannel = (SocketChannel) f.channel();
            } else {
                LOGGER.warn("客户端启动失败！");
            }
            started = true;
        } catch (Exception e) {
            LOGGER.error("启动客户端服务异常", e);
        }
    }

    public void stop() {
        if (started) {
            bootstrap = null;
            workGroup.shutdownGracefully();
            workGroup = null;
            socketChannel.close();
            socketChannel = null;
            started = false;
        }
    }

    /**
     * 获取socket
     *
     * @return socket
     */
    public SocketChannel getSocketChannel() {
        return socketChannel;
    }
}
