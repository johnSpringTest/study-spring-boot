package com.john.study.netty.common.codec.packet;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * 数据包解码工具类
 * @author jianguangtao03382 2018/4/27
 */
public class PacketDecoder extends ByteToMessageDecoder implements PacketCodecs {
    private static final Logger LOGGER = LoggerFactory.getLogger(PacketDecoder.class);

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        // 可读取字节数
        if (in.readableBytes() < PACKET_HEAD_LEN) {
            return;
        }
        // 标记开始读取位置
        in.markReaderIndex();

        int magic_number = in.readInt();

        // 不是自己需要的包类型
        if (MAGIC_CODE != magic_number) {
            ctx.close();
            return;
        }
        short packetType = in.readShort();
        byte version = in.readByte();
        long packetId = in.readLong();
        int length = in.readInt();

        if (length < 0) {
            ctx.close();
            return;
        }

        PacketVo vo = new PacketVo(packetType, version, packetId, null);

        if (length > 0) {
            if (in.readableBytes() < length) {
                // 重置到开始读取位置
                in.resetReaderIndex();
                return;
            }
            byte[] body = new byte[length];
            in.readBytes(body);

            vo.setData(new String(body, DEFAULT_ENCODING));
        }
//        LOGGER.debug("读取到新包：{}", vo);
        out.add(vo);
    }
}
