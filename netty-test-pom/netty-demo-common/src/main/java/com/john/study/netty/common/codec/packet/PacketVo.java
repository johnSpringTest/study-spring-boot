package com.john.study.netty.common.codec.packet;

import java.io.Serializable;

/**
 * 数据包id
 * @author jianguangtao03382 2018/4/27
 */
public class PacketVo implements Serializable {
    private static final long serialVersionUID = -3604358822081495647L;
    private short type; //包类型
    private byte version; // 数据包版本号
    private long packetId;
    private String data;

    public PacketVo() {
        version = PacketCodecs.DEFAULT_VERSION;
    }

    public PacketVo(short type, byte version, long packetId, String data) {
        this.type = type;
        this.version = version;
        this.packetId = packetId;
        this.data = data;
    }

    public short getType() {
        return type;
    }

    public void setType(short type) {
        this.type = type;
    }

    public byte getVersion() {
        return version;
    }

    public void setVersion(byte version) {
        this.version = version;
    }

    public long getPacketId() {
        return packetId;
    }

    public void setPacketId(long packetId) {
        this.packetId = packetId;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "PacketVo{" +
                "type=" + type +
                ", version=" + version +
                ", packetId=" + packetId +
                ", data='" + data + '\'' +
                '}';
    }
}
