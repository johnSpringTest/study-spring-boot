package com.john.study.netty.common.codec.packet;

/**
 * @author jianguangtao03382 2018/4/27
 */
public class PacketVos {
    private PacketVos() {

    }

    public static PacketVo getPingPacket(long packetId) {
        String pingData = System.currentTimeMillis() + "";
        return new PacketVo(PacketCodecs.PACKET_TYPE_PING, PacketCodecs.DEFAULT_VERSION, packetId, pingData);
    }

    public static PacketVo getPong(long packetId) {
        String pongData = System.currentTimeMillis() + "";
        return new PacketVo(PacketCodecs.PACKET_TYPE_PONG, PacketCodecs.DEFAULT_VERSION, packetId, pongData);
    }
}
