package com.john.study.netty.common.codec.packet;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * @author jianguangtao03382 2018/4/27
 */
public interface PacketCodecs {
    /**
     * 数据包的魔数 4字节
     */
    int MAGIC_CODE = 0x2F1029EA;
    /**
     * 包体中的数据内容的编码格式
     */
    Charset DEFAULT_ENCODING = StandardCharsets.UTF_8;
    /**
     * 包体头长度 字节
     */
    int PACKET_HEAD_LEN = 19;

    /**
     * 默认的版本号 1字节
     */
    byte DEFAULT_VERSION = 0x01;

    /**
     * 发送ping消息 同步用的
     */
    short PACKET_TYPE_PING = 0x00;
    /**
     * 返回ping消息 同步用的
     */
    short PACKET_TYPE_PONG = 0x01;
    /**
     * 注册服务
     */
    short PACKET_TYPE_REGISTER = 0x02;
    /**
     * 反注册服务
     */
    short PACKET_TYPE_UN_REGISTER = 0x03;
    /**
     * 基础业务数据 发送或者返回
     */
    short PACKET_TYPE_BUSINESS = 0x10;
    /**
     * 控制位掩码字符串 2字节
     */
    int CONTROL_BITS_MASK = 0xffff;
}
