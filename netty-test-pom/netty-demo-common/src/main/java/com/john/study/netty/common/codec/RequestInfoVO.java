package com.john.study.netty.common.codec;

import java.io.Serializable;

/**
 * @author jianguangtao03382 2018/4/27
 */
public class RequestInfoVO implements Serializable {
    private static final long serialVersionUID = -1960956561901420771L;
    private String body;
    private byte type;
    private int sequence;


    public void setBody(String body) {
        this.body = body;
    }

    public String getBody() {
        return body;
    }

    public void setType(byte type) {
        this.type = type;
    }

    public byte getType() {
        return type;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public int getSequence() {
        return sequence;
    }
}
