package com.john.study.netty.common.codec.packet;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.apache.commons.lang3.StringUtils;

/**
 * 数据包编码工具类
 */
public class PacketEncoder extends MessageToByteEncoder<PacketVo> implements PacketCodecs {
    @Override
    protected void encode(ChannelHandlerContext ctx, PacketVo msg, ByteBuf out) throws Exception {
        ByteBufOutputStream writer = new ByteBufOutputStream(out);
        byte[] body = null;
        if (null == msg) {
            return;
        }

        if (StringUtils.isNotEmpty(msg.getData())) {
            body = msg.getData().getBytes(DEFAULT_ENCODING);
        }

        writer.writeInt(MAGIC_CODE);
        writer.writeShort(msg.getType());
        writer.writeByte(msg.getVersion());
        writer.writeLong(msg.getPacketId());
        // 没有数据体的包
        if (null == body || body.length == 0) {
            writer.writeInt(0);
        } else {
            writer.writeInt(body.length);
            writer.write(body);
        }
    }

}