package com.john.study.netty.server;

import com.john.study.netty.server.channelhandler.EchoChannelHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author jianguangtao03382 2018/4/27
 */
public class DemoServer {
    private static final Logger LOGGER = LoggerFactory.getLogger(DemoServer.class);
    private static final int port = 9911;

    public static void main(String[] args) {
        EventLoopGroup boosGroup = new NioEventLoopGroup(1);
        EventLoopGroup workGroup = new NioEventLoopGroup(4);

        ServerBootstrap bootstrap = new ServerBootstrap();

        try {
            bootstrap.group(boosGroup, workGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel socketChannel) throws Exception {
                            socketChannel.pipeline().addLast(new StringDecoder(), new StringEncoder(),new EchoChannelHandler());
                        }
                    })
                    .option(ChannelOption.SO_BACKLOG, 128) //保持连接数
                    .option(ChannelOption.TCP_NODELAY, true)//有数据立即发送
                    .childOption(ChannelOption.SO_KEEPALIVE, true) // 保持连接

            ;

            ChannelFuture future = bootstrap.bind(port).sync();
            LOGGER.info("服务启动！{}", port);

            future.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            boosGroup.shutdownGracefully();
            workGroup.shutdownGracefully();
        }

    }
}
