package com.john.study.netty.server.rpc;

import io.netty.channel.socket.ServerSocketChannel;
import io.netty.channel.socket.SocketChannel;

import java.util.LinkedList;
import java.util.List;

/**
 * @author jianguangtao03382 2018/4/28
 */
public class RpcServerContext {
    /**
     * 服务端socket
     */
    private ServerSocketChannel serverSocketChannel;
    /**
     * 对应的客户端列表
     */
    private List<SocketChannel> clientSockets = new LinkedList<>();

    public ServerSocketChannel getServerSocketChannel() {
        return serverSocketChannel;
    }

    public void setServerSocketChannel(ServerSocketChannel serverSocketChannel) {
        this.serverSocketChannel = serverSocketChannel;
    }

    public List<SocketChannel> getClientSockets() {
        return clientSockets;
    }

    public void addClientSocket(SocketChannel clientSocket) {
        clientSockets.add(clientSocket);
    }

    public void removeClientSocket(SocketChannel clientSocket) {
        clientSockets.remove(clientSocket);
    }
}
