package com.john.study.netty.server.rpc.handler;

import com.john.study.netty.common.codec.packet.PacketVo;
import com.john.study.netty.common.codec.packet.PacketVos;
import com.john.study.netty.server.rpc.RpcServerContext;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.socket.SocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author jianguangtao03382 2018/4/28
 */
public class PacketInboundHandler extends ChannelInboundHandlerAdapter {
    private static final Logger LOGGER = LoggerFactory.getLogger(PacketInboundHandler.class);

    private RpcServerContext rpcServerContext;

    public PacketInboundHandler(RpcServerContext rpcServerContext) {
        this.rpcServerContext = rpcServerContext;
    }

    @Override
    public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
        super.channelRegistered(ctx);
        SocketChannel channel = (SocketChannel) ctx.channel();
//        channel.isConnected();
//        channel.isOpen();
//        channel.close();
        rpcServerContext.addClientSocket(channel);
        LOGGER.info("注册新的连接 {}", channel);
        LOGGER.info("远程地址是：{}", channel.remoteAddress());
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
        super.channelUnregistered(ctx);
        SocketChannel channel = (SocketChannel) ctx.channel();
        rpcServerContext.removeClientSocket(channel);
        LOGGER.info("移除连接 {}", channel);
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
        SocketChannel channel = (SocketChannel) ctx.channel();
        LOGGER.info("激活连接 {}", channel);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx);
        SocketChannel channel = (SocketChannel) ctx.channel();
        LOGGER.info("反激活连接 {}", channel);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
//        super.channelRead(ctx, msg);
        PacketVo vo = (PacketVo) msg;
        LOGGER.debug("收到的包内容是：{}", vo);
        ctx.writeAndFlush(PacketVos.getPong(vo.getPacketId() + 1));
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
//        super.exceptionCaught(ctx, cause);
        SocketChannel channel = (SocketChannel) ctx.channel();
        LOGGER.error("连接出现异常 {} ", channel, cause);
    }
}
