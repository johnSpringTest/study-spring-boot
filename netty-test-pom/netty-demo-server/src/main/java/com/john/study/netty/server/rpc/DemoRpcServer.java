package com.john.study.netty.server.rpc;

import com.john.study.netty.common.codec.packet.PacketDecoder;
import com.john.study.netty.common.codec.packet.PacketEncoder;
import com.john.study.netty.server.rpc.handler.PacketInboundHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.ServerSocketChannel;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author jianguangtao03382 2018/4/28
 */
public class DemoRpcServer {
    private static final Logger LOGGER = LoggerFactory.getLogger(DemoRpcServer.class);
    private int port;
    private ServerBootstrap bootstrap;
    EventLoopGroup boosGroup;
    EventLoopGroup workGroup;

    ServerSocketChannel serverSocketChannel;

    public DemoRpcServer(int port) {
        this.port = port;
    }

    public void start() {
        boosGroup = new NioEventLoopGroup(1);
        workGroup = new NioEventLoopGroup(4);

        bootstrap = new ServerBootstrap();

        RpcServerContext rpcServerContext = new RpcServerContext();

        try {
            bootstrap.group(boosGroup, workGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel socketChannel) throws Exception {
                            socketChannel.pipeline().addLast(new PacketDecoder(), new PacketEncoder(),
                                    new PacketInboundHandler(rpcServerContext));
                        }
                    })
                    .option(ChannelOption.SO_BACKLOG, 1024) //保持连接数
                    .option(ChannelOption.TCP_NODELAY, true)//有数据立即发送
                    .childOption(ChannelOption.SO_KEEPALIVE, true) // 保持连接
            ;

            ChannelFuture future = bootstrap.bind(port).sync();
            LOGGER.info("服务启动！{}", port);
            if (future.isSuccess()) {
                serverSocketChannel = (ServerSocketChannel) future.channel();
                rpcServerContext.setServerSocketChannel(serverSocketChannel);
            }

        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    public void stop() {
        if (null != boosGroup) {
            boosGroup.shutdownGracefully();
        }
        if (null != workGroup) {
            workGroup.shutdownGracefully();
        }

    }

}
