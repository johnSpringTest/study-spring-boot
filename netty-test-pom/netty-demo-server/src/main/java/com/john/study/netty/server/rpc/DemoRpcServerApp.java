package com.john.study.netty.server.rpc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author jianguangtao03382 2018/4/28
 */
public class DemoRpcServerApp {
    private static final Logger LOGGER = LoggerFactory.getLogger(DemoRpcServerApp.class);

    public static void main(String[] args) throws IOException {
        int port = 9921;
        DemoRpcServer rpcServer = new DemoRpcServer(port);
        rpcServer.start();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        LOGGER.info("服务已经启动 {} 输出exit/quit结束服务", port);
        while (true) {
            String line = reader.readLine();

            if ("exit".equalsIgnoreCase(line) || "quit".equalsIgnoreCase(line)) {
                break;
            }
        }

        rpcServer.stop();
        LOGGER.info("关闭服务端");
    }
}
