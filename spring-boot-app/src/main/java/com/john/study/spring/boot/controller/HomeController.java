package com.john.study.spring.boot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by jiangguangtao on 2017/4/12.
 */
@Controller
public class HomeController {
    public static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @RequestMapping({"/", "/home"})
    public String home(Model model) {
        model.addAttribute("recipient", "后台用户");
        return "index";
    }

    @RequestMapping("/demo")
    public String demo(Model model) {
        model.addAttribute("serverText", "服务商生成的文本!" + sdf.format(new Date()));
        return "demo";
    }
}
