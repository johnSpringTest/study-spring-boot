/**
 * Created by jiangguangtao on 2016/7/25.
 */

function activeNav(code) {
    $('#bs-navbar').find('li')
        .removeClass('active')
        .each(function(){
        var $li = $(this);
        var dc = $li.data('link-code');
        if(dc == code) {
            $li.addClass('active');
        }
    });
}