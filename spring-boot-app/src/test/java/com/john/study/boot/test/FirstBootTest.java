package com.john.study.boot.test;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;

/**
 * @author jianguangtao03382 2018/4/18
 */
public class FirstBootTest extends BaseBootTest{
    private static final Logger LOGGER = LoggerFactory.getLogger(FirstBootTest.class);

    @Autowired
    private DataSource dataSource;

    @Test
    public void test1() {
        LOGGER.info("测试启动OK！");
        LOGGER.info("datasource :{}", dataSource);
    }
}
