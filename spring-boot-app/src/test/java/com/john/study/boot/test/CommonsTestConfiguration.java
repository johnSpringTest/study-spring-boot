package com.john.study.boot.test;

import com.john.common.dao.config.EnableSimpleDal;
import org.mybatis.spring.boot.autoconfigure.MybatisAutoConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.transaction.TransactionAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author jianguangtao03382 2018/4/18
 */
@EnableSimpleDal
@Configuration
@ComponentScan(basePackages = {"com.john.study.spring.boot.commons"})
@EnableAutoConfiguration(exclude = {MybatisAutoConfiguration.class, JpaRepositoriesAutoConfiguration.class})
//@ImportAutoConfiguration(classes = {DataSourceAutoConfiguration.class})
public class CommonsTestConfiguration {


}
