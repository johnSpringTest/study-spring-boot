package com.john.study.boot.test;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import com.john.study.spring.boot.MyBatisConfig;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author jianguangtao03382 2018/4/18
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {CommonsTestConfiguration.class})
public abstract class BaseBootTest {

}
