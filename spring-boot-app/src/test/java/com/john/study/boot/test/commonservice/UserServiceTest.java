package com.john.study.boot.test.commonservice;

import com.john.common.dao.data.data.Pagination;
import com.john.common.dao.data.data.PaginationResult;
import com.john.study.boot.test.BaseBootTest;
import com.john.study.spring.boot.commons.data.vo.UserVo;
import com.john.study.spring.boot.commons.service.IUserService;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * @author jianguangtao03382 2018/4/26
 */
public class UserServiceTest extends BaseBootTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceTest.class);
    @Autowired
    private IUserService userService;

    @Test
    public void testFindByUsername() {
        String username = "john";
        UserVo vo = userService.findByUsername(username);
        Assert.assertNotNull(vo);
        Assert.assertTrue(username.equals(vo.getUsername()));
        LOGGER.info("查询出的用户信息是：{}", vo);
    }

    @Test
    public void testFindSecurityUser() {
        String username = "admin";

        UserVo vo = userService.findSecurityUserByUsername(username);
        Assert.assertNotNull(vo);
        Assert.assertTrue(username.equals(vo.getUsername()));
        LOGGER.info("查询出的用户信息是：{}", vo);
    }

    @Test
    public void testQueryUserList() {
        UserVo vo = new UserVo();
        vo.setPager(Pagination.getDefault());
        PaginationResult<UserVo> result = userService.queryUserListPage(vo);

        Assert.assertNotNull(result);
        Assert.assertTrue(CollectionUtils.isNotEmpty(result.getDatas()));
        LOGGER.info("返回的分页信息是：{}", result.getPager());
        LOGGER.info("返回的数据信息是：{}", result.getDatas());
    }

}
