package com.john.study.spring.boot.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangguangtao on 2017/4/8.
 */
@RestController
@SpringBootApplication
//@RibbonClient(name = "server-hello", configuration = ServerHelloConfiguration.class)
//@EnableCircuitBreaker
@EnableDiscoveryClient
public class HelloClientApplication {
    private static final Logger LOGGER = LoggerFactory.getLogger(HelloClientApplication.class);

    @Autowired
    private LoadBalancerClient loadBalancerClient;

    @Autowired
    private DiscoveryClient discoveryClient;

    @Bean
    @LoadBalanced
    RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @Autowired
    RestTemplate restTemplate;

    @RequestMapping("/")
    public String home() {
        return "主页面，请访问 /hi";
    }


    @GetMapping("/hi")
    public String showHi(@RequestParam(defaultValue = "John") String name) {
        String greeting = this.restTemplate.getForObject("http://server-hello/greeting", String.class);
        LOGGER.info("远程返回值：{}", greeting);
        return String.format("Hi %s, %s!", greeting, name);
    }

    @GetMapping("/ping")
    public String ping(@RequestParam(defaultValue = "John") String name) {
        Map<String, Object> params = new HashMap<>(1);
        params.put("from", name);
        String pong = this.restTemplate.getForObject("http://server-hello/ping?from={from}", String.class, params);
        LOGGER.info("请求参数：{}，  远程返回值：{}", name, pong);
        return String.format("接口返回值： %s!", pong);
    }


    @RequestMapping("/discovery")
    public String discovery() {
        ServiceInstance serviceInstance = loadBalancerClient.choose("server-hello");
        LOGGER.info("选中的服务是：{}", serviceInstance);
        return "you can discovery method!";
    }

    @RequestMapping("/serviceList")
    public String serviceList() throws JsonProcessingException {
        List<String> services = discoveryClient.getServices();
        Map<String, List<String>> serviceMap = Maps.newHashMap();
        serviceMap.put("注册的服务列表", services);
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(serviceMap);
    }


    public static void main(String[] args) {
        SpringApplication.run(HelloClientApplication.class, args);
    }
}
