package com.john.study.spring.boot.client;

import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.AvailabilityFilteringRule;
import com.netflix.loadbalancer.IPing;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.PingUrl;
import org.springframework.context.annotation.Bean;

/**
 * @author jiangguangtao on 2017/4/8.
 */
public class ServerHelloConfiguration {
    private IClientConfig clientConfig;


    @Bean
    public IPing ribbonPing(IClientConfig clientConfig) {
        return new PingUrl();
    }

    @Bean
    public IRule ribbonRule(IClientConfig clientConfig) {
        return new AvailabilityFilteringRule();
    }
}
