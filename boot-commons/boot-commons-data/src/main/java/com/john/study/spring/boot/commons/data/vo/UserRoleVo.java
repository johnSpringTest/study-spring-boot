package com.john.study.spring.boot.commons.data.vo;

import com.john.common.dao.data.data.BaseBusinessVo;

/**
 * 用户角色关联Vo对象
 *
 * @author jiangguangtao@foxmail.com 2016/3/13
 */
public class UserRoleVo extends BaseBusinessVo<Long> {
    private static final long serialVersionUID = 6708186424342340172L;
    //用户ID
    private Long userId;
    //角色ID
    private Long roleId;
    // 角色编码
    private String roleCode;

    public UserRoleVo() {
    }

    /**
     * 用户角色关联对象
     *
     * @param userId   用户id
     * @param roleId   角色id
     * @param roleCode 角色编码  冗余的值
     */
    public UserRoleVo(Long userId, Long roleId, String roleCode) {
        this.userId = userId;
        this.roleId = roleId;
        this.roleCode = roleCode;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }
}
