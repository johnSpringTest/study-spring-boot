package com.john.study.spring.boot.commons.data.vo;

import com.john.common.dao.data.data.BaseBusinessVo;

import java.util.Objects;

/**
 * 角色关联的连接地址列表
 *
 * @author jianguangtao03382 2018/4/26
 */
public class RoleLinkVo extends BaseBusinessVo<Long> {
    private static final long serialVersionUID = -3764631847691692466L;
    /**
     * 角色id
     */
    private Long roleId;
    /**
     * 链接记录id
     */
    private Long linkId;

    /**
     * 角色编码
     */
    private String roleCode;
    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 链接编码
     */
    private String linkCode;

    @Override
    public Long getId() {
        return super.getId();
    }

    @Override
    public void setId(Long id) {
        super.setId(id);
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getLinkId() {
        return linkId;
    }

    public void setLinkId(Long linkId) {
        this.linkId = linkId;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getLinkCode() {
        return linkCode;
    }

    public void setLinkCode(String linkCode) {
        this.linkCode = linkCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoleLinkVo that = (RoleLinkVo) o;
        return Objects.equals(roleId, that.roleId) &&
                Objects.equals(linkId, that.linkId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roleId, linkId);
    }
}
