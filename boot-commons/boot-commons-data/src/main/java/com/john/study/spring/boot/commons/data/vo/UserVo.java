package com.john.study.spring.boot.commons.data.vo;

import com.john.common.dao.data.data.BaseBusinessPagerVo;
import com.john.study.spring.boot.commons.data.constants.UserStatusEnum;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author jianguangtao03382 2018/4/17
 */
public class UserVo extends BaseBusinessPagerVo<Long> implements UserDetails {
    private static final long serialVersionUID = 2448530389998910213L;
    /**
     * 用户昵称
     */
    private String nickName;
    /**
     * 用户名，登录名
     */
    private String userName;
    /**
     * 用户邮箱
     */
    private String email;
    /**
     * 用户状态 0新注册 1正常 2过期 3锁定 4过期 5注销
     */
    private String userStatus;
    /**
     * 用户密码
     */
    private String password;

    /**
     * 所属组织id
     */
    private Long orgId;
    /**
     * 手机
     */
    private String phone;
    /**
     * qq
     */
    private String qq;
    /**
     * 启用日期 新注册用户激活的日期
     */
    private Date enableDate;
    /**
     * 失效日期
     */
    private Date expireDate;
    /**
     * 用户备注
     */
    private String remark;
    /**
     * 用户角色列表
     */
    private List<RoleVo> roleList;
    /**
     * 权限列表
     */
    private List<LinkPrivilegeVo> privilegeList;

    @Override
    public Long getId() {
        return super.getId();
    }

    @Override
    public void setId(Long id) {
        super.setId(id);
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getUsername() {
        return userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public Date getEnableDate() {
        return enableDate;
    }

    public void setEnableDate(Date enableDate) {
        this.enableDate = enableDate;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public List<RoleVo> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<RoleVo> roleList) {
        this.roleList = roleList;
    }

    public List<LinkPrivilegeVo> getPrivilegeList() {
        return privilegeList;
    }

    public void setPrivilegeList(List<LinkPrivilegeVo> privilegeList) {
        this.privilegeList = privilegeList;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(roleList)) {
            roleList.forEach((role) -> authorities.add(new SimpleGrantedAuthority(role.getRoleCode())));
        } else {
            authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        }
        // 添加链接地址权限
        if (CollectionUtils.isNotEmpty(privilegeList)) {
            privilegeList.forEach((privilege) -> authorities.add(new SimpleGrantedAuthority(privilege.getPrivilegeCode())));
        } else {
            authorities.add(new SimpleGrantedAuthority("oper.user.view"));
        }

        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return !UserStatusEnum.EXPIRED.equals(userStatus);
    }

    @Override
    public boolean isAccountNonLocked() {
        return !UserStatusEnum.LOCKED.equals(userStatus);
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return UserStatusEnum.NORMAL.equals(userStatus);
    }

    @Override
    public String toString() {
        return "UserVo{" +
                "id=" + id +
                ", nickName='" + nickName + '\'' +
                ", userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                ", userStatus='" + userStatus + '\'' +
                ", password='" + password + '\'' +
                ", orgId=" + orgId +
                ", phone='" + phone + '\'' +
                ", qq='" + qq + '\'' +
                ", enableDate=" + enableDate +
                ", expireDate=" + expireDate +
                ", remark='" + remark + '\'' +
                ", pager=" + pager +
                ", creatorId=" + creatorId +
                ", createTime=" + createTime +
                ", updaterId=" + updaterId +
                ", updateTime=" + updateTime +
                ", recordStatus='" + recordStatus + '\'' +
                ", recordStatusStr='" + recordStatusStr + '\'' +
                '}';
    }
}
