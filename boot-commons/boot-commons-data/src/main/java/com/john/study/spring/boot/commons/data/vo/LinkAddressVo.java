package com.john.study.spring.boot.commons.data.vo;

import com.john.common.dao.data.data.BaseBusinessVo;

import java.util.List;
import java.util.Objects;

/**
 * 功能树vo
 *
 * @author jianguangtao03382 2018/4/26
 */
public class LinkAddressVo extends BaseBusinessVo<Long> {
    private static final long serialVersionUID = 4953337485540234471L;
    /**
     * 链接编码
     */
    private String linkCode;
    /**
     * 链接显示名称
     */
    private String linkName;
    /**
     * 上级链接id
     */
    private Long parentId;
    /**
     * 链接类型
     */
    private String linkType;
    /**
     * 链接层级 0 1 2 3
     * 0为根目录
     */
    private Integer linkLevel;

    /**
     * 显示顺序
     */
    private Integer viewIndex;
    /**
     * 是否为目录标识
     */
    private String folderFlag;
    /**
     * 是否启用标识
     */
    private String enableFlag;
    /**
     * 链接显示样式 css类名
     */
    private String linkIconClass;
    /**
     * 链接的具体地址
     */
    private String linkAddress;
    /**
     * 打开链接的方式
     */
    private String linkTarget;
    /**
     * 链接备注信息
     */
    private String remark;
    /**
     * 下级链接列表
     */
    private List<LinkAddressVo> subLinks;
    /**
     * 此链接关系的权限列表
     */
    private List<LinkPrivilegeVo> linkPrivileges;

    @Override
    public Long getId() {
        return super.getId();
    }

    @Override
    public void setId(Long id) {
        super.setId(id);
    }

    public String getLinkCode() {
        return linkCode;
    }

    public void setLinkCode(String linkCode) {
        this.linkCode = linkCode;
    }

    public String getLinkName() {
        return linkName;
    }

    public void setLinkName(String linkName) {
        this.linkName = linkName;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getLinkType() {
        return linkType;
    }

    public void setLinkType(String linkType) {
        this.linkType = linkType;
    }

    public Integer getLinkLevel() {
        return linkLevel;
    }

    public void setLinkLevel(Integer linkLevel) {
        this.linkLevel = linkLevel;
    }

    public Integer getViewIndex() {
        return viewIndex;
    }

    public void setViewIndex(Integer viewIndex) {
        this.viewIndex = viewIndex;
    }

    public String getFolderFlag() {
        return folderFlag;
    }

    public void setFolderFlag(String folderFlag) {
        this.folderFlag = folderFlag;
    }

    public String getEnableFlag() {
        return enableFlag;
    }

    public void setEnableFlag(String enableFlag) {
        this.enableFlag = enableFlag;
    }

    public String getLinkIconClass() {
        return linkIconClass;
    }

    public void setLinkIconClass(String linkIconClass) {
        this.linkIconClass = linkIconClass;
    }

    public String getLinkAddress() {
        return linkAddress;
    }

    public void setLinkAddress(String linkAddress) {
        this.linkAddress = linkAddress;
    }

    public String getLinkTarget() {
        return linkTarget;
    }

    public void setLinkTarget(String linkTarget) {
        this.linkTarget = linkTarget;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public List<LinkAddressVo> getSubLinks() {
        return subLinks;
    }

    public void setSubLinks(List<LinkAddressVo> subLinks) {
        this.subLinks = subLinks;
    }

    public List<LinkPrivilegeVo> getLinkPrivileges() {
        return linkPrivileges;
    }

    public void setLinkPrivileges(List<LinkPrivilegeVo> linkPrivileges) {
        this.linkPrivileges = linkPrivileges;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LinkAddressVo that = (LinkAddressVo) o;
        return Objects.equals(linkCode, that.linkCode) &&
                Objects.equals(linkName, that.linkName) &&
                Objects.equals(parentId, that.parentId) &&
                Objects.equals(linkAddress, that.linkAddress);
    }

    @Override
    public int hashCode() {

        return Objects.hash(linkCode, linkName, parentId, linkAddress);
    }
}
