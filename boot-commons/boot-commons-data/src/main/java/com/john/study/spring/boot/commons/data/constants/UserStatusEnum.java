package com.john.study.spring.boot.commons.data.constants;

import com.john.common.dao.data.data.IKeyTextEnum;

/**
 * 用户状态枚举
 * 0新注册 1正常 2过期 3锁定 4注销
 *
 * @author jianguangtao03382 2018/4/18
 */
public enum UserStatusEnum implements IKeyTextEnum<UserStatusEnum> {
    NEW("0", "新注册"),
    NORMAL("1", "正常"),
    EXPIRED("2", "过期"),
    LOCKED("3", "锁定"),
    DESTROYED("4", "注销");

    private String key;
    private String text;

    private UserStatusEnum(String key, String text) {
        this.key = key;
        this.text = text;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public boolean equals(String key) {
        return this.key.equals(key);
    }
}
