package com.john.study.spring.boot.commons.data.vo;

import com.john.common.dao.data.data.BaseBusinessPagerVo;
import com.john.study.spring.boot.commons.data.constants.RoleTypeEnum;

/**
 * 基本的角色信息
 *
 * @author jiangguangtao@foxmail.com 2014年5月16日
 */
public class RoleVo extends BaseBusinessPagerVo<Long> {
    private static final long serialVersionUID = -4187173245918646454L;
    /**
     * 角色编码
     */
    private String roleCode;
    /**
     * 角色名称
     */
    private String roleName;
    /**
     * 角色类型
     */
    private String roleType;
    /**
     * 是否启用标识
     */
    private String enableFlag;
    /**
     * 角色备注
     */
    private String remark;

    public RoleVo() {
        super();
    }

    /**
     * 参数化新建一个角色
     *
     * @param roleCode 角色编码
     * @param roleName 角色名称
     */
    public RoleVo(String roleCode, String roleName) {
        super();
        this.roleCode = roleCode;
        this.roleName = roleName;
        this.enableFlag = "1";
        this.roleType = RoleTypeEnum.NORMAL.getKey();
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    public String getEnableFlag() {
        return enableFlag;
    }

    public void setEnableFlag(String enableFlag) {
        this.enableFlag = enableFlag;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
