package com.john.study.spring.boot.commons.data.vo;

import com.john.common.dao.data.data.BaseBusinessVo;

import java.util.Objects;

/**
 * 功能树权限vo
 *
 * @author jianguangtao03382 2018/4/26
 */
public class LinkPrivilegeVo extends BaseBusinessVo<Long> {
    private static final long serialVersionUID = -1755013493720816103L;
    /**
     * 所属链接id
     */
    private Long linkId;
    /**
     * 权限编码
     */
    private String privilegeCode;
    /**
     * 权限名称 描述
     */
    private String privilegeName;


    @Override
    public Long getId() {
        return super.getId();
    }

    @Override
    public void setId(Long id) {
        super.setId(id);
    }

    public Long getLinkId() {
        return linkId;
    }

    public void setLinkId(Long linkId) {
        this.linkId = linkId;
    }

    public String getPrivilegeCode() {
        return privilegeCode;
    }

    public void setPrivilegeCode(String privilegeCode) {
        this.privilegeCode = privilegeCode;
    }

    public String getPrivilegeName() {
        return privilegeName;
    }

    public void setPrivilegeName(String privilegeName) {
        this.privilegeName = privilegeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LinkPrivilegeVo that = (LinkPrivilegeVo) o;
        return Objects.equals(linkId, that.linkId) &&
                Objects.equals(privilegeCode, that.privilegeCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(linkId, privilegeCode);
    }
}
