package com.john.study.spring.boot.commons.data.constants;

import com.john.common.dao.data.data.IKeyTextEnum;
import org.apache.commons.lang3.StringUtils;

/**
 * 角色类型枚举
 * <p>
 * normal:一般角色，privilege:权限角色
 *
 * @author jiangguangtao@foxmail.com 2016/4/16
 */
public enum RoleTypeEnum implements IKeyTextEnum<RoleTypeEnum> {
    NORMAL("normal", "一般角色"),
    PRIVILEGE("privilege", "权限角色");

    private String key;
    private String text;

    private RoleTypeEnum(String key, String text) {
        this.key = key;
        this.text = text;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public boolean equals(String key) {
        return StringUtils.equals(this.getKey(), key);
    }

    @Override
    public String toString() {
        return "RoleTypeEnum{" +
                "key='" + key + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
