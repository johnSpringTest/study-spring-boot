package com.john.study.spring.boot.commons.data.vo;

import com.john.common.dao.data.data.BaseBusinessVo;

import java.util.Objects;

/**
 * 角色关联的功能权限上的权限映射
 *
 * @author jianguangtao03382 2018/4/26
 */
public class RoleLinkPrivilegeVo extends BaseBusinessVo<Long> {
    private static final long serialVersionUID = 4165232939869220696L;
    /**
     * 角色id
     */
    private Long roleId;

    /**
     * 权限所属链接id
     */
    private Long linkId;
    /**
     * 链接权限id
     */
    private Long linkPrivilegeId;

    /**
     * 角色编码
     */
    private String roleCode;

    /**
     * 链接编码
     */
    private String linkCode;
    /**
     * 用户id、查询参数
     */
    private Long userId;
    /**
     * 链接的是否启用标识
     */
    private String linkEnableFlag;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getLinkId() {
        return linkId;
    }

    public void setLinkId(Long linkId) {
        this.linkId = linkId;
    }

    public Long getLinkPrivilegeId() {
        return linkPrivilegeId;
    }

    public void setLinkPrivilegeId(Long linkPrivilegeId) {
        this.linkPrivilegeId = linkPrivilegeId;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getLinkCode() {
        return linkCode;
    }

    public void setLinkCode(String linkCode) {
        this.linkCode = linkCode;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getLinkEnableFlag() {
        return linkEnableFlag;
    }

    public void setLinkEnableFlag(String linkEnableFlag) {
        this.linkEnableFlag = linkEnableFlag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoleLinkPrivilegeVo that = (RoleLinkPrivilegeVo) o;
        return Objects.equals(roleId, that.roleId) &&
                Objects.equals(linkId, that.linkId) &&
                Objects.equals(linkPrivilegeId, that.linkPrivilegeId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(roleId, linkId, linkPrivilegeId);
    }
}
