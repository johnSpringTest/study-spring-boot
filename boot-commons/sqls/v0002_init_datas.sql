-- 初始化一批测试用户
INSERT INTO t_user (user_name, nick_name, email, user_status, enable_date, remark)
VALUES ('admin', '系统管理员', 'admin@email.com', '1', now(), NULL);
INSERT INTO t_user (user_name, nick_name, email, user_status, enable_date, remark)
VALUES ('john', 'John Jiang', 'john@email.com', '1', now(), NULL);
INSERT INTO t_user (user_name, nick_name, email, user_status, enable_date, remark)
VALUES ('peter', 'Peter Mao', 'peter@email.com', '0', NULL, NULL);
INSERT INTO t_user (user_name, nick_name, email, user_status, enable_date, remark)
VALUES ('elly', 'Elly Jil', 'elly@email.com', '2', NULL, NULL);


-- 初始化角色
INSERT INTO t_role(role_code, role_name, role_type, enable_flag) VALUES ('ROLE_admin','系统管理员','privilege','1');
INSERT INTO t_role(role_code, role_name, role_type, enable_flag) VALUES ('ROLE_user','系统一般用户','privilege','1');
INSERT INTO t_role(role_code, role_name, role_type, enable_flag) VALUES ('ROLE_userAdmin','用户管理员','privilege','1');
INSERT INTO t_role(role_code, role_name, role_type, enable_flag) VALUES ('ROLE_single','测试单权限','privilege','1');

-- 初始化菜单
INSERT INTO t_link_address (link_code, link_name, link_level, folder_flag, remark) VALUES ('ROOT', '根菜单结点', 0, '1' ,'所有的菜单从这儿开始');

INSERT INTO t_link_address(link_code, link_name, parent_id, link_level, link_icon_class, link_address, link_target, view_index, folder_flag)
    SELECT 'RM01','后台管理根菜单', id, 1, null, null, null, 1, '1' FROM t_link_address t WHERE t.link_code='ROOT';
INSERT INTO t_link_address(link_code, link_name, parent_id, link_level, link_icon_class, link_address, link_target, view_index, folder_flag)
  SELECT 'RM02','用户前台菜单', id, 1, null, null, null, 2, '1' FROM t_link_address t WHERE t.link_code='ROOT';

-- 后台菜单的二级菜单
INSERT INTO t_link_address(link_code, link_name, parent_id, link_level, link_icon_class, link_address, link_target, view_index, folder_flag)
  SELECT 'RM01A001','工作台', id, 2, 'fa-dashboard', '/admin/home.htm', null, 1, '0' FROM t_link_address t WHERE t.link_code='RM01';
INSERT INTO t_link_address(link_code, link_name, parent_id, link_level, link_icon_class, link_address, link_target, view_index, folder_flag)
  SELECT 'RM01A003','订单中心', id, 2, 'fa-order', null, null, 3, '1' FROM t_link_address t WHERE t.link_code='RM01';
INSERT INTO t_link_address(link_code, link_name, parent_id, link_level, link_icon_class, link_address, link_target, view_index, folder_flag)
  SELECT 'RM01A050','系统管理', id, 2, 'fa-gears', null, null, 50, '1' FROM t_link_address t WHERE t.link_code='RM01';
INSERT INTO t_link_address(link_code, link_name, parent_id, link_level, link_icon_class, link_address, link_target, view_index, folder_flag)
  SELECT 'RM01A060','报表设计中心', id, 2, 'fa-report', null, null, 60, '1' FROM t_link_address t WHERE t.link_code='RM01';

-- 后台三级菜单 订单中心
INSERT INTO t_link_address(link_code, link_name, parent_id, link_level, link_icon_class, link_address, link_target, view_index, folder_flag)
  SELECT 'RM01A003B001','订单管理', id, 3, 'fa-order-list', '/order/orderList.htm', null, 1, '0' FROM t_link_address t WHERE t.link_code='RM01A003';
INSERT INTO t_link_address(link_code, link_name, parent_id, link_level, link_icon_class, link_address, link_target, view_index, folder_flag)
  SELECT 'RM01A003B003','年度订单汇总', id, 3, 'fa-table', '/order/toAnualOrderCollect.htm', null, 3, '0' FROM t_link_address t WHERE t.link_code='RM01A003';
INSERT INTO t_link_address(link_code, link_name, parent_id, link_level, link_icon_class, link_address, link_target, view_index, folder_flag)
  SELECT 'RM01A003B005','月度订单汇总', id, 3, 'fa-table', '/order/toMonthOrderCollect.htm', null, 5, '0' FROM t_link_address t WHERE t.link_code='RM01A003';

-- 后台三级菜单 系统管理
INSERT INTO t_link_address(link_code, link_name, parent_id, link_level, link_icon_class, link_address, link_target, view_index, folder_flag)
  SELECT 'RM01A050B001','用户管理', id, 3, '', '/admin/user/toUserList.htm', null, 1, '0' FROM t_link_address t WHERE t.link_code='RM01A050';
INSERT INTO t_link_address(link_code, link_name, parent_id, link_level, link_icon_class, link_address, link_target, view_index, folder_flag)
  SELECT 'RM01A050B003','机构管理', id, 3, '', '/admin/org/toOrgList.htm', null, 3, '0' FROM t_link_address t WHERE t.link_code='RM01A050';
INSERT INTO t_link_address(link_code, link_name, parent_id, link_level, link_icon_class, link_address, link_target, view_index, folder_flag)
  SELECT 'RM01A050B005','角色管理', id, 3, '', '/admin/role/toRoleList.htm', null, 5, '0' FROM t_link_address t WHERE t.link_code='RM01A050';
INSERT INTO t_link_address(link_code, link_name, parent_id, link_level, link_icon_class, link_address, link_target, view_index, folder_flag)
  SELECT 'RM01A050B007','功能链接管理', id, 3, '', '/admin/linkaddress/toLinkAddressList.htm', null, 7, '0' FROM t_link_address t WHERE t.link_code='RM01A050';

-- 后台三级菜单 报表设计中心
INSERT INTO t_link_address(link_code, link_name, parent_id, link_level, link_address, link_target, view_index, folder_flag)
  SELECT 'RM01A060B001','访问日志查询', id, 3, '/report/toAccessLog.htm', null, 1, '0' FROM t_link_address t WHERE t.link_code='RM01A060';
INSERT INTO t_link_address(link_code, link_name, parent_id, link_level, link_address, link_target, view_index, folder_flag)
  SELECT 'RM01A060B003','最高访问链接查询', id, 3, '/report/toHighAccessLink.htm', null, 3, '0' FROM t_link_address t WHERE t.link_code='RM01A060';

-- 前台菜单的二级菜单
INSERT INTO t_link_address(link_code, link_name, parent_id, link_level, link_icon_class, link_address, link_target, view_index, folder_flag)
  SELECT 'RM02A001','首页', id, 2, 'fa-dashboard', '/home.htm', null, 1, '0' FROM t_link_address t WHERE t.link_code='RM02';
INSERT INTO t_link_address(link_code, link_name, parent_id, link_level, link_icon_class, link_address, link_target, view_index, folder_flag)
  SELECT 'RM02A003','订单中心', id, 2, 'fa-order', null, null, 3, '0' FROM t_link_address t WHERE t.link_code='RM02';
INSERT INTO t_link_address(link_code, link_name, parent_id, link_level, link_icon_class, link_address, link_target, view_index, folder_flag)
  SELECT 'RM02A005','关于我们', id, 2, 'fa-gears', null, null, 5, '0' FROM t_link_address t WHERE t.link_code='RM02';
INSERT INTO t_link_address(link_code, link_name, parent_id, link_level, link_icon_class, link_address, link_target, view_index, folder_flag)
  SELECT 'RM02A007','加入我们', id, 2, 'fa-report', null, null, 7, '0' FROM t_link_address t WHERE t.link_code='RM02';


-- 功能链接权限初始化
INSERT INTO t_link_privilege(link_id, privilege_code, privilege_name)
  SELECT t.id, concat(t.link_code,'_view_user_statics'),'查看用户统计简报' from t_link_address t where t.link_code='RM01A001';
INSERT INTO t_link_privilege(link_id, privilege_code, privilege_name)
  SELECT t.id, concat(t.link_code,'_view_order_statics'), '查看订单统计简报' from t_link_address t where t.link_code='RM01A001';
INSERT INTO t_link_privilege(link_id, privilege_code, privilege_name)
  SELECT t.id, concat(t.link_code,'_show_work_board'), '显示工作中心' from t_link_address t where t.link_code='RM01A001';

INSERT INTO t_link_privilege(link_id, privilege_code, privilege_name)
  SELECT t.id, concat(t.link_code,'_query'), '查询' from t_link_address t where t.link_code='RM01A003B001';
INSERT INTO t_link_privilege(link_id, privilege_code, privilege_name)
  SELECT t.id, concat(t.link_code,'_export'), '导出' from t_link_address t where t.link_code='RM01A003B001';

INSERT INTO t_link_privilege(link_id, privilege_code, privilege_name)
  SELECT t.id, concat(t.link_code,'_export'), '导出年报表' from t_link_address t where t.link_code='RM01A003B003';
INSERT INTO t_link_privilege(link_id, privilege_code, privilege_name)
  SELECT t.id, concat(t.link_code,'_export'), '导出月报表' from t_link_address t where t.link_code='RM01A003B005';

INSERT INTO t_link_privilege(link_id, privilege_code, privilege_name)
  SELECT t.id, concat(t.link_code,'_add'), '新增用户' from t_link_address t where t.link_code='RM01A050B001';
INSERT INTO t_link_privilege(link_id, privilege_code, privilege_name)
  SELECT t.id, concat(t.link_code,'_edit'), '修改用户' from t_link_address t where t.link_code='RM01A050B001';
INSERT INTO t_link_privilege(link_id, privilege_code, privilege_name)
  SELECT t.id, concat(t.link_code,'_delete'), '删除用户' from t_link_address t where t.link_code='RM01A050B001';
INSERT INTO t_link_privilege(link_id, privilege_code, privilege_name)
  SELECT t.id, concat(t.link_code,'_reset_password'), '重置密码' from t_link_address t where t.link_code='RM01A050B001';
INSERT INTO t_link_privilege(link_id, privilege_code, privilege_name)
  SELECT t.id, concat(t.link_code,'_auth'), '用户授权' from t_link_address t where t.link_code='RM01A050B001';
INSERT INTO t_link_privilege(link_id, privilege_code, privilege_name)
  SELECT t.id, concat(t.link_code,'_export'), '导出用户列表' from t_link_address t where t.link_code='RM01A050B001';

INSERT INTO t_link_privilege(link_id, privilege_code, privilege_name)
  SELECT t.id, concat(t.link_code,'_add'), '添加机构' from t_link_address t where t.link_code='RM01A050B003';
INSERT INTO t_link_privilege(link_id, privilege_code, privilege_name)
  SELECT t.id, concat(t.link_code,'_edit'), '更新机构' from t_link_address t where t.link_code='RM01A050B003';
INSERT INTO t_link_privilege(link_id, privilege_code, privilege_name)
  SELECT t.id, concat(t.link_code,'_delete'), '删除机构' from t_link_address t where t.link_code='RM01A050B003';
INSERT INTO t_link_privilege(link_id, privilege_code, privilege_name)
  SELECT t.id, concat(t.link_code,'_export'), '导出机构' from t_link_address t where t.link_code='RM01A050B003';

INSERT INTO t_link_privilege(link_id, privilege_code, privilege_name)
  SELECT t.id, concat(t.link_code,'_add'), '添加角色' from t_link_address t where t.link_code='RM01A050B005';
INSERT INTO t_link_privilege(link_id, privilege_code, privilege_name)
  SELECT t.id, concat(t.link_code,'_edit'), '更新角色' from t_link_address t where t.link_code='RM01A050B005';
INSERT INTO t_link_privilege(link_id, privilege_code, privilege_name)
  SELECT t.id, concat(t.link_code,'_delete'), '删除角色' from t_link_address t where t.link_code='RM01A050B005';
INSERT INTO t_link_privilege(link_id, privilege_code, privilege_name)
  SELECT t.id, concat(t.link_code,'_auth'), '角色授权' from t_link_address t where t.link_code='RM01A050B005';
INSERT INTO t_link_privilege(link_id, privilege_code, privilege_name)
  SELECT t.id, concat(t.link_code,'_export'), '导出角色' from t_link_address t where t.link_code='RM01A050B005';


INSERT INTO t_link_privilege(link_id, privilege_code, privilege_name)
  SELECT t.id, concat(t.link_code,'_add'), '添加功能链接' from t_link_address t where t.link_code='RM01A050B007';
INSERT INTO t_link_privilege(link_id, privilege_code, privilege_name)
  SELECT t.id, concat(t.link_code,'_edit'), '更新功能链接' from t_link_address t where t.link_code='RM01A050B007';
INSERT INTO t_link_privilege(link_id, privilege_code, privilege_name)
  SELECT t.id, concat(t.link_code,'_delete'), '删除功能链接' from t_link_address t where t.link_code='RM01A050B007';

INSERT INTO t_link_privilege(link_id, privilege_code, privilege_name)
  SELECT t.id, concat(t.link_code,'_export'), '导出访问日志' from t_link_address t where t.link_code='RM01A060B001';
INSERT INTO t_link_privilege(link_id, privilege_code, privilege_name)
  SELECT t.id, concat(t.link_code,'_export'), '导出最高访问链接' from t_link_address t where t.link_code='RM01A060B003';

-- 角色-功能授权
INSERT INTO t_role_link(role_id, link_id)
  SELECT r.id, l.id from t_role r, t_link_address l
  where r.role_code='ROLE_admin'
  and l.link_code LIKE 'RM01%';
INSERT INTO t_role_link_privilege(role_id, link_id, link_privilege_id)
  SELECT r.id, p.link_id, p.id from t_role r , t_link_privilege p
  where r.role_code='ROLE_admin';

-- 一般用户授权
INSERT INTO t_role_link(role_id, link_id)
  SELECT r.id, l.id from t_role r, t_link_address l
  where r.role_code='ROLE_user'
        and (l.link_code LIKE 'RM01A001%' OR l.link_code LIKE 'RM01A003%');

INSERT INTO t_role_link_privilege(role_id, link_id, link_privilege_id)
  SELECT r.id, p.link_id, p.id
  from t_role r ,t_role_link rl, t_link_privilege p
  where r.role_code='ROLE_user'
  and r.id = rl.role_id
  and rl.link_id = p.link_id
  ;

-- 用户角色授权
INSERT INTO t_user_role (user_id, role_id, role_code)
  SELECT u.id, r.id, r.role_code
  FROM t_user u , t_role r
  where u.user_name='admin'
  and r.role_code='ROLE_admin';

INSERT INTO t_user_role (user_id, role_id, role_code)
  SELECT u.id, r.id, r.role_code
  FROM t_user u , t_role r
  where u.user_name='john'
        and r.role_code='ROLE_user';
INSERT INTO t_user_role (user_id, role_id, role_code)
  SELECT u.id, r.id, r.role_code
  FROM t_user u , t_role r
  where u.user_name='peter'
        and r.role_code='ROLE_single';