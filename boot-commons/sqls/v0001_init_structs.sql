-- auto-generated definition

use demo;

DROP TABLE  IF EXISTS t_business_demo;
CREATE TABLE t_business_demo (
  id BIGINT AUTO_INCREMENT PRIMARY KEY COMMENT '记录id',
#   in this line go you columns
  creator_id BIGINT DEFAULT 0 COMMENT '创建人id',
  create_time TIMESTAMP DEFAULT now() COMMENT '记录创建时间',
  updater_id BIGINT DEFAULT 0 COMMENT '记录最后更新人id',
  update_time DATETIME DEFAULT NULL COMMENT '记录最后更新时间',
  record_status CHAR(1) DEFAULT '0' COMMENT '记录状态 0正常 1删除'
) COMMENT '业务信息表-示例';


DROP TABLE  IF EXISTS t_user;
CREATE TABLE t_user (
  id BIGINT AUTO_INCREMENT PRIMARY KEY COMMENT '记录id',
  user_name VARCHAR(40) NOT NULL COMMENT '用户名，登录名',
  nick_name VARCHAR(50) NOT NULL COMMENT '用户昵称',
  email VARCHAR(50) NOT NULL COMMENT '用户邮箱',
  password VARCHAR(64) DEFAULT NULL COMMENT '用户密码',
  org_id BIGINT DEFAULT NULL COMMENT '所属组织id',
  phone VARCHAR(15) DEFAULT NULL COMMENT '手机',
  qq VARCHAR(15) DEFAULT NULL COMMENT 'qq',
  user_status VARCHAR(2) DEFAULT '0' COMMENT '用户状态 0新注册 1正常 2过期 3锁定 4注销',
  enable_date DATETIME DEFAULT NULL COMMENT '启用日期 新注册用户激活的日期',
  expire_date DATETIME DEFAULT NULL COMMENT '失效日期',
  remark VARCHAR(200) DEFAULT NULL COMMENT '用户备注',

  creator_id BIGINT DEFAULT 0 COMMENT '创建人id',
  create_time TIMESTAMP DEFAULT NOW() COMMENT '记录创建时间',
  updater_id BIGINT DEFAULT 0 COMMENT '记录最后更新人id',
  update_time DATETIME DEFAULT NOW() COMMENT '记录最后更新时间',
  record_status CHAR(1) DEFAULT '1' COMMENT '记录状态 1:正常，2:删除；3:禁用'
) COMMENT '用户信息表';

DROP TABLE  IF EXISTS t_role;
CREATE TABLE t_role (
  id BIGINT AUTO_INCREMENT PRIMARY KEY COMMENT '记录id',
  role_code VARCHAR(30) NOT NULL COMMENT '角色编码',
  role_name VARCHAR(50) NOT NULL COMMENT '角色名称',
  role_type VARCHAR(20) DEFAULT 'normal' COMMENT '角色类型 normal:一般角色，privilege:权限角色',
  enable_flag CHAR(1) DEFAULT '1' COMMENT '启用标识 1是 0否',
  remark VARCHAR(200) DEFAULT NULL COMMENT '角色备注',

  creator_id BIGINT DEFAULT 0 COMMENT '创建人id',
  create_time TIMESTAMP DEFAULT NOW() COMMENT '记录创建时间',
  updater_id BIGINT DEFAULT 0 COMMENT '记录最后更新人id',
  update_time DATETIME DEFAULT NOW() COMMENT '记录最后更新时间',
  record_status CHAR(1) DEFAULT '1' COMMENT '记录状态 1:正常，2:删除；3:禁用',
  INDEX idx_role_code (role_code)
) COMMENT '角色信息表';


DROP TABLE  IF EXISTS t_user_role;
CREATE TABLE t_user_role (
  id BIGINT AUTO_INCREMENT PRIMARY KEY COMMENT '记录id',
  user_id BIGINT NOT NULL COMMENT '用户ID',
  role_id BIGINT NOT NULL COMMENT '角色ID',
  role_code VARCHAR(30) DEFAULT NULL COMMENT '角色名称',
  INDEX idx_user_role01 (user_id, role_id)
) COMMENT '用户角色关联表';

DROP TABLE  IF EXISTS t_link_address;
CREATE TABLE t_link_address (
  id BIGINT AUTO_INCREMENT PRIMARY KEY COMMENT '记录id',
  link_code VARCHAR(30) NOT NULL COMMENT '链接编码',
  link_name VARCHAR(100) NOT NULL COMMENT '链接显示名称',
  parent_id BIGINT DEFAULT NULL COMMENT '上级链接id 根链接为null',
  link_type VARCHAR(4) DEFAULT NULL COMMENT '链接类型 暂时未使用',
  link_level TINYINT DEFAULT 0 COMMENT '链接层级 0 1 2 3',
  view_index TINYINT DEFAULT 0 COMMENT '显示顺序',
  folder_flag CHAR(1) DEFAULT '1' COMMENT '是否为目录标识 1是 0否',
  enable_flag CHAR(1) DEFAULT '1' COMMENT '是否启用标识 1是 0否',
  link_icon_class VARCHAR(50) DEFAULT NULL COMMENT '链接显示样式 css类名',
  link_address VARCHAR(200) DEFAULT NULL COMMENT '链接的具体地址',
  link_target VARCHAR(30) DEFAULT NULL COMMENT '打开链接的方式 见html a标签属性',
  remark VARCHAR(200) DEFAULT NULL COMMENT '链接备注信息',

  creator_id BIGINT DEFAULT 0 COMMENT '创建人id',
  create_time TIMESTAMP DEFAULT NOW() COMMENT '记录创建时间',
  updater_id BIGINT DEFAULT 0 COMMENT '记录最后更新人id',
  update_time DATETIME DEFAULT NOW() COMMENT '记录最后更新时间',
  record_status CHAR(1) DEFAULT '1' COMMENT '记录状态 1:正常，2:删除；3:禁用',
  INDEX idx_link_address_code (link_code),
  INDEX idx_link_address_pid (parent_id, enable_flag)
) COMMENT '功能链接信息表';

DROP TABLE  IF EXISTS t_link_privilege;
CREATE TABLE t_link_privilege (
  id BIGINT AUTO_INCREMENT PRIMARY KEY COMMENT '记录id',
  link_id BIGINT NOT NULL COMMENT '所属链接id',
  privilege_code VARCHAR(40) NOT NULL COMMENT '权限编码',
  privilege_name VARCHAR(50) DEFAULT NULL COMMENT '权限名称 描述',
  INDEX idx_link_privilege_code (link_id, privilege_code)
) COMMENT '链接上挂的权限列表';

DROP TABLE  IF EXISTS t_role_link;
CREATE TABLE t_role_link (
  id BIGINT AUTO_INCREMENT PRIMARY KEY COMMENT '记录id',
  role_id BIGINT NOT NULL COMMENT '角色id',
  link_id BIGINT NOT NULL COMMENT '链接记录id',
  INDEX idx_role_link01 (role_id, link_id)
) COMMENT '角色与链接关联信息';


DROP TABLE  IF EXISTS t_role_link_privilege;
CREATE TABLE t_role_link_privilege (
  id BIGINT AUTO_INCREMENT PRIMARY KEY COMMENT '记录id',
  role_id BIGINT NOT NULL COMMENT '角色id',
  link_id BIGINT NOT NULL COMMENT '链接记录id',
  link_privilege_id BIGINT NOT NULL COMMENT '链接权限id',
  INDEX idx_role_link_privilege01 (role_id, link_id)
) COMMENT '角色与链接关联信息';
