package com.john.study.spring.boot.commons.service.impl;

import com.john.common.dao.data.data.PaginationResult;
import com.john.common.dao.data.data.RecordStatusEnum;
import com.john.study.spring.boot.commons.dao.IUserDao;
import com.john.study.spring.boot.commons.dao.entity.UserEntity;
import com.john.study.spring.boot.commons.data.vo.LinkPrivilegeVo;
import com.john.study.spring.boot.commons.data.vo.RoleVo;
import com.john.study.spring.boot.commons.data.vo.UserVo;
import com.john.study.spring.boot.commons.service.IRoleLinkPrivilegeService;
import com.john.study.spring.boot.commons.service.IUserRoleService;
import com.john.study.spring.boot.commons.service.IUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author jianguangtao03382 2018/4/19
 */
@Service
public class UserServiceImpl extends EntityServiceImpl<UserEntity, IUserDao> implements IUserService {
    private IUserRoleService userRoleService;
    private IRoleLinkPrivilegeService roleLinkPrivilegeService;


    @Resource
    public void setUserRoleService(IUserRoleService userRoleService) {
        this.userRoleService = userRoleService;
    }

    @Resource
    public void setRoleLinkPrivilegeService(IRoleLinkPrivilegeService roleLinkPrivilegeService) {
        this.roleLinkPrivilegeService = roleLinkPrivilegeService;
    }

    @Override
    public UserVo findByUsername(String username) {
        if (StringUtils.isBlank(username)) {
            return null;
        }
        UserVo vo = new UserVo();
        vo.setUserName(username);
        vo.setRecordStatus(RecordStatusEnum.NORMAL.getKey());

        return super.findByCnd(vo, UserVo.class);
    }

    @Override
    public UserVo findSecurityUserByUsername(String username) {
        UserVo userVo = findByUsername(username);
        if (null != userVo) {
            List<RoleVo> list = userRoleService.queryRoleListByUserId(userVo.getId());
            userVo.setRoleList(list);
            List<LinkPrivilegeVo> linkPrivilegeVos = roleLinkPrivilegeService.queryLinkPrivilegeListByUserId(userVo.getId());
            userVo.setPrivilegeList(linkPrivilegeVos);
        }

        return userVo;
    }

    @Override
    public PaginationResult<UserVo> queryUserListPage(UserVo queryVo) {
        queryVo.setRecordStatus(RecordStatusEnum.NORMAL.getKey());
        return getDal().queryUserListPage(queryVo);
    }

    @Override
    @Transactional
    public Long createUser(UserVo vo) {
        return null;
    }
}
