package com.john.study.spring.boot.commons.service.impl;

import com.john.common.dao.data.data.RecordStatusEnum;
import com.john.common.dao.data.data.YesNoEnum;
import com.john.study.spring.boot.commons.dao.IRoleLinkPrivilegeDao;
import com.john.study.spring.boot.commons.dao.entity.RoleLinkPrivilegeEntity;
import com.john.study.spring.boot.commons.data.vo.LinkPrivilegeVo;
import com.john.study.spring.boot.commons.data.vo.RoleLinkPrivilegeVo;
import com.john.study.spring.boot.commons.service.IRoleLinkPrivilegeService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author jianguangtao03382 2018/4/26
 */
@Service
public class RoleLinkPrivilegeServiceImpl extends EntityServiceImpl<RoleLinkPrivilegeEntity, IRoleLinkPrivilegeDao>
        implements IRoleLinkPrivilegeService {
    @Override
    public List<LinkPrivilegeVo> queryLinkPrivilegeListByUserId(Long userId) {
        RoleLinkPrivilegeVo vo = new RoleLinkPrivilegeVo();
        vo.setUserId(userId);
        vo.setLinkEnableFlag(YesNoEnum.YES.getKey());
        vo.setRecordStatus(RecordStatusEnum.NORMAL.getKey());

        return getDal().queryLinkPrivilegeListByUserId(vo);
    }
}
