package com.john.study.spring.boot.commons.service;

import com.john.common.dalservice.IEntityService;
import com.john.study.spring.boot.commons.dao.IRoleLinkPrivilegeDao;
import com.john.study.spring.boot.commons.dao.entity.RoleLinkPrivilegeEntity;
import com.john.study.spring.boot.commons.data.vo.LinkPrivilegeVo;

import java.util.List;

/**
 * @author jianguangtao03382 2018/4/26
 */
public interface IRoleLinkPrivilegeService extends IEntityService<RoleLinkPrivilegeEntity, IRoleLinkPrivilegeDao> {
    /**
     * 根据用户id查询此用户关联的功能链接权限列表
     *
     * @param userId 用户id
     * @return 功能链接权限列表
     */
    List<LinkPrivilegeVo> queryLinkPrivilegeListByUserId(Long userId);
}
