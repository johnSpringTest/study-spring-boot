package com.john.study.spring.boot.commons.service;

import com.john.common.dalservice.IEntityService;
import com.john.study.spring.boot.commons.dao.IRoleLinkDao;
import com.john.study.spring.boot.commons.dao.entity.RoleLinkEntity;

/**
 * @author jianguangtao03382 2018/4/26
 */
public interface IRoleLinkService extends IEntityService<RoleLinkEntity, IRoleLinkDao> {
}
