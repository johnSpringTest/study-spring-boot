package com.john.study.spring.boot.commons.service.impl;

import com.john.study.spring.boot.commons.dao.IRoleDao;
import com.john.study.spring.boot.commons.dao.entity.RoleEntity;
import com.john.study.spring.boot.commons.service.IRoleService;
import org.springframework.stereotype.Service;

/**
 * @author jianguangtao03382 2018/4/25
 */
@Service
public class RoleServiceImpl extends EntityServiceImpl<RoleEntity, IRoleDao> implements IRoleService {
}
