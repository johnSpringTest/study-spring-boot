package com.john.study.spring.boot.commons.service;

import com.john.common.dalservice.IEntityService;
import com.john.common.dao.data.data.PaginationResult;
import com.john.study.spring.boot.commons.dao.IUserDao;
import com.john.study.spring.boot.commons.dao.entity.UserEntity;
import com.john.study.spring.boot.commons.data.vo.UserVo;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户服务类
 *
 * @author jianguangtao03382 2018/4/17
 */
public interface IUserService extends IEntityService<UserEntity, IUserDao> {
    /**
     * 根据用户名称（登录名）查询用户信息
     *
     * @param username 用户名
     * @return 用户vo
     */
    UserVo findByUsername(String username);

    /**
     * 根据用户名称（登录名）查询用户信息
     * <p>
     * 用于用户登录，加载权限信息
     *
     * @param username 用户名
     * @return 用户vo
     */
    UserVo findSecurityUserByUsername(String username);

    /**
     * 分页查询用户信息
     *
     * @param queryVo 查询参数
     * @return 用户信息分页列表
     */
    PaginationResult<UserVo> queryUserListPage(UserVo queryVo);

    /**
     * 创建用户
     *
     * @param vo 新增用户参数
     * @return 记录id
     */
    @Transactional
    Long createUser(UserVo vo);
}
