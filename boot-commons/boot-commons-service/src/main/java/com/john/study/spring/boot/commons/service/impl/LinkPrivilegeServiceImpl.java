package com.john.study.spring.boot.commons.service.impl;

import com.john.study.spring.boot.commons.dao.ILinkPrivilegeDao;
import com.john.study.spring.boot.commons.dao.entity.LinkPrivilegeEntity;
import com.john.study.spring.boot.commons.service.ILinkPrivilegeService;
import org.springframework.stereotype.Service;

/**
 * @author jianguangtao03382 2018/4/26
 */
@Service
public class LinkPrivilegeServiceImpl extends EntityServiceImpl<LinkPrivilegeEntity, ILinkPrivilegeDao>
        implements ILinkPrivilegeService {
}
