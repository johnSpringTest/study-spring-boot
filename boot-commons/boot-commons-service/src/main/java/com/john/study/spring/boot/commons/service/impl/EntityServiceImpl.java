package com.john.study.spring.boot.commons.service.impl;

import com.john.common.dal.IBaseDal;
import com.john.common.dalservice.AbstractEntityServiceImpl;
import com.john.common.dao.data.entity.IEntity;
import com.john.study.spring.boot.commons.data.vo.UserVo;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * 本地实现的基本服务类
 *
 * @author jianguangtao03382 2018/4/19
 */
public class EntityServiceImpl<E extends IEntity, D extends IBaseDal<E>> extends AbstractEntityServiceImpl<E, D> {
    @Override
    public Long getLoginUserId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (null == authentication) {
            return -1L;
        }
        UserVo userVo = (UserVo) authentication.getPrincipal();
        return userVo.getId();
    }
}
