package com.john.study.spring.boot.commons.service.impl;

import com.john.study.spring.boot.commons.data.vo.UserVo;
import com.john.study.spring.boot.commons.service.IUserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author jianguangtao03382 2018/4/19
 */
@Service
public class UserDetailServiceImpl implements UserDetailsService {
    private IUserService userService;

    @Resource
    public void setUserService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserVo user = userService.findSecurityUserByUsername(username);
        if (null == user) {
            throw new UsernameNotFoundException(username);
        }

        return user;
    }
}
