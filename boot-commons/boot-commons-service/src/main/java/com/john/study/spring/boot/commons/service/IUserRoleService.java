package com.john.study.spring.boot.commons.service;

import com.john.common.dalservice.IEntityService;
import com.john.study.spring.boot.commons.dao.IUserRoleDao;
import com.john.study.spring.boot.commons.dao.entity.UserRoleEntity;
import com.john.study.spring.boot.commons.data.vo.RoleVo;

import java.util.List;

/**
 * @author jianguangtao03382 2018/4/25
 */
public interface IUserRoleService extends IEntityService<UserRoleEntity, IUserRoleDao> {
    /**
     * 根据用户id，查询用户对应的角色列表
     *
     * @param userId 用户id
     * @return 角色列表
     */
    List<RoleVo> queryRoleListByUserId(Long userId);
}
