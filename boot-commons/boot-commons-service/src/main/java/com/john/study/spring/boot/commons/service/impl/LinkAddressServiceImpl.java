package com.john.study.spring.boot.commons.service.impl;

import com.john.study.spring.boot.commons.dao.ILinkAddressDao;
import com.john.study.spring.boot.commons.dao.entity.LinkAddressEntity;
import com.john.study.spring.boot.commons.service.ILinkAddressService;
import org.springframework.stereotype.Service;

/**
 * @author jianguangtao03382 2018/4/26
 */
@Service
public class LinkAddressServiceImpl extends EntityServiceImpl<LinkAddressEntity, ILinkAddressDao>
        implements ILinkAddressService {
}
