package com.john.study.spring.boot.commons.service;

import com.john.common.dalservice.IEntityService;
import com.john.study.spring.boot.commons.dao.ILinkAddressDao;
import com.john.study.spring.boot.commons.dao.entity.LinkAddressEntity;

/**
 * @author jianguangtao03382 2018/4/26
 */
public interface ILinkAddressService extends IEntityService<LinkAddressEntity, ILinkAddressDao> {
}
