package com.john.study.spring.boot.commons.service;

import com.john.common.dalservice.IEntityService;
import com.john.study.spring.boot.commons.dao.ILinkPrivilegeDao;
import com.john.study.spring.boot.commons.dao.entity.LinkPrivilegeEntity;

/**
 * @author jianguangtao03382 2018/4/26
 */
public interface ILinkPrivilegeService extends IEntityService<LinkPrivilegeEntity, ILinkPrivilegeDao> {
}
