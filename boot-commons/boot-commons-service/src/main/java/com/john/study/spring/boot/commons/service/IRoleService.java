package com.john.study.spring.boot.commons.service;

import com.john.common.dalservice.IEntityService;
import com.john.study.spring.boot.commons.dao.IRoleDao;
import com.john.study.spring.boot.commons.dao.entity.RoleEntity;

/**
 * @author jianguangtao03382 2018/4/25
 */
public interface IRoleService extends IEntityService<RoleEntity, IRoleDao> {
}
