package com.john.study.spring.boot.commons.service.impl;

import com.john.study.spring.boot.commons.dao.IRoleLinkDao;
import com.john.study.spring.boot.commons.dao.entity.RoleLinkEntity;
import com.john.study.spring.boot.commons.service.IRoleLinkService;
import org.springframework.stereotype.Service;

/**
 * @author jianguangtao03382 2018/4/26
 */
@Service
public class RoleLinkServiceImpl extends EntityServiceImpl<RoleLinkEntity, IRoleLinkDao>
        implements IRoleLinkService {
}
