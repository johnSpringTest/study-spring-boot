package com.john.study.spring.boot.commons.service.impl;

import com.john.common.dao.data.data.RecordStatusEnum;
import com.john.study.spring.boot.commons.dao.IUserRoleDao;
import com.john.study.spring.boot.commons.dao.entity.UserRoleEntity;
import com.john.study.spring.boot.commons.data.vo.RoleVo;
import com.john.study.spring.boot.commons.data.vo.UserRoleVo;
import com.john.study.spring.boot.commons.service.IUserRoleService;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * @author jianguangtao03382 2018/4/25
 */
@Service
public class UserRoleServiceImpl extends EntityServiceImpl<UserRoleEntity, IUserRoleDao> implements IUserRoleService {
    @Override
    public List<RoleVo> queryRoleListByUserId(Long userId) {
        if (null == userId) {
            return Collections.emptyList();
        }

        UserRoleVo queryVo = new UserRoleVo();
        queryVo.setUserId(userId);
        queryVo.setRecordStatus(RecordStatusEnum.NORMAL.getKey());

        return getDal().queryRoleListByUserId(queryVo);
    }
}
