package com.john.study.spring.boot.commons.dao.impl;

import com.john.common.dal.impl.BaseDalImpl;
import com.john.study.spring.boot.commons.dao.ILinkPrivilegeDao;
import com.john.study.spring.boot.commons.dao.entity.LinkPrivilegeEntity;
import org.springframework.stereotype.Repository;

/**
 * @author jianguangtao03382 2018/4/26
 */
@Repository
public class LinkPrivilegeDaoImpl extends BaseDalImpl<LinkPrivilegeEntity> implements ILinkPrivilegeDao {
}
