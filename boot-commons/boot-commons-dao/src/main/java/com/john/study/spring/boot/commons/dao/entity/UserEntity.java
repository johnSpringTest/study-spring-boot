package com.john.study.spring.boot.commons.dao.entity;

import com.john.common.dao.data.entity.BaseBusinessEntity;
import com.john.common.dao.data.util.EntityIdValueUtil;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * @author jianguangtao03382 2018/4/17
 */
@Entity
@Table(name = "t_user")
public class UserEntity extends BaseBusinessEntity {
    private static final long serialVersionUID = 4800257721655646891L;

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "nick_name", length = 50)
    private String nickName;

    @Column(name = "user_name", length = 40)
    private String userName;

    @Column(name = "email", length = 50)
    private String email;
    /**
     * 用户状态 0新注册 1正常 2过期 3锁定 4过期 5注销
     */
    @Column(name = "user_status", length = 20)
    private String userStatus;

    @Column(name = "password", length = 64)
    private String password;

    /**
     * 所属组织id
     */
    @Column(name = "org_id")
    private Long orgId;
    /**
     * 手机
     */
    @Column(length = 15)
    private String phone;
    /**
     * qq
     */
    @Column(length = 15)
    private String qq;
    /**
     * 启用日期 新注册用户激活的日期
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "enable_date")
    private Date enableDate;
    /**
     * 失效日期
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "expire_date")
    private Date expireDate;
    /**
     * 用户备注
     */
    @Column(length = 200)
    private String remark;


    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public void setId(Serializable id) {
        this.id = EntityIdValueUtil.getLong(id);
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public Date getEnableDate() {
        return enableDate;
    }

    public void setEnableDate(Date enableDate) {
        this.enableDate = enableDate;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "UserEntity{" +
                "id=" + id +
                ", nickName='" + nickName + '\'' +
                ", userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                ", userStatus='" + userStatus + '\'' +
                ", password='" + password + '\'' +
                ", creatorId=" + creatorId +
                ", createTime=" + createTime +
                ", updaterId=" + updaterId +
                ", updateTime=" + updateTime +
                ", recordStatus='" + recordStatus + '\'' +
                '}';
    }
}
