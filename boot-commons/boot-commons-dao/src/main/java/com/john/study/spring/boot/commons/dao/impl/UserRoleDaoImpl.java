package com.john.study.spring.boot.commons.dao.impl;

import com.john.common.dal.impl.BaseDalImpl;
import com.john.study.spring.boot.commons.dao.IUserRoleDao;
import com.john.study.spring.boot.commons.dao.entity.UserRoleEntity;
import com.john.study.spring.boot.commons.data.vo.RoleVo;
import com.john.study.spring.boot.commons.data.vo.UserRoleVo;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author jianguangtao03382 2018/4/23
 */
@Repository
public class UserRoleDaoImpl extends BaseDalImpl<UserRoleEntity> implements IUserRoleDao {
    private static final String SQL_QUERY_ROLE_LIST_BY_USER_ID = "sec.userrole.queryRoleListByUserId";

    @Override
    public List<RoleVo> queryRoleListByUserId(UserRoleVo queryVo) {
        return super.querySql(SQL_QUERY_ROLE_LIST_BY_USER_ID, queryVo, RoleVo.class);
    }
}
