package com.john.study.spring.boot.commons.dao;

import com.john.common.dal.IBaseDal;
import com.john.study.spring.boot.commons.dao.entity.UserRoleEntity;
import com.john.study.spring.boot.commons.data.vo.RoleVo;
import com.john.study.spring.boot.commons.data.vo.UserRoleVo;

import java.util.List;

/**
 * 用户角色关联对象Dao类
 *
 * @author jiangguangtao@foxmail.com 2016/3/13
 */
public interface IUserRoleDao extends IBaseDal<UserRoleEntity> {
    /**
     * 根据用户id查询此用户关联的所有角色列表
     *
     * @param queryVo 查询参数， userId是必须的参数
     * @return 角色列表
     */
    List<RoleVo> queryRoleListByUserId(UserRoleVo queryVo);
}
