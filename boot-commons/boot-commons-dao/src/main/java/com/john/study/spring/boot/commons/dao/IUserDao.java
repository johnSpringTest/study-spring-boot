package com.john.study.spring.boot.commons.dao;

import com.john.common.dal.IBaseDal;
import com.john.common.dao.data.data.PaginationResult;
import com.john.study.spring.boot.commons.dao.entity.UserEntity;
import com.john.study.spring.boot.commons.data.vo.UserVo;

/**
 * @author jianguangtao03382 2018/4/19
 */
public interface IUserDao extends IBaseDal<UserEntity> {
    /**
     * 分页查询用户列表
     *
     * @param queryVo 查询参数
     * @return 分页用户列表
     */
    PaginationResult<UserVo> queryUserListPage(UserVo queryVo);
}
