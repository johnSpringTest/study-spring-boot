package com.john.study.spring.boot.commons.dao.entity;

import com.john.common.dao.data.annotation.ParentId;
import com.john.common.dao.data.entity.IIdEntity;
import com.john.common.dao.data.util.EntityIdValueUtil;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

/**
 * 角色与链接关联信息
 *
 * @author jianguangtao03382 2018/4/26
 */
@Entity
@Table(name = "t_role_link")
public class RoleLinkEntity implements IIdEntity {
    private static final long serialVersionUID = 1182752293680330653L;
    @Id
    @GeneratedValue
    private Long id;
    /**
     * 角色id
     */
    @ParentId
    @Column(name = "role_id")
    private Long roleId;
    /**
     * 链接记录id
     */
    @Column(name = "link_id")
    private Long linkId;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public void setId(Serializable id) {
        this.id = EntityIdValueUtil.getLong(id);
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getLinkId() {
        return linkId;
    }

    public void setLinkId(Long linkId) {
        this.linkId = linkId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoleLinkEntity that = (RoleLinkEntity) o;
        return Objects.equals(roleId, that.roleId) &&
                Objects.equals(linkId, that.linkId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roleId, linkId);
    }

    @Override
    public String toString() {
        return "RoleLinkEntity{" +
                "id=" + id +
                ", roleId=" + roleId +
                ", linkId=" + linkId +
                '}';
    }
}
