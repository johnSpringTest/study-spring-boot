package com.john.study.spring.boot.commons.dao.entity;

import com.john.common.dao.data.annotation.ParentId;
import com.john.common.dao.data.entity.IIdEntity;
import com.john.common.dao.data.util.EntityIdValueUtil;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

/**
 * 角色关联的链接权限
 *
 * @author jianguangtao03382 2018/4/26
 */
@Entity
@Table(name = "t_role_link_privilege")
public class RoleLinkPrivilegeEntity implements IIdEntity {
    private static final long serialVersionUID = 3194356906930939940L;
    @Id
    @GeneratedValue
    private Long id;

    /**
     * 角色id
     */
    @ParentId
    @Column(name = "role_id")
    private Long roleId;

    /**
     * 权限所属链接id
     */
    @Column(name = "link_id")
    private Long linkId;
    /**
     * 链接权限id
     */
    @Column(name = "link_privilege_id")
    private Long linkPrivilegeId;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public void setId(Serializable id) {
        this.id = EntityIdValueUtil.getLong(id);
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getLinkId() {
        return linkId;
    }

    public void setLinkId(Long linkId) {
        this.linkId = linkId;
    }

    public Long getLinkPrivilegeId() {
        return linkPrivilegeId;
    }

    public void setLinkPrivilegeId(Long linkPrivilegeId) {
        this.linkPrivilegeId = linkPrivilegeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoleLinkPrivilegeEntity that = (RoleLinkPrivilegeEntity) o;
        return Objects.equals(roleId, that.roleId) &&
                Objects.equals(linkId, that.linkId) &&
                Objects.equals(linkPrivilegeId, that.linkPrivilegeId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(roleId, linkId, linkPrivilegeId);
    }

    @Override
    public String toString() {
        return "RoleLinkPrivilegeEntity{" +
                "id=" + id +
                ", roleId=" + roleId +
                ", linkId=" + linkId +
                ", linkPrivilegeId=" + linkPrivilegeId +
                '}';
    }
}
