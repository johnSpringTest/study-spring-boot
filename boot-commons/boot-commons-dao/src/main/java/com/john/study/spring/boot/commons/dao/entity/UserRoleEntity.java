package com.john.study.spring.boot.commons.dao.entity;

import com.john.common.dao.data.annotation.ParentId;
import com.john.common.dao.data.entity.IIdEntity;
import com.john.common.dao.data.util.EntityIdValueUtil;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 用户角色关联Vo对象
 *
 * @author jiangguangtao@foxmail.com 2016/3/13
 */
@Entity
@Table(name = "t_user_role")
public class UserRoleEntity implements IIdEntity {
    private static final long serialVersionUID = 1351116102012282086L;
    @Id
    @GeneratedValue
    private Long id;

    /**
     * 用户ID
     */
    @ParentId
    @Column(name = "user_id")
    private Long userId;
    /**
     * 角色ID
     */
    @Column(name = "role_id")
    private Long roleId;
    /**
     * 角色编码
     */
    @Column(name = "role_code", length = 30)
    private String roleCode;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Serializable id) {
        this.id = EntityIdValueUtil.getLong(id);
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    @Override
    public boolean equals(Object params) {
        if (this == params) return true;
        if (params == null || getClass() != params.getClass()) return false;

        UserRoleEntity that = (UserRoleEntity) params;

        if (!userId.equals(that.userId)) return false;
        return (!roleId.equals(that.roleId));

    }

    @Override
    public int hashCode() {
        int result = userId.hashCode();
        result = 31 * result + roleId.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "UserRoleEntity{" +
                "id=" + id +
                ", userId=" + userId +
                ", roleId=" + roleId +
                ", roleCode='" + roleCode + '\'' +
                '}';
    }
}
