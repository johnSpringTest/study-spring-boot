package com.john.study.spring.boot.commons.dao.impl;

import com.john.common.dal.impl.BaseDalImpl;
import com.john.common.dao.data.data.PaginationResult;
import com.john.study.spring.boot.commons.dao.IRoleDao;
import com.john.study.spring.boot.commons.dao.entity.RoleEntity;
import com.john.study.spring.boot.commons.data.vo.RoleVo;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author jianguangtao03382 2018/4/23
 */
@Repository
public class RoleDaoImpl extends BaseDalImpl<RoleEntity> implements IRoleDao {
    private static final String SQL_QUERY_ROLE_BY_PARAMS = "sec.role.queryRoleByParams";

    @Override
    public List<RoleVo> queryByParams(RoleVo queryVo) {
        return super.querySql(SQL_QUERY_ROLE_BY_PARAMS, queryVo, RoleVo.class);
    }

    @Override
    public PaginationResult<RoleVo> queryPagerByParams(RoleVo queryVo) {
        return super.querySql(SQL_QUERY_ROLE_BY_PARAMS, queryVo, queryVo.getPager(), RoleVo.class);
    }
}
