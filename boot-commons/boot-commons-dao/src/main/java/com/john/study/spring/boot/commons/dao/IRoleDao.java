package com.john.study.spring.boot.commons.dao;

import com.john.common.dal.IBaseDal;
import com.john.common.dao.data.data.PaginationResult;
import com.john.study.spring.boot.commons.dao.entity.RoleEntity;
import com.john.study.spring.boot.commons.data.vo.RoleVo;

import java.util.List;

/**
 * 角色信息dao
 */
public interface IRoleDao extends IBaseDal<RoleEntity> {
    List<RoleVo> queryByParams(RoleVo queryVo);

    PaginationResult<RoleVo> queryPagerByParams(RoleVo queryVo);
}
