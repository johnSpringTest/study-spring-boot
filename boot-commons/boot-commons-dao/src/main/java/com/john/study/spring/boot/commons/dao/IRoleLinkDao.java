package com.john.study.spring.boot.commons.dao;

import com.john.common.dal.IBaseDal;
import com.john.study.spring.boot.commons.dao.entity.RoleLinkEntity;

/**
 * @author jianguangtao03382 2018/4/26
 */
public interface IRoleLinkDao extends IBaseDal<RoleLinkEntity> {
}
