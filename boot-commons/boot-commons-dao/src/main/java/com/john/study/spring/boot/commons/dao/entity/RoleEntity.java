package com.john.study.spring.boot.commons.dao.entity;

import com.john.common.dao.data.entity.BaseBusinessEntity;
import com.john.common.dao.data.util.EntityIdValueUtil;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 基本的角色信息
 *
 * @author jiangguangtao@foxmail.com 2014年5月16日
 */
@Entity
@Table(name = "t_role")
public class RoleEntity extends BaseBusinessEntity {
    private static final long serialVersionUID = -4891935760152476657L;

    @Id
    @GeneratedValue
    protected Long id;
    /**
     * 角色编号
     */
    @Column(name = "role_code", length = 30)
    private String roleCode;
    /**
     * 角色名称
     */
    @Column(name = "role_name", length = 50)
    private String roleName;
    /**
     * 角色类型
     */
    @Column(name = "role_type", length = 20)
    private String roleType;
    /**
     * 启用标识
     */
    @Column(name = "enable_flag", length = 1)
    private String enableFlag;
    /**
     * 角色备注
     */
    @Column(length = 200)
    private String remark;

    public RoleEntity() {
        super();
    }

    @Override
    public void setId(Serializable id) {
        this.id = EntityIdValueUtil.getLong(id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleLevel) {
        this.roleType = roleLevel;
    }

    public String getEnableFlag() {
        return enableFlag;
    }

    public void setEnableFlag(String enableFlag) {
        this.enableFlag = enableFlag;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public boolean equals(Object params) {
        if (this == params) return true;
        if (params == null || getClass() != params.getClass()) return false;

        RoleEntity secRole = (RoleEntity) params;

        if (!roleCode.equals(secRole.roleCode)) return false;
        if (!roleName.equals(secRole.roleName)) return false;
        if (roleType != null ? !roleType.equals(secRole.roleType) : secRole.roleType != null) return false;
        return !(enableFlag != null ? !enableFlag.equals(secRole.enableFlag) : secRole.enableFlag != null);
    }

    @Override
    public int hashCode() {
        int result = roleCode.hashCode();
        result = 31 * result + roleName.hashCode();
        result = 31 * result + (roleType != null ? roleType.hashCode() : 0);
        result = 31 * result + (enableFlag != null ? enableFlag.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RoleEntity{" +
                "id=" + id +
                ", roleCode='" + roleCode + '\'' +
                ", roleName='" + roleName + '\'' +
                ", roleType='" + roleType + '\'' +
                ", enableFlag='" + enableFlag + '\'' +
                ", remark='" + remark + '\'' +
                ", creatorId=" + creatorId +
                ", createTime=" + createTime +
                ", updaterId=" + updaterId +
                ", updateTime=" + updateTime +
                ", recordStatus='" + recordStatus + '\'' +
                '}';
    }
}
