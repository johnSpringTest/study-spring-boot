package com.john.study.spring.boot.commons.dao.entity;

import com.john.common.dao.data.annotation.ParentId;
import com.john.common.dao.data.entity.BaseBusinessEntity;
import com.john.common.dao.data.util.EntityIdValueUtil;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

/**
 * 链接上挂的权限列表
 *
 * @author jianguangtao03382 2018/4/26
 */
@Entity
@Table(name = "t_link_privilege")
public class LinkPrivilegeEntity extends BaseBusinessEntity {
    private static final long serialVersionUID = 4279933178018603887L;

    @Id
    @GeneratedValue
    private Long id;
    /**
     * 所属链接id
     */
    @ParentId
    @Column(name = "link_id")
    private Long linkId;
    /**
     * 权限编码
     */
    @Column(name = "privilege_code", length = 40, nullable = false)
    private String privilegeCode;
    /**
     * 权限名称 描述
     */
    @Column(name = "privilege_name", length = 50)
    private String privilegeName;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public void setId(Serializable id) {
        this.id = EntityIdValueUtil.getLong(id);
    }

    public Long getLinkId() {
        return linkId;
    }

    public void setLinkId(Long linkId) {
        this.linkId = linkId;
    }

    public String getPrivilegeCode() {
        return privilegeCode;
    }

    public void setPrivilegeCode(String privilegeCode) {
        this.privilegeCode = privilegeCode;
    }

    public String getPrivilegeName() {
        return privilegeName;
    }

    public void setPrivilegeName(String privilegeName) {
        this.privilegeName = privilegeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LinkPrivilegeEntity that = (LinkPrivilegeEntity) o;
        return Objects.equals(linkId, that.linkId) &&
                Objects.equals(privilegeCode, that.privilegeCode);
    }

    @Override
    public int hashCode() {

        return Objects.hash(linkId, privilegeCode);
    }

}
