package com.john.study.spring.boot.commons.dao.entity;

import com.john.common.dao.data.annotation.ParentId;
import com.john.common.dao.data.entity.BaseBusinessEntity;
import com.john.common.dao.data.util.EntityIdValueUtil;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

/**
 * 链接地址配置
 *
 * @author jianguangtao03382 2018/4/26
 */
@Entity
@Table(name = "t_link_address")
public class LinkAddressEntity extends BaseBusinessEntity {
    private static final long serialVersionUID = 3468687560477753321L;
    @Id
    @GeneratedValue
    private Long id;
    /**
     * 链接编码
     */
    @Column(name = "link_code", length = 30, nullable = false, updatable = false)
    private String linkCode;
    /**
     * 链接显示名称
     */
    @Column(name = "link_name", length = 100, nullable = false)
    private String linkName;
    /**
     * 上级链接id
     */
    @ParentId
    @Column(name = "parent_id")
    private Long parentId;
    /**
     * 链接类型
     */
    @Column(name = "link_type", length = 4)
    private String linkType;
    /**
     * 链接层级 0 1 2 3
     * 0为根目录
     */
    @Column(name = "link_level")
    private Integer linkLevel;

    /**
     * 显示顺序
     */
    @Column(name = "view_index")
    private Integer viewIndex;
    /**
     * 是否为目录标识
     */
    @Column(name = "folder_flag", length = 1)
    private String folderFlag;
    /**
     * 是否启用标识
     */
    @Column(name = "enable_flag")
    private String enableFlag;
    /**
     * 链接显示样式 css类名
     */
    @Column(name = "link_icon_class", length = 50)
    private String linkIconClass;
    /**
     * 链接的具体地址
     */
    @Column(name = "link_address", length = 200)
    private String linkAddress;
    /**
     * 打开链接的方式
     */
    @Column(name = "link_target", length = 30)
    private String linkTarget;
    /**
     * 链接备注信息
     */
    @Column(length = 200)
    private String remark;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public void setId(Serializable id) {
        this.id = EntityIdValueUtil.getLong(id);
    }

    public String getLinkCode() {
        return linkCode;
    }

    public void setLinkCode(String linkCode) {
        this.linkCode = linkCode;
    }

    public String getLinkName() {
        return linkName;
    }

    public void setLinkName(String linkName) {
        this.linkName = linkName;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getLinkType() {
        return linkType;
    }

    public void setLinkType(String linkType) {
        this.linkType = linkType;
    }

    public Integer getLinkLevel() {
        return linkLevel;
    }

    public void setLinkLevel(Integer linkLevel) {
        this.linkLevel = linkLevel;
    }

    public Integer getViewIndex() {
        return viewIndex;
    }

    public void setViewIndex(Integer viewIndex) {
        this.viewIndex = viewIndex;
    }

    public String getFolderFlag() {
        return folderFlag;
    }

    public void setFolderFlag(String folderFlag) {
        this.folderFlag = folderFlag;
    }

    public String getEnableFlag() {
        return enableFlag;
    }

    public void setEnableFlag(String enableFlag) {
        this.enableFlag = enableFlag;
    }

    public String getLinkIconClass() {
        return linkIconClass;
    }

    public void setLinkIconClass(String linkIconClass) {
        this.linkIconClass = linkIconClass;
    }

    public String getLinkAddress() {
        return linkAddress;
    }

    public void setLinkAddress(String linkAddress) {
        this.linkAddress = linkAddress;
    }

    public String getLinkTarget() {
        return linkTarget;
    }

    public void setLinkTarget(String linkTarget) {
        this.linkTarget = linkTarget;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LinkAddressEntity that = (LinkAddressEntity) o;
        return Objects.equals(linkCode, that.linkCode) &&
                Objects.equals(linkName, that.linkName) &&
                Objects.equals(parentId, that.parentId) &&
                Objects.equals(enableFlag, that.enableFlag) &&
                Objects.equals(linkAddress, that.linkAddress);
    }

    @Override
    public int hashCode() {

        return Objects.hash(linkCode, linkName, parentId, enableFlag, linkAddress);
    }


}
