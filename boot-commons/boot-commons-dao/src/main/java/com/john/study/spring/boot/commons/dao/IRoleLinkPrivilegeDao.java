package com.john.study.spring.boot.commons.dao;

import com.john.common.dal.IBaseDal;
import com.john.study.spring.boot.commons.dao.entity.RoleLinkPrivilegeEntity;
import com.john.study.spring.boot.commons.data.vo.LinkPrivilegeVo;
import com.john.study.spring.boot.commons.data.vo.RoleLinkPrivilegeVo;

import java.util.List;

/**
 * @author jianguangtao03382 2018/4/26
 */
public interface IRoleLinkPrivilegeDao extends IBaseDal<RoleLinkPrivilegeEntity> {
    /**
     * 根据用户id，查询此用户关联的功能权限列表
     *
     * @param queryVo 查询参数
     * @return 功能权限列表
     */
    List<LinkPrivilegeVo> queryLinkPrivilegeListByUserId(RoleLinkPrivilegeVo queryVo);
}
