package com.john.study.spring.boot.commons.dao.impl;

import com.john.common.dal.impl.BaseDalImpl;
import com.john.study.spring.boot.commons.dao.IRoleLinkPrivilegeDao;
import com.john.study.spring.boot.commons.dao.entity.RoleLinkPrivilegeEntity;
import com.john.study.spring.boot.commons.data.vo.LinkPrivilegeVo;
import com.john.study.spring.boot.commons.data.vo.RoleLinkPrivilegeVo;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author jianguangtao03382 2018/4/26
 */
@Repository
public class RoleLinkPrivilegeDaoImpl extends BaseDalImpl<RoleLinkPrivilegeEntity>
        implements IRoleLinkPrivilegeDao {
    private static final String SQL_QUERY_LINK_PRIVILEGE_BY_USERID = "sec.role.link.privilege.queryLinkPrivilegeListByUserId";

    @Override
    public List<LinkPrivilegeVo> queryLinkPrivilegeListByUserId(RoleLinkPrivilegeVo queryVo) {
        return super.querySql(SQL_QUERY_LINK_PRIVILEGE_BY_USERID, queryVo, LinkPrivilegeVo.class);
    }
}
