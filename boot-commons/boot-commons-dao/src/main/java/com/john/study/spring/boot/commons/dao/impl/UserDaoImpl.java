package com.john.study.spring.boot.commons.dao.impl;

import com.john.common.dal.impl.BaseDalImpl;
import com.john.common.dao.data.data.PaginationResult;
import com.john.study.spring.boot.commons.dao.IUserDao;
import com.john.study.spring.boot.commons.dao.entity.UserEntity;
import com.john.study.spring.boot.commons.data.vo.UserVo;
import org.springframework.stereotype.Repository;

/**
 * @author jianguangtao03382 2018/4/19
 */
@Repository
public class UserDaoImpl extends BaseDalImpl<UserEntity> implements IUserDao {
    private static final String SQL_QUERY_USER_LIST = "sec.userinfo.queryUserByParams";

    @Override
    public PaginationResult<UserVo> queryUserListPage(UserVo queryVo) {
        return super.querySql(SQL_QUERY_USER_LIST, queryVo, queryVo.getPager(), UserVo.class);
    }
}
