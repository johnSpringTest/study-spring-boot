package com.john.study.spring.boot.did;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author jiangguangtao on 2017/9/20.
 */
@SpringBootApplication
public class MongoDidApplication {
    public static void main(String[] args) {
        SpringApplication.run(MongoDidApplication.class, args);
    }
}
