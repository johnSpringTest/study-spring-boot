package com.john.study.spring.boot.did.dao;

import com.john.study.spring.boot.did.entity.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * @author jiangguangtao on 2017/9/20.
 */
public interface CustomerRepository extends MongoRepository<Customer, String> {
    /**
     * 通过首名查询
     * @param firstName
     * @return
     */
    Customer findByFirstName(String firstName);

    /**
     * 通过姓查询
     * @param lastName
     * @return
     */
    List<Customer> queryByLastName(String lastName);
}
