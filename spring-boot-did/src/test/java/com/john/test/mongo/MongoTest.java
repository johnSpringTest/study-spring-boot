package com.john.test.mongo;

import com.john.study.spring.boot.did.MongoDidApplication;
import com.john.study.spring.boot.did.dao.CustomerRepository;
import com.john.study.spring.boot.did.entity.Customer;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import java.util.List;

/**
 * @author jiangguangtao on 2017/9/20.
 */
@SpringBootTest(classes = {MongoDidApplication.class})
public class MongoTest extends AbstractJUnit4SpringContextTests {
    private static final Logger LOGGER = LoggerFactory.getLogger(MongoTest.class);

    @Autowired
    private CustomerRepository customerRepository;

    @Test
    public void testSaveCustomer() {
//        Customer customer = new Customer("John", "Jiang");
//        Customer customer = new Customer("Peter", "Zhao");
        Customer customer = new Customer("Yun", "Zhao");
        LOGGER.info("待保存记录：{}", customer);
        Customer saved = customerRepository.save(customer);
        LOGGER.info("保存后记录：{}", saved);
    }

    @Test
    public void testFindByFirstName() {
        Customer c = customerRepository.findByFirstName("John");
        LOGGER.info("FirstName查询到的记录：{}", c);

        List<Customer> list = customerRepository.queryByLastName("Zhao");
        LOGGER.info("通过LastName查询：{}", list);

        c = customerRepository.findOne("59c23b37ef79361f30cbe64d");
        LOGGER.info("Id查询到的记录：{}", c);
    }
}
