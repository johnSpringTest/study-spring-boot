package com.john.study.spring.boot.server;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * @author jiangguangtao on 2017/4/8.
 */
@RestController
@SpringBootApplication
@EnableDiscoveryClient
public class SayHelloServer {
    private static final Logger LOGGER = LoggerFactory.getLogger(SayHelloServer.class);

    private static final List<String> greetings = Arrays.asList("Hi there", "Greetings", "Salutations");

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Autowired
    private Environment environment;

    @Value("${msg.hello:hellomsg}")
    private String helloMsg;

    @Value("${app.env:prd}")
    private String appEnv;

    @RequestMapping("/")
    public String home() {
        LOGGER.info("访问根应用");

        LOGGER.info("hello:{},,,env:{}", helloMsg, appEnv);
        return "Hello";
    }

    @RequestMapping("/greeting")
    public String greeting() {
        LOGGER.info("访问 /greeting");
        Random random = new Random(System.currentTimeMillis());
        int idx = random.nextInt(greetings.size());

        return greetings.get(idx);
    }

    @RequestMapping("/ping")
    public String ping(@RequestParam(name = "from", defaultValue = "匿名") String from) {
        if (StringUtils.isBlank(from)) {
            from = "匿名";
        }
        LOGGER.info("收到ping信息：{}", from);
        String serverPort = environment.getProperty("server.port");
        LOGGER.info("服务器端口：{}", serverPort);
        return String.format("%s Pong from %s for user %s", sdf.format(new Date()), serverPort, from);
    }

    public static void main(String[] args) {
        SpringApplication.run(SayHelloServer.class, args);
    }
}
