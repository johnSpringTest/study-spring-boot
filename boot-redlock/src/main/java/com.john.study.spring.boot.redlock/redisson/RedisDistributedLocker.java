package com.john.study.spring.boot.redlock.redisson;

import com.john.study.spring.boot.redlock.redisson.exception.UnableToAcquireLockException;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @author jiangguangtao on 2017/9/20.
 */
@Component
public class RedisDistributedLocker implements DistributedLocker {
    private static final String LOCK_PREFIX = "lock:";

    @Autowired
    private RedissionConnector redissionConnector;


    @Override
    public <T> T tryLock(String resourceName, AcquiredLockWorker<T> worker) throws UnableToAcquireLockException {
        return tryLock(resourceName, DEFAULT_WAIT_TIME, DEFAULT_HOLD_TIME, worker);
    }

    @Override
    public <T> T tryLock(String resourceName, long waitTime, long holdTime, AcquiredLockWorker<T> worker) throws UnableToAcquireLockException {
        RedissonClient client = redissionConnector.getClient();
        RLock lock = client.getLock(getLockName(resourceName));
        try {
            boolean lockFlag = lock.tryLock(waitTime, holdTime, TimeUnit.MILLISECONDS);
            if (lockFlag) {
                try {
                    return worker.invokeAfterAcquiredLock();
                } finally {
                    lock.unlock();
                }
            }
        } catch (InterruptedException e) {
            throw new UnableToAcquireLockException(resourceName + " 获取锁的时间中断了", e);
        }

        throw new UnableToAcquireLockException(resourceName);
    }

    @Override
    public <T> T lock(String resourceName, AcquiredLockWorker<T> worker) throws UnableToAcquireLockException {
        return lock(resourceName, worker, DEFAULT_HOLD_TIME);
    }

    @Override
    public <T> T lock(String resourceName, AcquiredLockWorker<T> worker, long holdTime)
            throws UnableToAcquireLockException {
        RedissonClient client = redissionConnector.getClient();
        RLock lock = client.getLock(LOCK_PREFIX + resourceName);

        try {
            lock.lock(holdTime, TimeUnit.SECONDS);
            return worker.invokeAfterAcquiredLock();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean isLocked(String resourceName) {
//        RedissonClient client = redissionConnector.getClient();
//        RLock lock = client.getLock(getLockName(resourceName));
//        return lock.isLocked();
        return doWithClient(client -> client.getLock(getLockName(resourceName)).isLocked());
    }

    @Override
    public boolean isHeldByCurrentThread(String resourceName) {
        return doWithClient(client -> client.getLock(getLockName(resourceName)).isHeldByCurrentThread());
    }


    @Override
    public <T> T doWithClient(RedisClientWorker<T> worker) {
        RedissonClient client = redissionConnector.getClient();
        return worker.doWithClient(client);
    }

    /**
     * 获取资源对应的锁名称
     *
     * @param resourceName 资源名称
     * @return 资源名称对应的锁名称
     */
    private String getLockName(String resourceName) {
        return LOCK_PREFIX + resourceName;
    }
}
