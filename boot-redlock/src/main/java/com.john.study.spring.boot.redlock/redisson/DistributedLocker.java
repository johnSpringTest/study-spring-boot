package com.john.study.spring.boot.redlock.redisson;

import com.john.study.spring.boot.redlock.redisson.exception.UnableToAcquireLockException;

/**
 * 分布式的获取锁的管理工具
 * <p>
 * 锁等待、超时时间的单位默认毫秒
 *
 * 只实现了简单的锁实现，其它请自行实现
 *
 * @author jiangguangtao on 2017/9/20.
 */
public interface DistributedLocker {
    /**
     * 默认的等待锁的时间 500毫秒
     */
    long DEFAULT_WAIT_TIME = 500L;
    /**
     * 默认的持有锁的时间 -1 一直持有到方法结束
     */
    long DEFAULT_HOLD_TIME = -1L;

    /**
     * 尝试获取资源锁
     * <p>
     * 默认等待锁时间与持有锁时间参见常量
     *
     * @param resourceName 资源名称
     * @param worker       成功获取锁后执行的方法
     * @param <T>          方法返回值类型
     * @return 返回值
     * @throws UnableToAcquireLockException 获取锁失败
     */
    <T> T tryLock(String resourceName, AcquiredLockWorker<T> worker) throws UnableToAcquireLockException;


    /**
     * 尝试获取资源锁
     * <p>
     * 如果锁未被占用，直接返回； 如果在等待<code>waitTime</code>后还未能获取获取锁失败
     *
     * @param resourceName 资源名称
     * @param waitTime     等待锁超时时间
     * @param holdTime     锁定资源后持有时间 超过此时会释放锁  -1表示一直持有到方法结束
     * @param worker       成功获取锁后执行的方法
     * @param <T>          方法返回值类型
     * @return 返回值
     * @throws UnableToAcquireLockException 获取锁失败
     */
    <T> T tryLock(String resourceName, long waitTime, long holdTime, AcquiredLockWorker<T> worker) throws UnableToAcquireLockException;


    /**
     * 获取锁
     * <p>
     * 默认持有锁时间参见常量
     *
     * @param resourceName 待锁定的资源名称
     * @param worker       锁定资源后执行的方法
     * @param <T>          返回值类型
     * @return 执行方法返回值
     * @throws UnableToAcquireLockException 获取锁失败
     */
    <T> T lock(String resourceName, AcquiredLockWorker<T> worker) throws UnableToAcquireLockException;

    /**
     * 获取锁
     * <p>
     * 如果能获取锁直接返回，如果锁当前不可用，将一直等待锁变成可用！
     *
     * @param <T>          返回值类型
     * @param resourceName 待锁定的资源名称
     * @param worker       锁定资源后执行的方法
     * @param holdTime     锁定资源后持有锁的最大时间 毫秒  -1表示一直持有到方法结束
     * @return 执行方法返回值
     * @throws UnableToAcquireLockException 获取锁失败
     */
    <T> T lock(String resourceName, AcquiredLockWorker<T> worker, long holdTime) throws UnableToAcquireLockException;

    /**
     * 判断指定的资源是否有锁
     *
     * @param resourceName 待锁定的资源名称
     * @return true有锁，false没有锁
     */
    boolean isLocked(String resourceName);

    /**
     * 检查资源锁是否被当前线程持有
     *
     * @return <code>true</code> if held by current thread
     * otherwise <code>false</code>
     */
    boolean isHeldByCurrentThread(String resourceName);

    /**
     * 在Redission客户端中执行的方法
     * @param worker
     * @param <T>
     * @return
     */
    <T> T doWithClient(RedisClientWorker<T> worker);
}
