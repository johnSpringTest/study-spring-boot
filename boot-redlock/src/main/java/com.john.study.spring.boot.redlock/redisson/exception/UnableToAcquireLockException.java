package com.john.study.spring.boot.redlock.redisson.exception;

/**
 * 获取锁异常
 * @author jiangguangtao on 2017/9/20.
 */
public class UnableToAcquireLockException extends Exception {
    private static final long serialVersionUID = -5984786244873356120L;

    public UnableToAcquireLockException() {
    }

    public UnableToAcquireLockException(String message) {
        super(message);
    }

    public UnableToAcquireLockException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnableToAcquireLockException(Throwable cause) {
        super(cause);
    }

    public UnableToAcquireLockException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
