package com.john.study.spring.boot.redlock.redisson;

/**
 * 获取锁后执行的方法
 *
 * @author jiangguangtao on 2017/9/20.
 */
public interface AcquiredLockWorker<T> {
    /**
     * 获取锁成功后的回调函数
     * <p>
     * 此回调方法不能抛出声明异常。
     *
     * @return 返回类型
     * @throws RuntimeException 执行时出错了。
     */
    T invokeAfterAcquiredLock();
}
