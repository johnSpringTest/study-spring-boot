package com.john.study.spring.boot.redlock.redisson;

import org.redisson.api.RedissonClient;

/**
 * @author jianguangtao03382 2018/4/17
 */
public interface RedisClientWorker<T> {
    /**
     * 获取到Redis客户端后执行的回调方法
     * @param client 客户端对象
     * @return 方法返回值
     */
    T doWithClient(RedissonClient client);
}
