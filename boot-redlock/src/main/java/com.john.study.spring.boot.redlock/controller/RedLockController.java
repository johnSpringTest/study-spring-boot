package com.john.study.spring.boot.redlock.controller;

import com.john.study.spring.boot.redlock.redisson.RedisDistributedLocker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

/**
 * @author jiangguangtao on 2017/9/20.
 */
@RestController
public class RedLockController {
    private static final Logger LOGGER = LoggerFactory.getLogger(RedLockController.class);

    @Autowired
    private RedisDistributedLocker redisDistributedLocker;

    @GetMapping("/testLock")
    public String testLock() {
        CountDownLatch startLatch = new CountDownLatch(1);
        CountDownLatch doneLatch = new CountDownLatch(5);

        for(int i=0;i<5;i++) {
            new Thread(new Worker(startLatch, doneLatch)).start();
        }
        startLatch.countDown(); // 开始任务

        try {
            doneLatch.await(); // 等待所有任务完成
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        LOGGER.info("所有任务完成了。");

        return "redLock";
    }

    @GetMapping("/testTryLock")
    public String testTryLock() {

        int threads = 5;
        CountDownLatch doneLatch = new CountDownLatch(threads);
        CyclicBarrier barrier = new CyclicBarrier(threads);

        for(int i=0;i<5;i++) {
            new Thread(new TryWorker(barrier, doneLatch)).start();
        }

//        try {
//            doneLatch.await(); // 等待所有任务完成
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        LOGGER.info("所有任务完成了。");

        return "tryLock";
    }



    class Worker implements Runnable {
        private final Logger LOGGER = LoggerFactory.getLogger(Worker.class);

        private final CountDownLatch startLatch;
        private final CountDownLatch doneLatch;

        public Worker(CountDownLatch startLatch, CountDownLatch doneLatch) {
            this.startLatch = startLatch;
            this.doneLatch = doneLatch;
        }

        @Override
        public void run() {
            try {
                startLatch.await();
                redisDistributedLocker.lock("testRedLock", () -> {
                    doTask();
                    return null;
                });
            } catch (Exception e) {
                LOGGER.error("执行任务异常", e);
            }
        }

        void doTask() {
            LOGGER.info("start");
            Random r = new Random();
            int times = r.nextInt(500);
            LOGGER.info("将执行{}ms", times);
            try {
                Thread.sleep(times);
            } catch (InterruptedException e) {
            }
            LOGGER.info("end");
            doneLatch.countDown();
        }
    }



    class TryWorker implements Runnable {
        private final Logger LOGGER = LoggerFactory.getLogger(TryWorker.class);

        private final CyclicBarrier cyclicBarrier;
        private final CountDownLatch doneLatch;

        public TryWorker(CyclicBarrier cyclicBarrier, CountDownLatch doneLatch) {
            this.cyclicBarrier = cyclicBarrier;
            this.doneLatch = doneLatch;
        }

        @Override
        public void run() {
            try {
                cyclicBarrier.await();
                redisDistributedLocker.tryLock("testRedLock", () -> {
                    doTask();
                    return null;
                });
            } catch (Exception e) {
                LOGGER.error("执行任务异常", e);
            }
        }

        void doTask() {
            LOGGER.info("start");
            Random r = new Random();
            int times = r.nextInt(600);
            LOGGER.info("将执行{}ms", times);
            try {
                Thread.sleep(times);
            } catch (InterruptedException e) {
            }
            LOGGER.info("end");
            doneLatch.countDown();
        }
    }
}
