package com.john.test.semaphore;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class SemaphoreTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(SemaphoreTest.class);

    @Test
    public void test1() {
        // 线程池 
        ExecutorService exec = Executors.newCachedThreadPool();
        // 只能5个线程同时访问 
        final Semaphore semp = new Semaphore(5);
        // 模拟20个客户端访问
        for (int index = 0; index < 20; index++) {
            final int NO = index;
            Runnable run = () -> {
                try {
                    // 获取许可
                    semp.acquire();
                    System.out.println("Accessing: " + NO);
                    Thread.sleep((long) (Math.random() * 10000));
                    // 访问完后，释放 ，如果屏蔽下面的语句，则在控制台只能打印5条记录，之后线程一直阻塞
                    semp.release();
                } catch (InterruptedException e) {
                }
            };
            exec.execute(run);
        }
        // 退出线程池 
        exec.shutdown();
    }

    @Test
    public void test2() {
        // 线程池
        ExecutorService exec = Executors.newScheduledThreadPool(5);

        CountDownLatch latch = new CountDownLatch(20);

        // 模拟20个客户端访问
        for (int index = 0; index < 20; index++) {
            final int NO = index;
            Runnable run = () -> {
                try {
                    System.out.println("Accessing: " + NO);
                    Thread.sleep((long) (Math.random() * 10000));
                    latch.countDown();
                } catch (InterruptedException e) {
                }
            };
            exec.execute(run);
        }
        // 退出线程池
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        exec.shutdown();
    }
}